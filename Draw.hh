/*
 * Author: pschmidt
 */
#pragma once

#include <memory>
#include <ObjectTypes/TriangleMesh/TriangleMesh.hh>
#include <ObjectTypes/TriangleMesh/TriangleMeshTypes.hh>

#include "RWTHColors.hh"

#include <memory>
#include <unordered_map>

namespace ACG { namespace SceneGraph { class TransformNode; } }
namespace ACG { namespace SceneGraph { class PointNode; } }
namespace ACG { namespace SceneGraph { class LineNode; } }

namespace Utils
{

class Draw
{
public:
    using TransformNode = ACG::SceneGraph::TransformNode;
    using PointNode = ACG::SceneGraph::PointNode;
    using LineNode = ACG::SceneGraph::LineNode;

    using Color = ACG::Vec4f;

public:
    Draw();
    ~Draw();

    void set_translation(ACG::Vec3d _t);
    void reset_translation();

    void clear();
    void point(ACG::Vec2d _p, Color _color, float _width = 10.0f);
    void point(ACG::Vec3d _p, Color _color, float _width = 10.0f);
    void line(ACG::Vec2d _from, ACG::Vec2d _to, Color _color, float _width = 4.0f);
    void line(ACG::Vec3d _from, ACG::Vec3d _to, Color _color, float _width = 4.0f);

private:
    void apply_translation();
    TransformNode* transform_node();
    PointNode* point_node(float _width);
    LineNode* line_node(float _width);

private:
    ACG::Vec3d translation_;
    TransformNode* transform_node_;
    std::vector<PointNode*> point_nodes_;
    std::vector<LineNode*> line_nodes_;
};

} // namespace Utils
