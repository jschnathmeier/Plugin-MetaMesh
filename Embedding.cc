#include "Embedding.hh"
#include <math.h>
#include <QDebug>
#include <random>

/*!
 * \brief Embedding::Embedding
 */
Embedding::Embedding() {
  qDebug() << "Start of MetaMesh ctor";
  base_mesh_ = nullptr;
  meta_mesh_ = nullptr;
  qDebug() << "Initialized MetaMesh";
}

/*!
 * \brief Embedding::CopyInitialization initializes the meta mesh as a copy of the
 * base mesh
 * \param base_mesh
 * \param meta_mesh
 */
void Embedding::CopyInitialization(TriMesh &base_mesh, PolyMesh &meta_mesh) {
  qDebug() << "Copying the base mesh into the metamesh.";
  boundaries_ = false;
  initial_triangulation_ = true;
  base_mesh_ = &base_mesh;
  meta_mesh_ = &meta_mesh;
  meta_mesh.clear();
  InitializeProperties();
  for (auto bvh : base_mesh_->vertices()) {
    auto mvh = meta_mesh_->add_vertex(base_mesh_->point(bvh));
    base_mesh_->property(bv_connection_, bvh) = mvh;
    meta_mesh_->property(mv_connection_, mvh) = bvh;
    base_mesh_->property(bsplithandle_, bvh) =
        OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
    base_mesh_->property(voronoiID_, bvh) =
        OpenMesh::PolyConnectivity::InvalidVertexHandle;
  }
  for (auto bfh : base_mesh_->faces()) {
    auto bheh0 = base_mesh_->halfedge_handle(bfh);
    auto bheh1 = base_mesh_->next_halfedge_handle(bheh0);
    auto bheh2 = base_mesh_->next_halfedge_handle(bheh1);
    auto bvh0 = base_mesh_->from_vertex_handle(bheh0);
    auto bvh1 = base_mesh_->from_vertex_handle(bheh1);
    auto bvh2 = base_mesh_->from_vertex_handle(bheh2);
    auto mvh0 = base_mesh_->property(bv_connection_, bvh0);
    auto mvh1 = base_mesh_->property(bv_connection_, bvh1);
    auto mvh2 = base_mesh_->property(bv_connection_, bvh2);
    auto mheh0 = base_mesh_->property(bhe_connection_,
                     base_mesh_->opposite_halfedge_handle(bheh0));
    auto mheh1 = base_mesh_->property(bhe_connection_,
                     base_mesh_->opposite_halfedge_handle(bheh1));
    auto mheh2 = base_mesh_->property(bhe_connection_,
                     base_mesh_->opposite_halfedge_handle(bheh2));
    if (meta_mesh_->is_valid_handle(mheh0)) {
      mheh0 = meta_mesh_->opposite_halfedge_handle(mheh0);
    }
    if (meta_mesh_->is_valid_handle(mheh1)) {
      mheh1 = meta_mesh_->opposite_halfedge_handle(mheh1);
    }
    if (meta_mesh_->is_valid_handle(mheh2)) {
      mheh2 = meta_mesh_->opposite_halfedge_handle(mheh2);
    }
    auto mfh = AddFace({mvh0, mvh1, mvh2}, {mheh0, mheh1, mheh2});
    mheh0 = meta_mesh_->halfedge_handle(mfh);
    mheh1 = meta_mesh_->next_halfedge_handle(mheh0);
    mheh2 = meta_mesh_->next_halfedge_handle(mheh1);
    base_mesh_->property(bhe_connection_, bheh0) = mheh0;
    base_mesh_->property(bhe_connection_, bheh1) = mheh1;
    base_mesh_->property(bhe_connection_, bheh2) = mheh2;
    meta_mesh_->property(mhe_connection_, mheh0) = bheh0;
    meta_mesh_->property(mhe_connection_, mheh1) = bheh1;
    meta_mesh_->property(mhe_connection_, mheh2) = bheh2;
    base_mesh_->property(next_heh_, bheh0) =
        OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
    base_mesh_->property(next_heh_, bheh1) =
        OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
    base_mesh_->property(next_heh_, bheh2) =
        OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
    base_mesh_->property(halfedge_weight_, bheh0) = 1.0;
    base_mesh_->property(halfedge_weight_, bheh1) = 1.0;
    base_mesh_->property(halfedge_weight_, bheh2) = 1.0;
  }
  for (auto bheh : base_mesh_->halfedges()) {
    if (base_mesh_->is_boundary(bheh)) {
      auto mheh = meta_mesh_->opposite_halfedge_handle(
        base_mesh_->property(bhe_connection_, base_mesh_->opposite_halfedge_handle(bheh)));
      base_mesh_->property(bhe_connection_, bheh) = mheh;
      meta_mesh_->property(mhe_connection_, mheh) = bheh;
      meta_mesh_->set_boundary(mheh);
      meta_mesh_->set_halfedge_handle(meta_mesh_->from_vertex_handle(mheh), mheh);
    }
  }
  for (auto mheh : meta_mesh_->halfedges()) {
    if (meta_mesh_->is_boundary(mheh)) {
      auto mhehn = meta_mesh_->halfedge_handle(meta_mesh_->to_vertex_handle(mheh));
      meta_mesh_->set_next_halfedge_handle(mheh, mhehn);
    }
  }
  ColorizeMetaMesh();
  initial_triangulation_ = false;
}

/*!
 * \brief Embedding::SelectionTriangulation triangulate selected meta vertices
 * \param base_mesh
 * \param meta_mesh
 * \param type
 */
void Embedding::SelectionTriangulation(TriMesh& base_mesh, PolyMesh& meta_mesh) {
  qDebug() << "Entering InitialTriangulation function.";
  boundaries_ = false;
  initial_triangulation_ = true;
  base_mesh_ = &base_mesh;
  meta_mesh_ = &meta_mesh;
  meta_mesh.clear();
  std::vector<OpenMesh::VertexHandle> meta_mesh_points;
  for (auto vh : base_mesh_->vertices()) {
    if (base_mesh_->status(vh).selected() == true) {
      meta_mesh_points.push_back(vh);
    }
  }
  if (meta_mesh_points.empty()) {
    CopyInitialization(base_mesh, meta_mesh);
    return;
  }
  TriangulationPipeline(meta_mesh_points);
  initial_triangulation_ = false;
}

/*!
 * \brief Embedding::RandomTriangulation
 * 1 / ratio chance of a vertex being randomly selected as a meta vertex
 * \param base_mesh
 * \param meta_mesh
 * \param input
 * \param rtype
 * \param type
 */
void Embedding::RandomTriangulation(TriMesh &base_mesh, PolyMesh &meta_mesh, double input,
                                    RandomType rtype) {
  initial_triangulation_ = true;
  boundaries_ = false;
  base_mesh_ = &base_mesh;
  meta_mesh_ = &meta_mesh;
  meta_mesh.clear();
  std::random_device rd;  //Will be used to obtain a seed for the random number engine
  std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
  std::uniform_int_distribution<> dis0(0, static_cast<int>(1.0/input));
  std::uniform_int_distribution<> dis1(0, static_cast<int>(base_mesh_->n_vertices())-1);
  std::vector<OpenMesh::VertexHandle> meta_mesh_points;

  // Find boundary vertices
  OpenMesh::VPropHandleT<bool> traversed_boundary;
  base_mesh_->add_property(traversed_boundary, "Mark boundaries that have been traversed");
  for (auto bvh : base_mesh_->vertices()) {
    base_mesh_->property(traversed_boundary, bvh) = false;
  }
  for (auto bvh : base_mesh_->vertices()) {
    // Go into each boundary once
    if (base_mesh_->is_boundary(bvh)
        && base_mesh_->property(traversed_boundary, bvh) == false) {
      uint step = 0;
      // Find the right stepsize to evenly distribute boundary vertices
      if (rtype == RATIO) {
        step = static_cast<uint>(1/input);
      } else if (rtype == TOTAL) {
        step = static_cast<uint>(base_mesh_->n_vertices()/input);
      }
      base_mesh_->property(traversed_boundary, bvh) = true;
      meta_mesh_points.push_back(bvh);
      uint currstep = 1;
      auto bheh = base_mesh_->halfedge_handle(bvh);
      while (!base_mesh_->property(traversed_boundary,
                                   base_mesh_->to_vertex_handle(bheh))) {
        base_mesh_->property(traversed_boundary,
                             base_mesh_->to_vertex_handle(bheh)) = true;
        if (currstep == step) {
          meta_mesh_points.push_back(base_mesh_->to_vertex_handle(bheh));
          currstep = 0;
        }
        ++currstep;
        bheh = base_mesh_->next_halfedge_handle(bheh);
      }
    }
  }
  base_mesh_->remove_property(traversed_boundary);

  if (rtype == RATIO) {
    for (auto bvh : base_mesh_->vertices()) {
      if (dis0(gen) == 1 && !base_mesh_->is_boundary(bvh)) {
        meta_mesh_points.push_back(bvh);
      }
    }
  } else if (rtype == TOTAL) {
    while (meta_mesh_points.size()<input*10000) {
      meta_mesh_points.push_back(base_mesh_->vertex_handle(static_cast<uint>(dis1(gen))));
    }
  }
  while (!TriangulationPipeline(meta_mesh_points)) {
    meta_mesh_points.clear();
    qDebug() << "Automatically remeshing";
    // TODO: There is a memory leak somewhere in here that shows when Triangulation has to
    // be repeated many times because some Voronoi regions were of the wrong Euler characteristic.
    // In those cases TriangulationPipeline returns false, and everything is repeated.
    // Calling clear() here does not seem to deallocate all memory that should be
    // deallocated. But inside the clear() function it looks like it deallocates all
    // properties and also calls clean() afterwards. All the memory allocated at this point
    // should be in the form of elements and properties of base_mesh_ and meta_mesh_.
    // __Most likely__ this bug was due to not removing the new properties and doubly
    // creating them when TriangulationPipeline() is called again, but I have no time left
    // for extensive testing. IF some memory error crops up again when rerolling triangulations
    // it is most likely here but I don't know what exactly causes it.
    meta_mesh_->clear();
    RemoveProperties();
    if (rtype == RATIO) {
      for (auto vh : base_mesh_->vertices()) {
        if (dis0(gen) == 1)
          meta_mesh_points.push_back(vh);
      }
    } else if (rtype == TOTAL) {
      for (unsigned long i=0; i<input*10000; ++i) {
        meta_mesh_points.push_back(base_mesh_->vertex_handle(static_cast<uint>(dis1(gen))));
      }
    }
  }
  initial_triangulation_ = false;
}

/*!
 * \brief Embedding::TriangulationPipeline
 * \param meta_mesh_points
 * \param type
 * \return
 */
bool Embedding::TriangulationPipeline(
    std::vector<OpenMesh::VertexHandle> meta_mesh_points) {
  debug_hard_stop_ = false;
  InitializeProperties();
  CreateMetaMeshVertices(meta_mesh_points);
  if (!TriangulateMetaMesh())
    return false;
  CleanUpBaseMesh();
  TestHalfedgeConsistency();
  ColorizeMetaMesh();
  return true;
}

/*!
 * \brief Embedding::InitializeProperties
 */
void Embedding::InitializeProperties() {
  base_mesh_->add_property(voronoiID_, "Voronoi area");
  base_mesh_->add_property(bsplithandle_, "Vertex to collapse into, undefined if not introduced"
                                          "by a splitting operation");
  base_mesh_->add_property(bv_connection_, "BVV_Connection");
  meta_mesh_->add_property(mv_connection_, "MVV_Connection");
  base_mesh_->add_property(bhe_connection_, "Pointer from base he to meta heh");
  meta_mesh_->add_property(mhe_connection_, "Pointer from meta he to first base heh");
  base_mesh_->add_property(bhe_border_, "Pointer from border base he to meta heh");
  meta_mesh_->add_property(mhe_border_, "Pointer from meta he to first border base heh");
  base_mesh_->add_property(next_heh_, "Pointer from base he to next heh on meta edge");
  base_mesh_->add_property(halfedge_weight_, "Pointer from base he to meta heh");
}

/*!
 * \brief Embedding::RemoveProperties
 */
void Embedding::RemoveProperties() {
  base_mesh_->remove_property(voronoiID_);
  base_mesh_->remove_property(bsplithandle_);
  base_mesh_->remove_property(bv_connection_);
  meta_mesh_->remove_property(mv_connection_);
  base_mesh_->remove_property(bhe_connection_);
  meta_mesh_->remove_property(mhe_connection_);
  base_mesh_->remove_property(bhe_border_);
  meta_mesh_->remove_property(mhe_border_);
  base_mesh_->remove_property(next_heh_);
  base_mesh_->remove_property(halfedge_weight_);
}

/*!
 * \brief Embedding::CreateMetaMeshVertices
 * \param meta_mesh_points
 */
void Embedding::CreateMetaMeshVertices(std::vector<OpenMesh::VertexHandle> meta_mesh_points) {
  qDebug() << "Start appointing BaseMesh properties:";
  for (auto bvh : base_mesh_->vertices()) {
    base_mesh_->property(bv_connection_, bvh) = OpenMesh::PolyConnectivity::InvalidVertexHandle;
    base_mesh_->property(bsplithandle_, bvh) = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  }
  for (auto bheh : base_mesh_->halfedges()) {
    base_mesh_->property(next_heh_, bheh) = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
    base_mesh_->property(bhe_connection_, bheh)
        = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
    base_mesh_->property(halfedge_weight_, bheh) = 1.0;
    if (base_mesh_->is_boundary(bheh)) {
      boundaries_ = true;
    }
  }

  qDebug() << "Start appointing MetaMesh properties:";
  for (auto mpoint : meta_mesh_points) {
    auto mvhnew = meta_mesh_->add_vertex(base_mesh_->point(mpoint));
    meta_mesh_->property(mv_connection_, mvhnew) = mpoint;
    base_mesh_->property(bv_connection_, mpoint) = mvhnew;
  }
  qDebug() << "Finish appointing MetaMesh properties:";
}

/*!
 * \brief Embedding::TriangulateMetaMesh
 * \param type
 * \return
 */
bool Embedding::TriangulateMetaMesh() {
  OpenMesh::VPropHandleT<double> voronoidistance;
  OpenMesh::VPropHandleT<OpenMesh::HalfedgeHandle> to_heh;
  OpenMesh::HPropHandleT<int> multiplicity_heh;
  base_mesh_->add_property(voronoidistance, "Voronoi distance from center");
  base_mesh_->add_property(to_heh, "Incoming edge from the shortest path");
  base_mesh_->add_property(multiplicity_heh, "Mark duplicate paths for elimination");
  qDebug() << "Entering Delaunay:";
  if (!Delaunay(voronoidistance, to_heh, multiplicity_heh)) {
      return false;
  }

  A_StarTriangulation();

  base_mesh_->remove_property(voronoidistance);
  base_mesh_->remove_property(to_heh);
  base_mesh_->remove_property(multiplicity_heh);
  return true;
}

/*!
 * \brief Embedding::PreProcessEdges global preprocessing
 */
void Embedding::PreProcessEdges() {
  for (auto beh : base_mesh_->edges()) {
    auto bheh = base_mesh_->halfedge_handle(beh, 0);
    ConditionalSplit(bheh);
  }
  base_mesh_->update_normals();
}

/*!
 * \brief Embedding::ProcessEdge local preprocessing
 * \param meh
 */
void Embedding::ProcessEdge(OpenMesh::EdgeHandle meh) {
  ProcessHalfedge(meta_mesh_->halfedge_handle(meh, 0));
  ProcessHalfedge(meta_mesh_->halfedge_handle(meh, 1));
}

/*!
 * \brief Embedding::ProcessVertex local preprocessing
 * \param bvh
 */
void Embedding::ProcessVertex(OpenMesh::VertexHandle bvh) {
  std::list<OpenMesh::HalfedgeHandle> splits;
  for (auto bvoheh : base_mesh_->voh_range(bvh)) {
    if (IsSectorBorder(base_mesh_->to_vertex_handle(bvoheh))) {
      splits.push_back(bvoheh);
    }
  }
  while (!splits.empty()) {
    ConditionalSplit(splits.front());
    splits.pop_front();
  }
}

/*!
 * \brief Embedding::CleanupVertex perform legal base collapses around vertex bvh
 * \param bvh
 */
void Embedding::CleanupVertex(OpenMesh::VertexHandle bvh) {
  std::list<OpenMesh::VertexHandle> collapses;
  for (auto bvhit : base_mesh_->vv_range(bvh)) {
    if (base_mesh_->is_valid_handle(bvhit)
        && !base_mesh_->status(bvhit).deleted()
        && base_mesh_->is_valid_handle(base_mesh_->property(bsplithandle_, bvhit))
        && !base_mesh_->status(base_mesh_->property(bsplithandle_, bvhit)).deleted()) {
      collapses.push_back(bvhit);
    }
  }
  while (!collapses.empty()) {
    ConditionalCollapse(collapses.front());
    collapses.pop_front();
  }
}

/*!
 * \brief Embedding::CleanupFace cleans up the faces around mheh
 * \param mheh
 */
void Embedding::CleanupFace(OpenMesh::HalfedgeHandle mheh) {
  auto mhehiter = mheh;
  do {
    if (base_mesh_->is_valid_handle(meta_mesh_->property(mhe_connection_, mhehiter))) {
      CleanupHalfedge(mhehiter);
    }
    mhehiter = meta_mesh_->next_halfedge_handle(mhehiter);
  } while (mhehiter != mheh);
}

/*!
 * \brief Embedding::CleanupHalfedge cleans up mheh
 * \param mheh
 */
void Embedding::CleanupHalfedge(OpenMesh::HalfedgeHandle mheh) {
  auto linehalfedges = GetBaseHalfedges(mheh);
  auto mhehnext = meta_mesh_->next_halfedge_handle(mheh);
  while (!base_mesh_->is_valid_handle(meta_mesh_->property(mhe_connection_, mhehnext))) {
    mhehnext = meta_mesh_->next_halfedge_handle(meta_mesh_->opposite_halfedge_handle(mhehnext));
  }
  linehalfedges.push_back(meta_mesh_->property(mhe_connection_, mhehnext));
  for (auto blheh : linehalfedges) {
    auto bvhfrom = base_mesh_->from_vertex_handle(blheh);
    if (base_mesh_->is_valid_handle(bvhfrom)
        && base_mesh_->is_valid_handle(base_mesh_->property(bsplithandle_, bvhfrom))
        && !base_mesh_->status(bvhfrom).deleted()
        && !base_mesh_->status(base_mesh_->property(bsplithandle_, bvhfrom)).deleted()) {
      ConditionalCollapse(bvhfrom);
    }
    for (auto bheh : LeftHalfCircle(blheh)) {
      auto bvhto = base_mesh_->to_vertex_handle(bheh);
      if (base_mesh_->is_valid_handle(bvhto)
          && base_mesh_->is_valid_handle(base_mesh_->property(bsplithandle_, bvhto))
          && !base_mesh_->status(bvhto).deleted()
          && !base_mesh_->status(base_mesh_->property(bsplithandle_, bvhto)).deleted()) {
        ConditionalCollapse(bvhto);
      }
    }
  }
  BaseGarbageCollection();
}

/*!
 * \brief Embedding::ProcessNeighbors pre process the neighboring edges around mheh
 * this is called before tracing
 * \param meh
 */
void Embedding::ProcessNeighbors(OpenMesh::EdgeHandle meh) {
  auto mheh0 = meta_mesh_->halfedge_handle(meh, 0);
  auto mheh1 = meta_mesh_->halfedge_handle(meh, 1);

  // Find the ACTUAL neighbors that are already traced, not the neighbors in the meta mesh
  auto neighbor0 = meta_mesh_->opposite_halfedge_handle(
        meta_mesh_->prev_halfedge_handle(mheh0));
  while (!base_mesh_->is_valid_handle(meta_mesh_->property(mhe_connection_, neighbor0))
         && neighbor0 != mheh0) {
    neighbor0 = meta_mesh_->opposite_halfedge_handle(
          meta_mesh_->prev_halfedge_handle(neighbor0));
  }
  auto neighbor1 = meta_mesh_->next_halfedge_handle(
        meta_mesh_->opposite_halfedge_handle(mheh0));
  while (!base_mesh_->is_valid_handle(meta_mesh_->property(mhe_connection_, neighbor1))
         && neighbor1 != mheh0) {
    neighbor1 = meta_mesh_->next_halfedge_handle(
          meta_mesh_->opposite_halfedge_handle(neighbor1));
  }
  auto neighbor2 = meta_mesh_->opposite_halfedge_handle(
        meta_mesh_->prev_halfedge_handle(mheh1));
  while (!base_mesh_->is_valid_handle(meta_mesh_->property(mhe_connection_, neighbor2))
         && neighbor2 != mheh1) {
    neighbor2 = meta_mesh_->opposite_halfedge_handle(
          meta_mesh_->prev_halfedge_handle(neighbor2));
  }
  auto neighbor3 = meta_mesh_->next_halfedge_handle(
        meta_mesh_->opposite_halfedge_handle(mheh1));
  while (!base_mesh_->is_valid_handle(meta_mesh_->property(mhe_connection_, neighbor3))
         && neighbor3 != mheh1) {
    neighbor3 = meta_mesh_->next_halfedge_handle(
          meta_mesh_->opposite_halfedge_handle(neighbor3));
  }

  // neighbor0 and neighbor 2 were found searching towards the left but
  // ProcessHalfedge is left-facing, so process the opposite halfedges.
  if (neighbor0 != mheh0) {
    ProcessHalfedge(meta_mesh_->opposite_halfedge_handle(neighbor0));
    ProcessHalfedge(neighbor1);
  }
  if (neighbor2 != mheh1) {
    ProcessHalfedge(meta_mesh_->opposite_halfedge_handle(neighbor2));
    ProcessHalfedge(neighbor3);
  }
}

/*!
 * \brief Embedding::ProcessFace
 * Iterate over the currently traced patch that mheh is part of and call pre-processing
 * on all traversed meta halfedges
 * \param mheh
 */
void Embedding::ProcessFace(OpenMesh::HalfedgeHandle mheh) {
  // Edge case: Trying to process the face of an mheh which has an adjacent valence 1 vertex
  // If mheh points towards that vertex there is no problem in the iteration
  // If mheh points away from that vertex it immediately iterates into itself and terminates
  // -> no processing
  // Fix this by changing mheh to its opposite halfedge if it points away from a valence 1 vertex
  // This cannot go wrong since only one of the two vertices of mheh can be valence 1 in any
  // scenario where pre-processing is required (both valence 1 can only happen in
  // initial triangulation)
  // The previous solution works for edges next to valence 1 being traced but breaks face splits
  // so instead just find an adjacent traced edge and iterate from there; this works in both
  // cases.
  auto mhehf = meta_mesh_->next_halfedge_handle(mheh);
  auto mhehb = meta_mesh_->next_halfedge_handle(meta_mesh_->opposite_halfedge_handle(mheh));
  if (base_mesh_->is_valid_handle(meta_mesh_->property(mhe_connection_, mhehf))) {
    mheh = mhehf;
  } else if (base_mesh_->is_valid_handle(meta_mesh_->property(mhe_connection_, mhehb))) {
    mheh = mhehb;
  } else {
  // In this case BOTH vertices have to be empty, so the state has to be initial triangulation
    assert(initial_triangulation_);
    return;
  }
  auto mhehtemp = mheh;
  do {
    if (base_mesh_->is_valid_handle(meta_mesh_->property(mhe_connection_, mhehtemp))
        && base_mesh_->property(bhe_connection_, meta_mesh_->property(mhe_connection_, mhehtemp))
        == mhehtemp) {
      ProcessHalfedge(mhehtemp);
      mhehtemp = meta_mesh_->next_halfedge_handle(mhehtemp);
    } else {
      mhehtemp = meta_mesh_->next_halfedge_handle(meta_mesh_->opposite_halfedge_handle(mhehtemp));
    }
  } while (mhehtemp != mheh);
}

/*!
 * \brief Embedding::ProcessHalfedge pre-process base halfedges around meta halfedge mheh
 * \param mheh
 */
void Embedding::ProcessHalfedge(OpenMesh::HalfedgeHandle mheh) {
  auto linehalfedges = GetBaseHalfedges(mheh);
  auto mhehnext = meta_mesh_->next_halfedge_handle(mheh);
  while (!base_mesh_->is_valid_handle(meta_mesh_->property(mhe_connection_, mhehnext))) {
    mhehnext = meta_mesh_->next_halfedge_handle(meta_mesh_->opposite_halfedge_handle(mhehnext));
  }
  linehalfedges.push_back(meta_mesh_->property(mhe_connection_, mhehnext));
  for (auto blheh : linehalfedges) {
    for (auto bheh : LeftHalfCircle(blheh)) {
      ConditionalSplit(bheh);
    }
  }
}

/*!
 * \brief Embedding::ConditionalSplit split bheh if permissible
 * \param bheh
 * \return
 */
bool Embedding::ConditionalSplit(OpenMesh::HalfedgeHandle bheh) {
  // Don't split boundary halfedges for now since it is leading to crashes.
  // This could be enabled (I was in the middle of adding boundary split functionality)
  // But there is not enough time
  // TODO: remove this check and fix boundary splits in CollapseBaseHe and SplitBaseHe
  // After testing quite a bit without splitting boundary halfedges this seems to be working quite
  // well. Splitting boundary halfedges may not be necessary at all.
  if (base_mesh_->is_boundary(bheh)
      || base_mesh_->is_boundary(base_mesh_->opposite_halfedge_handle(bheh))) {
    return false;
  }

  if (!meta_mesh_->is_valid_handle(base_mesh_->property(bhe_connection_, bheh))) {
    bool split1 = false;
    bool split2 = false;
    bool split3 = false;
    for (auto bheh : base_mesh_->voh_range(base_mesh_->from_vertex_handle(bheh))) {
      if (meta_mesh_->is_valid_handle(base_mesh_->property(bhe_connection_, bheh))) {
        split1 = true;
        auto mvh0 = base_mesh_->property(bv_connection_,
                                         base_mesh_->to_vertex_handle(bheh));
        if (meta_mesh_->is_valid_handle(mvh0)
            && meta_mesh_->to_vertex_handle(base_mesh_->property(
               bhe_connection_, bheh)) != mvh0
            && meta_mesh_->from_vertex_handle(base_mesh_->property(
               bhe_connection_, bheh)) != mvh0) {
          split2 = true;
        }
      }
    }
    for (auto bheh : base_mesh_->voh_range(base_mesh_->to_vertex_handle(bheh))) {
      if (meta_mesh_->is_valid_handle(base_mesh_->property(bhe_connection_, bheh))) {
        split2 = true;
        auto mvh0 = base_mesh_->property(bv_connection_,
                                         base_mesh_->from_vertex_handle(bheh));
        if (meta_mesh_->is_valid_handle(mvh0)
            && meta_mesh_->to_vertex_handle(base_mesh_->property(
               bhe_connection_, bheh)) != mvh0
            && meta_mesh_->from_vertex_handle(base_mesh_->property(
               bhe_connection_, bheh)) != mvh0) {
          split1 = true;
        }
      }
    }
    auto bvid0 = base_mesh_->property(voronoiID_, base_mesh_->from_vertex_handle(bheh));
    auto bvid1 = base_mesh_->property(voronoiID_, base_mesh_->to_vertex_handle(bheh));
    if (bvid0 != bvid1 && initial_triangulation_) {
      auto meh = GetMetaEdge(base_mesh_->from_vertex_handle(bheh));
      if (!meta_mesh_->is_valid_handle(meh)) {
        split3 = true;
      } else {
        auto mvid0 = meta_mesh_->from_vertex_handle(meta_mesh_->halfedge_handle(meh, 0));
        auto mvid1 = meta_mesh_->to_vertex_handle(meta_mesh_->halfedge_handle(meh, 0));
        if (!((mvid0 == bvid0 && mvid1 == bvid1)  || (mvid0 == bvid1 && mvid1 == bvid0))) {
          split3 = true;
        }
      }
    }

    if ((split1 && split2) || (split3 && (split1 || split2)))  {
      SplitBaseHe(bheh);
      return true;
    }
  } else if ((meta_mesh_->is_valid_handle(base_mesh_->property(bv_connection_,
              base_mesh_->from_vertex_handle(bheh))))
             && (meta_mesh_->is_valid_handle(base_mesh_->property(bv_connection_,
                 base_mesh_->to_vertex_handle(bheh))))) {
    SplitBaseHe(bheh);
    return true;
  }
  return false;
}

/*!
 * \brief Embedding::LeftHalfCircle
 * \param bheh
 * \return the halfedges surrounding bheh in a left halfcircle and
 * stopping when reaching a meta halfedge (used for preprocessing)
 */
std::vector<OpenMesh::HalfedgeHandle> Embedding::LeftHalfCircle(OpenMesh::HalfedgeHandle bheh) {
  std::vector<OpenMesh::HalfedgeHandle> retval;
  retval.push_back(bheh);
  auto curr = base_mesh_->opposite_halfedge_handle(base_mesh_->prev_halfedge_handle(bheh));
  retval.push_back(curr);
  while (curr != bheh && !meta_mesh_->is_valid_handle(
           base_mesh_->property(bhe_connection_, curr))) {
    curr = base_mesh_->opposite_halfedge_handle(base_mesh_->prev_halfedge_handle(curr));
    retval.push_back(curr);
  }
  return retval;
}

/*!
 * \brief Embedding::GetBaseHalfedges
 * \param mheh
 * \return the base halfedges of meta halfedge mheh
 */
std::vector<OpenMesh::HalfedgeHandle> Embedding::GetBaseHalfedges(
    OpenMesh::HalfedgeHandle mheh) {
  std::vector<OpenMesh::HalfedgeHandle> retval;
  auto bheh = meta_mesh_->property(mhe_connection_, mheh);
  assert(base_mesh_->is_valid_handle(bheh));
  retval.push_back(bheh);
  auto curr = base_mesh_->property(next_heh_, bheh);
  while (base_mesh_->is_valid_handle(curr)) {
    retval.push_back(curr);
    curr = base_mesh_->property(next_heh_, curr);
  }
  return retval;
}

/*!
 * \brief Embedding::CleanUpBaseMesh collapses new bhehs where allowed and collects garbage it
 * \param garbagecollection collect garbage if set to true
 */
void Embedding::CleanUpBaseMesh(bool garbagecollection) {
  std::queue<OpenMesh::VertexHandle> hehqueue;
  for (auto bvh : base_mesh_->vertices()) {
    if (base_mesh_->is_valid_handle(bvh)
        && !base_mesh_->status(bvh).deleted()
        && base_mesh_->is_valid_handle(base_mesh_->property(bsplithandle_, bvh))
        && !base_mesh_->status(base_mesh_->property(bsplithandle_, bvh)).deleted()) {
      auto bvhnew = ConditionalCollapse(bvh);
      if (base_mesh_->is_valid_handle(bvhnew)) {
        hehqueue.push(bvhnew);
      }
    }
    if (debug_hard_stop_) {
      break;
    }
  }
  // More passes to clear up topologically blocked halfedges whose merges depend on order
  // this is tricky because there may be a few that _can't_ be merged no matter what
  unsigned long ctr = hehqueue.size()*2;
  while (!hehqueue.empty() && ctr > 0) {
    auto bvh = hehqueue.front();
    hehqueue.pop();
    auto bvhnew = ConditionalCollapse(bvh);
    if (base_mesh_->is_valid_handle(bvhnew)) {
      hehqueue.push(bvhnew);
    }
    --ctr;
  }

  if (garbagecollection) {
    BaseGarbageCollection();
  }
}

/*!
 * \brief Embedding::ConditionalCollapse collapse bvh into its bsplithandle_ halfedge
 * if permissible
 * \param bvh
 * \return
 */
OpenMesh::VertexHandle  Embedding::ConditionalCollapse(OpenMesh::VertexHandle bvh) {
  auto bheh = base_mesh_->property(bsplithandle_, bvh);
  auto bheho = base_mesh_->opposite_halfedge_handle(bheh);
  if (base_mesh_->status(bheh).deleted() || base_mesh_->status(bvh).deleted()) {
    return OpenMesh::PolyConnectivity::InvalidVertexHandle;
  }
  auto bvh0 = base_mesh_->from_vertex_handle(bheh);
  auto bvh1 = base_mesh_->to_vertex_handle(bheh);

  // Sanity check
  assert(base_mesh_->from_vertex_handle(bheh) == bvh);

  bool merge = true;

  auto meh0 = GetMetaEdge(bvh0);
  auto meh1 = GetMetaEdge(bvh1);
  // Collapsing into a deleted edge
  if (base_mesh_->status(bheh).deleted()) {
    merge = false;
    qDebug() << "Vertex " << bvh0.idx() << " points towards deleted halfedge "
             << bheh.idx() << " so it cannot be merged. Presumably something is wrong.";
  }

  // Collapsing from one meta halfedge into another
  if (meta_mesh_->is_valid_handle(meh0) && meta_mesh_->is_valid_handle(meh1)
      && !(meh0 == meh1)) {
    merge = false;
  }

  // Collapsing from a meta halfedge into itself but not along the edge
  if (meta_mesh_->is_valid_handle(meh0) && meh0 == meh1 && meta_mesh_->edge_handle(
        base_mesh_->property(bhe_connection_, bheh)) != meh0) {
    merge = false;
  }

  // Collapsing from a meta vertex into a meta halfedge that doesn't end
  if (meta_mesh_->is_valid_handle(base_mesh_->property(bv_connection_, bvh0))) {
    for (auto bih : base_mesh_->vih_range(bvh1)) {
      if (base_mesh_->is_valid_handle(base_mesh_->property(next_heh_, bih))
          && !(base_mesh_->property(next_heh_, bih) == bheho
               || bih == bheh)) {
        merge = false;
      }
    }
  }

  // Collapsing a meta vertex
  if (base_mesh_->is_valid_handle(base_mesh_->property(bv_connection_, bvh0))) {
    merge = false;
  }

  // Collapsing into a meta vertex but not along a meta edge when the collapsed vertex lies on a meta edge
  auto mhehx = meta_mesh_->edge_handle(base_mesh_->property(bhe_connection_, bheh));
  if (meta_mesh_->is_valid_handle(base_mesh_->property(bv_connection_, bvh1))
      && meta_mesh_->is_valid_handle(meh0)
      && mhehx != meh0) {
    merge = false;
  }

  // Collapsing of inhabited edges onto boundary edges to avoid blocking
  if (meta_mesh_->is_valid_handle(meh0) && base_mesh_->is_boundary(bvh1)) {
    merge = false;
  }

  // Collapsing of a 2-loop can lead to a scenario where two neighboring base vertices are meta vertices
  // and also doubly connected. eg: A---B
  //                                 \ /
  //                                  C
  // with A and B being meta vertices and C not a meta vertex. The rules so far would allow collapsing
  // the CB halfedge, but this destroys the loop and should not be allowed before first removing the loop
  // in the meta mesh. Avoid this.
  // Conditions: 1. Collapsing into a meta vertex
  // 2: Collapsing from a meta edge
  // 3: The meta edge is also in one of the triangles of the collapsed edge eg. AC
  // 4: The other edge of that triangle belongs to a meta edge as well eg. AB
  // This specifically does not have to be a DIFFERENT meta edge but just ANY meta edge since otherwise a
  // self-edge of B going BA->AC->CB would be illegally collapsed.
  if (meta_mesh_->is_valid_handle(base_mesh_->property(bv_connection_, bvh1))               // C1
      && meta_mesh_->is_valid_handle(meh0)                                                  // C2
      && ((meta_mesh_->is_valid_handle(base_mesh_->property(bhe_connection_,
                                       base_mesh_->prev_halfedge_handle(bheh)))             // C3+4 left
          && meta_mesh_->is_valid_handle(base_mesh_->property(bhe_connection_,
                                         base_mesh_->next_halfedge_handle(bheh))))
        || (meta_mesh_->is_valid_handle(base_mesh_->property(bhe_connection_,               // C3+4 right
                                        base_mesh_->next_halfedge_handle(bheho)))
            && meta_mesh_->is_valid_handle(base_mesh_->property(bhe_connection_,
                                           base_mesh_->prev_halfedge_handle(bheho)))))) {
    merge = false;
  }


  // Other illegal collapses provided by OpenMesh; retry later in this case since these may be blocked by
  // collapsing order and collapseable later.
  if (!base_mesh_->is_collapse_ok(bheh)) {
    return bvh;
  }

  // This one should not be necessary, delete later and fix.
  /*
  if (base_mesh_->is_valid_handle(base_mesh_->property(bhe_connection_, bheh))) {
    merge = false;
  }
  */
  if (merge) {
    CollapseBaseHe(bheh);
  }
  return OpenMesh::PolyConnectivity::InvalidVertexHandle;
}

/*!
 * \brief Embedding::SplitBaseHe
 * Retain properties correctly while splitting halfedges and return the first half of the split
 * edge, also marks new vertices introduced by the split with a handle to the vertex to collapse
 * them into to undo the split.
 * \param bheh
 * \return the halfedge starting at the from_vertex of bheh and pointing towards the new vertex
 * resulting from the split.
 */
OpenMesh::HalfedgeHandle  Embedding::SplitBaseHe(OpenMesh::HalfedgeHandle bheh) {
  auto opp = base_mesh_->opposite_halfedge_handle(bheh);
  auto vertex_colors_pph = base_mesh_->vertex_colors_pph();
  auto vertex_color = base_mesh_->property(vertex_colors_pph,
                                           base_mesh_->from_vertex_handle(bheh));

  auto weight = base_mesh_->property(halfedge_weight_, bheh);
  bool lbound = base_mesh_->is_boundary(bheh);
  bool rbound = base_mesh_->is_boundary(base_mesh_->opposite_halfedge_handle(bheh));

  OpenMesh::HalfedgeHandle border0, border1, mborder0, mborder1;
  if (initial_triangulation_) {
    border0 = base_mesh_->property(bhe_border_, bheh);
    border1 = base_mesh_->property(bhe_border_, opp);
    mborder0 = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
    if (meta_mesh_->is_valid_handle(border0)) {
      if (meta_mesh_->property(mhe_border_, border0) == bheh) {
        mborder0 = bheh;
      }
    }
    mborder1 = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
    if (meta_mesh_->is_valid_handle(border1)) {
      if (meta_mesh_->property(mhe_border_, border1) == opp) {
        mborder1 = opp;
      }
    }
  }

  OpenMesh::HalfedgeHandle bhe_connection0 = base_mesh_->property(bhe_connection_, bheh);
  OpenMesh::HalfedgeHandle bhe_connection1 = base_mesh_->property(bhe_connection_, opp);
  OpenMesh::HalfedgeHandle next_heh0 = base_mesh_->property(next_heh_, bheh);
  OpenMesh::HalfedgeHandle next_heh1 = base_mesh_->property(next_heh_, opp);

  OpenMesh::HalfedgeHandle mhe_connection0 = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  if (meta_mesh_->is_valid_handle(bhe_connection0)){
    if (meta_mesh_->property(mhe_connection_, bhe_connection0) == bheh) {
      mhe_connection0 = bheh;
    }
  }
  OpenMesh::HalfedgeHandle mhe_connection1 = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  if (meta_mesh_->is_valid_handle(bhe_connection1)){
    if (meta_mesh_->property(mhe_connection_, bhe_connection1) == opp) {
      mhe_connection1 = opp;
    }
  }
  auto prev = base_mesh_->prev_halfedge_handle(bheh);

  bool backsplitadjust = (base_mesh_->property(bsplithandle_,
                          base_mesh_->from_vertex_handle(bheh))
                          == bheh);
  bool frontsplitadjust = (base_mesh_->property(bsplithandle_,
                          base_mesh_->to_vertex_handle(bheh))
                          == base_mesh_->opposite_halfedge_handle(bheh));


  TriMesh::Point splitpoint = (base_mesh_->point(base_mesh_->from_vertex_handle(bheh))
      + base_mesh_->point(base_mesh_->to_vertex_handle(bheh)) )/2.0;
  auto bvh = base_mesh_->split(base_mesh_->edge_handle(bheh), splitpoint);

  auto back = base_mesh_->next_halfedge_handle(prev);
  auto left = base_mesh_->next_halfedge_handle(back);
  auto front = base_mesh_->next_halfedge_handle(base_mesh_->opposite_halfedge_handle(left));
  auto right = base_mesh_->next_halfedge_handle(base_mesh_->opposite_halfedge_handle(front));
  assert (bvh == base_mesh_->to_vertex_handle(back));

  if (lbound) {
    front = base_mesh_->next_halfedge_handle(back);
    right = base_mesh_->next_halfedge_handle(base_mesh_->opposite_halfedge_handle(front));
    base_mesh_->set_boundary(back);
    base_mesh_->set_boundary(front);
    base_mesh_->set_halfedge_handle(bvh, front);
  }
  if (rbound) {
    base_mesh_->set_boundary(base_mesh_->opposite_halfedge_handle(back));
    base_mesh_->set_boundary(base_mesh_->opposite_halfedge_handle(front));
    base_mesh_->set_halfedge_handle(bvh, base_mesh_->opposite_halfedge_handle(back));
  }


  base_mesh_->property(bhe_connection_, back) = bhe_connection0;
  base_mesh_->property(bhe_connection_, base_mesh_->opposite_halfedge_handle(back))
      = bhe_connection1;
  base_mesh_->property(bhe_connection_, front) = bhe_connection0;
  base_mesh_->property(bhe_connection_, base_mesh_->opposite_halfedge_handle(front))
      = bhe_connection1;
  if (!lbound) {
    base_mesh_->property(bhe_connection_, left)
        = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
    base_mesh_->property(bhe_connection_, base_mesh_->opposite_halfedge_handle(left))
        = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  }
  if (!rbound) {
    base_mesh_->property(bhe_connection_, right)
        = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
    base_mesh_->property(bhe_connection_, base_mesh_->opposite_halfedge_handle(right))
        = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  }
  if (!lbound) {
    base_mesh_->property(next_heh_, left)
        = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
    base_mesh_->property(next_heh_, base_mesh_->opposite_halfedge_handle(left))
        = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  }
  if (!rbound) {
    base_mesh_->property(next_heh_, right)
        = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
    base_mesh_->property(next_heh_, base_mesh_->opposite_halfedge_handle(right))
        = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  }
  base_mesh_->property(halfedge_weight_, back)
      = weight;
  base_mesh_->property(halfedge_weight_, base_mesh_->opposite_halfedge_handle(back))
      = weight;
  base_mesh_->property(halfedge_weight_, front)
      = weight;
  base_mesh_->property(halfedge_weight_, base_mesh_->opposite_halfedge_handle(front))
      = weight;
  if (!lbound) {
    base_mesh_->property(halfedge_weight_, left)
        = 2.0*weight;
    base_mesh_->property(halfedge_weight_, base_mesh_->opposite_halfedge_handle(left))
        = 2.0*weight;
  }
  if (!rbound) {
    base_mesh_->property(halfedge_weight_, right)
        = 2.0*weight;
    base_mesh_->property(halfedge_weight_, base_mesh_->opposite_halfedge_handle(right))
        = 2.0*weight;
  }
  base_mesh_->property(bv_connection_, bvh)
      = OpenMesh::PolyConnectivity::InvalidVertexHandle;
  assert (!meta_mesh_->is_valid_handle(base_mesh_->property(bv_connection_, bvh)));
  base_mesh_->property(vertex_colors_pph, bvh) = vertex_color;

  if (backsplitadjust) {
    base_mesh_->property(bsplithandle_, base_mesh_->from_vertex_handle(back)) = back;
    base_mesh_->property(bsplithandle_, bvh) = front;
  } else if (frontsplitadjust) {
    base_mesh_->property(bsplithandle_, base_mesh_->to_vertex_handle(front))
        = base_mesh_->opposite_halfedge_handle(front);
    base_mesh_->property(bsplithandle_, bvh) = base_mesh_->opposite_halfedge_handle(back);
  } else {
    base_mesh_->property(bsplithandle_, bvh) = base_mesh_->opposite_halfedge_handle(back);
  }

  if (base_mesh_->is_valid_handle(mhe_connection0)) {
    meta_mesh_->property(mhe_connection_, bhe_connection0) = back;
  }
  if (base_mesh_->is_valid_handle(mhe_connection1)) {
    meta_mesh_->property(mhe_connection_, bhe_connection1)
        = base_mesh_->opposite_halfedge_handle(front);
  }
  if (base_mesh_->is_valid_handle(next_heh0) ||
      base_mesh_->is_valid_handle(next_heh1)) {
    base_mesh_->property(next_heh_, front) = next_heh0;
    base_mesh_->property(next_heh_, back) = front;
    base_mesh_->property(next_heh_, base_mesh_->opposite_halfedge_handle(back)) = next_heh1;
    base_mesh_->property(next_heh_, base_mesh_->opposite_halfedge_handle(front))
        = base_mesh_->opposite_halfedge_handle(back);
    if (base_mesh_->is_valid_handle(next_heh0)) {
      auto next = base_mesh_->opposite_halfedge_handle(next_heh0);
      base_mesh_->property(next_heh_, next) = base_mesh_->opposite_halfedge_handle(front);
    }
    if (base_mesh_->is_valid_handle(next_heh1)) {
      auto prev = base_mesh_->opposite_halfedge_handle(next_heh1);
      base_mesh_->property(next_heh_, prev) = back;
    }
  } else if (meta_mesh_->is_valid_handle(bhe_connection0)) {
    base_mesh_->property(next_heh_, back) = front;
    base_mesh_->property(next_heh_, base_mesh_->opposite_halfedge_handle(front))
        = base_mesh_->opposite_halfedge_handle(back);
    base_mesh_->property(next_heh_, base_mesh_->opposite_halfedge_handle(back))
        = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
    base_mesh_->property(next_heh_, front)
        = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  } else {
    base_mesh_->property(next_heh_, back)
        = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
    base_mesh_->property(next_heh_, base_mesh_->opposite_halfedge_handle(back))
        = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
    base_mesh_->property(next_heh_, front)
        = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
    base_mesh_->property(next_heh_, base_mesh_->opposite_halfedge_handle(front))
        = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  }

  if (!lbound) {
    auto v_left = base_mesh_->opposite_halfedge_handle(
          base_mesh_->next_halfedge_handle(left));
    base_mesh_->property(bhe_border_, left) = base_mesh_->property(bhe_border_, v_left);
    base_mesh_->property(bhe_border_, base_mesh_->opposite_halfedge_handle(left)) =
        base_mesh_->property(bhe_border_, base_mesh_->opposite_halfedge_handle(v_left));
  }

  if (!rbound) {
    auto v_right = base_mesh_->prev_halfedge_handle(
          base_mesh_->opposite_halfedge_handle(right));
    base_mesh_->property(bhe_border_, right) = base_mesh_->property(bhe_border_, v_right);
    base_mesh_->property(bhe_border_, base_mesh_->opposite_halfedge_handle(right)) =
        base_mesh_->property(bhe_border_, base_mesh_->opposite_halfedge_handle(v_right));
  }

  base_mesh_->property(bhe_border_, back) =
      OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  base_mesh_->property(bhe_border_, base_mesh_->opposite_halfedge_handle(back)) =
      OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;

  if (!meta_mesh_->is_valid_handle(bhe_connection0)) {
    assert(!meta_mesh_->is_valid_handle(bhe_connection1));
    assert(!IsSectorBorder(bvh));
    assert(ValidA_StarEdge(back,
                           OpenMesh::PolyConnectivity::InvalidHalfedgeHandle));
    assert(ValidA_StarEdge(base_mesh_->opposite_halfedge_handle(back),
                           OpenMesh::PolyConnectivity::InvalidHalfedgeHandle));
  }

  if (initial_triangulation_) {
    base_mesh_->property(bhe_border_, front) = border0;
    base_mesh_->property(bhe_border_, base_mesh_->opposite_halfedge_handle(front)) = border1;
    if (base_mesh_->is_valid_handle(mborder0)) {
      meta_mesh_->property(mhe_border_, border0) = front;
    }
    if (base_mesh_->is_valid_handle(mborder1)) {
      meta_mesh_->property(mhe_border_, border1) = base_mesh_->opposite_halfedge_handle(front);
    }

    base_mesh_->property(voronoiID_, bvh) = base_mesh_->property(voronoiID_,
               base_mesh_->from_vertex_handle(back));
    if (!meta_mesh_->is_valid_handle(bhe_connection0)) {
      assert(ValidA_StarEdge(front, border0));
      assert(ValidA_StarEdge(base_mesh_->opposite_halfedge_handle(front), border1));
    }
  }
  return back;
}

void Embedding::CollapseBaseHe(OpenMesh::HalfedgeHandle bheh) {
  // Ensure bheh is valid
  assert(base_mesh_->is_valid_handle(bheh));

  auto bvh0 = base_mesh_->from_vertex_handle(bheh);
  auto bheho = base_mesh_->opposite_halfedge_handle(bheh);

  bool lbound = base_mesh_->is_boundary(bheh);
  bool rbound = base_mesh_->is_boundary(bheho);

  // Ensure only vertices not from the original mesh are collapsed.
  assert (base_mesh_->property(bsplithandle_, bvh0) == bheh);

  // Adjust property pointers towards bheh if it lies on a meta edge
  AdjustPointersForBheCollapse(bheh);

  // Transfer properties and pointers over from redundant edges
  assert(base_mesh_->is_valid_handle(base_mesh_->prev_halfedge_handle(bheh)));
  assert(base_mesh_->is_valid_handle(base_mesh_->opposite_halfedge_handle(
                                     base_mesh_->next_halfedge_handle(bheh))));
  if (!lbound) {
    MergeProperties(base_mesh_->prev_halfedge_handle(bheh),
       base_mesh_->opposite_halfedge_handle(base_mesh_->next_halfedge_handle(bheh)));
  }
  if (debug_hard_stop_) return;
  assert(base_mesh_->is_valid_handle(base_mesh_->next_halfedge_handle(bheho)));
  assert(base_mesh_->is_valid_handle(base_mesh_->opposite_halfedge_handle(
                                     base_mesh_->prev_halfedge_handle(bheho))));
  if (!rbound){
    MergeProperties(base_mesh_->next_halfedge_handle(bheho),
       base_mesh_->opposite_halfedge_handle(base_mesh_->prev_halfedge_handle(bheho)));
  }
  if (debug_hard_stop_) return;

  base_mesh_->collapse(bheh);
}

/*!
 * \brief Embedding::LowLevelBaseCollapse reimplemented collapse method on the base mesh, not sure
 * if this is used at all? TODO: check if this needs deletion.
 * \param bheh
 */
void Embedding::LowLevelBaseCollapse(OpenMesh::HalfedgeHandle bheh) {
  // See PolyConnectivity::collapse and PolyConnectivity::collapse_edge
  OpenMesh::HalfedgeHandle  h  = bheh;
  OpenMesh::HalfedgeHandle  hn = base_mesh_->next_halfedge_handle(h);
  OpenMesh::HalfedgeHandle  hp = base_mesh_->prev_halfedge_handle(h);

  OpenMesh::HalfedgeHandle  o  = base_mesh_->opposite_halfedge_handle(h);
  OpenMesh::HalfedgeHandle  on = base_mesh_->next_halfedge_handle(o);
  OpenMesh::HalfedgeHandle  op = base_mesh_->prev_halfedge_handle(o);

  OpenMesh::FaceHandle      fh = base_mesh_->face_handle(h);
  OpenMesh::FaceHandle      fo = base_mesh_->face_handle(o);

  OpenMesh::VertexHandle    vh = base_mesh_->to_vertex_handle(h);
  OpenMesh::VertexHandle    vo = base_mesh_->to_vertex_handle(o);

  // halfedge -> vertex
  for (auto vih_it : base_mesh_->vih_range(vo)) {
    base_mesh_->set_vertex_handle(vih_it, vh);
  }

  // halfedge -> halfedge
  base_mesh_->set_next_halfedge_handle(hp, hn);
  base_mesh_->set_next_halfedge_handle(op, on);


  // face -> halfedge
  if (base_mesh_->is_valid_handle(fh))  {
    base_mesh_->set_halfedge_handle(fh, hn);
  }
  if (base_mesh_->is_valid_handle(fo))  {
    base_mesh_->set_halfedge_handle(fo, on);
  }


  // vertex -> halfedge
  if (base_mesh_->halfedge_handle(vh) == o)  {
    base_mesh_->set_halfedge_handle(vh, hn);
  }
  base_mesh_->adjust_outgoing_halfedge(vh);
  base_mesh_->set_isolated(vo);

  // delete stuff
  base_mesh_->status(base_mesh_->edge_handle(h)).set_deleted(true);
  base_mesh_->status(vo).set_deleted(true);
  if (base_mesh_->has_halfedge_status())
  {
    base_mesh_->status(h).set_deleted(true);
    base_mesh_->status(o).set_deleted(true);
  }

  // How to handle loops then?
}

/*!
 * \brief Embedding::AdjustPointersForBheCollapse lots of pointer operations to ensure a
 * base collapse operation does not disrupt the embedding
 * \param bheh
 */
void Embedding::AdjustPointersForBheCollapse(OpenMesh::HalfedgeHandle bheh) {
  auto bheho = base_mesh_->opposite_halfedge_handle(bheh);

  // move next_heh_ pointers and meta->base pointers if applicable
  if (base_mesh_->is_valid_handle(base_mesh_->property(next_heh_, bheh))
      && !base_mesh_->status(base_mesh_->property(next_heh_, bheh)).deleted()
      && base_mesh_->is_valid_handle(base_mesh_->property(next_heh_, bheho))
      && !base_mesh_->status(base_mesh_->property(next_heh_, bheho)).deleted()) {
    auto bhehn = base_mesh_->property(next_heh_, bheh);
    auto bhehno = base_mesh_->opposite_halfedge_handle(bhehn);
    auto bhehpo = base_mesh_->property(next_heh_, bheho);
    auto bhehp = base_mesh_->opposite_halfedge_handle(bhehpo);
    assert(base_mesh_->property(next_heh_, bhehp) == bheh);
    assert(base_mesh_->property(next_heh_, bhehno) == bheho);
    base_mesh_->property(next_heh_, bhehp) = bhehn;
    base_mesh_->property(next_heh_, bhehno) = bhehpo;
    assert(base_mesh_->property(bhe_connection_, bhehp)
           == base_mesh_->property(bhe_connection_, bhehn));
    assert(base_mesh_->property(bhe_connection_, bhehno)
           == base_mesh_->property(bhe_connection_, bhehpo));
  } else if (base_mesh_->is_valid_handle(base_mesh_->property(next_heh_, bheh))
             && !base_mesh_->status(base_mesh_->property(next_heh_, bheh)).deleted()) {
    assert(meta_mesh_->is_valid_handle(base_mesh_->property(bv_connection_,
               base_mesh_->from_vertex_handle(bheh))));
    meta_mesh_->property(mhe_connection_, base_mesh_->property(bhe_connection_, bheh))
        = base_mesh_->property(next_heh_, bheh);
    base_mesh_->property(next_heh_, base_mesh_->opposite_halfedge_handle(
                           base_mesh_->property(next_heh_, bheh)))
        = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  } else if (base_mesh_->is_valid_handle(base_mesh_->property(next_heh_, bheho))
             && !base_mesh_->status(base_mesh_->property(next_heh_, bheho)).deleted()) {
    assert(meta_mesh_->is_valid_handle(base_mesh_->property(bv_connection_,
               base_mesh_->to_vertex_handle(bheh))));
    meta_mesh_->property(mhe_connection_, base_mesh_->property(bhe_connection_, bheho))
        = base_mesh_->property(next_heh_, bheho);
    base_mesh_->property(next_heh_, base_mesh_->opposite_halfedge_handle(
                           base_mesh_->property(next_heh_, bheho)))
        = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  } else {
    // Sanity check; don't collapse 1 edge long meta edges.
    assert(!base_mesh_->is_valid_handle(base_mesh_->property(bhe_connection_, bheh)));
  }

  // bheh property deletion for visual debugging
  base_mesh_->property(next_heh_, bheh)
      = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  base_mesh_->property(next_heh_, bheho)
      = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  base_mesh_->property(bhe_connection_, bheh)
      = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  base_mesh_->property(bhe_connection_, bheho)
      = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;

  // TESTS
  // Traversal tests
  if (base_mesh_->is_valid_handle(base_mesh_->property(next_heh_, bheh))
      && base_mesh_->is_valid_handle(base_mesh_->property(next_heh_, bheho))) {
    assert(base_mesh_->property(next_heh_, base_mesh_->opposite_halfedge_handle(
           base_mesh_->property(next_heh_, bheh)))
           == base_mesh_->property(next_heh_, bheho));
    assert(base_mesh_->property(next_heh_, base_mesh_->opposite_halfedge_handle(
           base_mesh_->property(next_heh_, bheho)))
           == base_mesh_->property(next_heh_, bheh));
  } else if (base_mesh_->is_valid_handle(base_mesh_->property(next_heh_, bheh))) {
    assert(base_mesh_->property(next_heh_, base_mesh_->opposite_halfedge_handle(
           base_mesh_->property(next_heh_, bheh))) != bheho);
  } else if (base_mesh_->is_valid_handle(base_mesh_->property(next_heh_, bheho))) {
    assert(base_mesh_->property(next_heh_, base_mesh_->opposite_halfedge_handle(
           base_mesh_->property(next_heh_, bheho))) != bheh);
  }
  // Connection tests
  if (base_mesh_->is_valid_handle(base_mesh_->property(bhe_connection_, bheh))
      && !base_mesh_->is_valid_handle(base_mesh_->property(next_heh_, bheho))) {
    // Ensure new connectivity
    assert(meta_mesh_->property(mhe_connection_, base_mesh_->property(bhe_connection_, bheh))
           == base_mesh_->property(next_heh_, bheh));
    assert(!base_mesh_->is_valid_handle(base_mesh_->property(next_heh_,
            base_mesh_->opposite_halfedge_handle(
            base_mesh_->property(next_heh_, bheh)))));
  }
  if (base_mesh_->is_valid_handle(base_mesh_->property(bhe_connection_, bheho))
      && !base_mesh_->is_valid_handle(base_mesh_->property(next_heh_, bheh))) {
    // Ensure new connectivity
    assert(meta_mesh_->property(mhe_connection_, base_mesh_->property(bhe_connection_, bheho))
           == base_mesh_->property(next_heh_, bheho));
    assert(!base_mesh_->is_valid_handle(base_mesh_->property(next_heh_,
            base_mesh_->opposite_halfedge_handle(
            base_mesh_->property(next_heh_, bheho)))));
  }
  // bsplithandle tests
  assert(base_mesh_->property(bsplithandle_, base_mesh_->to_vertex_handle(bheh)) != bheho);
}

/*!
 * \brief Embedding::MergeProperties
  * Takes two edges and transfers the properties of and pointers
  * pointing to the first to the second if they're not empty.
 * \param bheh0
 * \param bheh1
 */
void Embedding::MergeProperties(OpenMesh::HalfedgeHandle bheh0,
                                OpenMesh::HalfedgeHandle bheh1) {
  auto bheh0o = base_mesh_->opposite_halfedge_handle(bheh0);
  auto bheh1o = base_mesh_->opposite_halfedge_handle(bheh1);
  auto nprop0 = base_mesh_->property(next_heh_, bheh0);
  auto nprop0o = base_mesh_->property(next_heh_, bheh0o);
  auto nprop1 = base_mesh_->property(next_heh_, bheh1);
  auto nprop1o = base_mesh_->property(next_heh_, bheh1o);
  auto bhcprop0 = base_mesh_->property(bhe_connection_, bheh0);
  auto bhcprop0o = base_mesh_->property(bhe_connection_, bheh0o);
  auto bhcprop1 = base_mesh_->property(bhe_connection_, bheh1);
  auto bhcprop1o = base_mesh_->property(bhe_connection_, bheh1o);
  auto wprop0 = base_mesh_->property(halfedge_weight_, bheh0);
  auto wprop0o = base_mesh_->property(halfedge_weight_, bheh0o);
  auto wprop1 = base_mesh_->property(halfedge_weight_, bheh1);
  auto wprop1o = base_mesh_->property(halfedge_weight_, bheh1o);
  auto mhcprop0 = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  auto mhcprop0o = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  auto mhcprop1 = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  auto mhcprop1o = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  if (meta_mesh_->is_valid_handle(bhcprop0)) {
    mhcprop0 = meta_mesh_->property(mhe_connection_, bhcprop0);
  }
  if (meta_mesh_->is_valid_handle(bhcprop0o)) {
    mhcprop0o = meta_mesh_->property(mhe_connection_, bhcprop0o);
  }
  if (meta_mesh_->is_valid_handle(bhcprop1)) {
    mhcprop1 = meta_mesh_->property(mhe_connection_, bhcprop1);
  }
  if (meta_mesh_->is_valid_handle(bhcprop1o)) {
    mhcprop1o = meta_mesh_->property(mhe_connection_, bhcprop1o);
  }
  auto bsprop0 = base_mesh_->property(bsplithandle_, base_mesh_->from_vertex_handle(bheh0));
  auto bsprop0o = base_mesh_->property(bsplithandle_, base_mesh_->to_vertex_handle(bheh0));
  // Sanity check
  assert((!(mhcprop0 == bheh0) && !(mhcprop0o == bheh0o))
         || (!(mhcprop1 == bheh1) && !(mhcprop1o == bheh1o)));
  if (meta_mesh_->is_valid_handle(bhcprop0)) {
    // more checks
    assert(meta_mesh_->is_valid_handle(bhcprop0o) && !meta_mesh_->is_valid_handle(bhcprop1)
           && !meta_mesh_->is_valid_handle(bhcprop1o));
    assert(!base_mesh_->is_valid_handle(nprop1) && !base_mesh_->is_valid_handle(nprop1o));
    // property merging
    // transfer bhe_connection properties
    base_mesh_->property(bhe_connection_, bheh1) = bhcprop0;
    base_mesh_->property(bhe_connection_, bheh1o) = bhcprop0o;

    // check the sides
    if (!base_mesh_->is_valid_handle(nprop0o)) {
      assert(mhcprop0 == bheh0);
      meta_mesh_->property(mhe_connection_, bhcprop0) = bheh1;
    // if this is not the end of the edge fix the next_heh pointers
    } else {
      assert(base_mesh_->is_valid_handle(nprop0o));
      base_mesh_->property(next_heh_, bheh1o) = nprop0o;
      base_mesh_->property(next_heh_, base_mesh_->opposite_halfedge_handle(nprop0o))
          = bheh1;
      assert(base_mesh_->property(bhe_connection_,
                                  base_mesh_->opposite_halfedge_handle(nprop0o))
             == base_mesh_->property(bhe_connection_, bheh1));
    }
    // repeat for the other side
    if (!base_mesh_->is_valid_handle(nprop0)) {
      assert(mhcprop0o == bheh0o);
      meta_mesh_->property(mhe_connection_, bhcprop0o) = bheh1o;
    } else {
      assert(base_mesh_->is_valid_handle(nprop0));
      base_mesh_->property(next_heh_, bheh1) = nprop0;
      base_mesh_->property(next_heh_, base_mesh_->opposite_halfedge_handle(nprop0))
          = bheh1o;
      assert(base_mesh_->property(bhe_connection_,
                                  base_mesh_->opposite_halfedge_handle(nprop0))
             == base_mesh_->property(bhe_connection_, bheh1o));
    }
  }
  // transfer bsplithandle properties
  if (bsprop0 == bheh0) {
    base_mesh_->property(bsplithandle_, base_mesh_->from_vertex_handle(bheh0)) = bheh1;
  }
  if (bsprop0o == bheh0o) {
    base_mesh_->property(bsplithandle_, base_mesh_->to_vertex_handle(bheh0)) = bheh1o;
  }

  assert(static_cast<int>(wprop0) == static_cast<int>(wprop0o));
  assert(static_cast<int>(wprop1) == static_cast<int>(wprop1o));
  base_mesh_->property(halfedge_weight_, bheh1) = std::min(wprop0, wprop1);
  base_mesh_->property(halfedge_weight_, bheh1o) = std::min(wprop0, wprop1);

  // bheh0 property deletion for visual debugging
  base_mesh_->property(next_heh_, bheh0)
      = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  base_mesh_->property(next_heh_, bheh0o)
      = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  base_mesh_->property(bhe_connection_, bheh0)
      = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  base_mesh_->property(bhe_connection_, bheh0o)
      = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;

  // TESTS:
  // Traversal tests
  if (base_mesh_->is_valid_handle(base_mesh_->property(next_heh_, bheh0))) {
    assert(base_mesh_->property(next_heh_, base_mesh_->opposite_halfedge_handle(
           base_mesh_->property(next_heh_, bheh0))) == bheh1o);
    assert(base_mesh_->property(next_heh_, bheh0)
           == base_mesh_->property(next_heh_, bheh1));
  }
  if (base_mesh_->is_valid_handle(base_mesh_->property(next_heh_, bheh0o))) {
    assert(base_mesh_->property(next_heh_, base_mesh_->opposite_halfedge_handle(
           base_mesh_->property(next_heh_, bheh0o))) == bheh1);
    assert(base_mesh_->property(next_heh_, bheh0o)
           == base_mesh_->property(next_heh_, bheh1o));
  }
  // Connection tests
  if (meta_mesh_->is_valid_handle(base_mesh_->property(bhe_connection_, bheh0))) {
    assert(base_mesh_->property(bhe_connection_, bheh0)
           == base_mesh_->property(bhe_connection_, bheh1));
    assert(base_mesh_->property(bhe_connection_, bheh0o)
           == base_mesh_->property(bhe_connection_, bheh1o));
  }
  if (!meta_mesh_->is_valid_handle(bhcprop0)
      && !meta_mesh_->is_valid_handle(bhcprop1)) {
    assert(!base_mesh_->is_valid_handle(base_mesh_->property(next_heh_, bheh1)));
    if (meta_mesh_->is_valid_handle(bhcprop0o)
        || meta_mesh_->is_valid_handle(bhcprop1o)) {
      assert(meta_mesh_->property(mhe_connection_,
             base_mesh_->property(bhe_connection_, bheh1o)) == bheh1o);
    }
  }
  if (!meta_mesh_->is_valid_handle(bhcprop0o)
      && !meta_mesh_->is_valid_handle(bhcprop1o)) {
    assert(!base_mesh_->is_valid_handle(base_mesh_->property(next_heh_, bheh1o)));
    if (meta_mesh_->is_valid_handle(bhcprop0)
        || meta_mesh_->is_valid_handle(bhcprop1)) {
      assert(meta_mesh_->property(mhe_connection_,
             base_mesh_->property(bhe_connection_, bheh1)) == bheh1);
    }
  }
  // bsplithandle tests
  assert(base_mesh_->property(bsplithandle_, base_mesh_->from_vertex_handle(bheh0)) != bheh0);
  assert(base_mesh_->property(bsplithandle_, base_mesh_->to_vertex_handle(bheh0)) != bheh0o);
  // existence tests
  assert(!base_mesh_->status(bheh0).deleted());
  assert(!base_mesh_->status(bheh0o).deleted());
  assert(!base_mesh_->status(bheh1).deleted());
  assert(!base_mesh_->status(bheh1o).deleted());
}

/*!
 * \brief Embedding::Delaunay delaunay triangulation
 * \param voronoidistance
 * \param to_heh
 * \param multiplicity_heh
 * \param type
 * \return true for success, false for failure
 */
bool Embedding::Delaunay(OpenMesh::VPropHandleT<double> voronoidistance,
                         OpenMesh::VPropHandleT<OpenMesh::HalfedgeHandle> to_heh,
                         OpenMesh::HPropHandleT<int> multiplicity_heh) {

  for (auto bvh : base_mesh_->vertices())  {
    base_mesh_->property(voronoiID_, bvh) = OpenMesh::PolyConnectivity::InvalidVertexHandle;
    base_mesh_->property(to_heh, bvh) = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
    base_mesh_->property(voronoidistance, bvh)=0.0;
  }

  for (auto bheh : base_mesh_->halfedges())  {
    base_mesh_->property(multiplicity_heh, bheh) = 0;
  }

  std::queue<OpenMesh::VertexHandle> metavhqueue;
  for (auto mvh : meta_mesh_->vertices()){
    //qDebug() << "Meta Vertex: " << mvh.idx();
    auto bvh = meta_mesh_->property(mv_connection_, mvh);
    base_mesh_->property(voronoiID_, bvh)=mvh;
    metavhqueue.push(bvh);
  }
  qDebug() << "Creating Voronoi Regions";
  NaiveVoronoi(metavhqueue, voronoidistance, to_heh);
  qDebug() << "Finished Voronoi Regions";

  for (auto fh : base_mesh_->faces()) {
    auto bheh0 = base_mesh_->halfedge_handle(fh);
    auto bheh1 = base_mesh_->next_halfedge_handle(bheh0);
    auto bheh2 = base_mesh_->next_halfedge_handle(bheh1);
    auto vh0 = base_mesh_->from_vertex_handle(bheh0);
    auto vh1 = base_mesh_->to_vertex_handle(bheh0);
    auto vh2 = base_mesh_->to_vertex_handle(bheh1);
    auto metavh0 = base_mesh_->property(voronoiID_, vh0);
    auto metavh1 = base_mesh_->property(voronoiID_, vh1);
    auto metavh2 = base_mesh_->property(voronoiID_, vh2);
    if (metavh0 != metavh1 && metavh0 != metavh2 && metavh1 != metavh2) {
      auto mheh0 = FindHalfedge(bheh0);
      auto mheh1 = FindHalfedge(bheh1);
      auto mheh2 = FindHalfedge(bheh2);
      auto mf = AddFace({metavh0, metavh1, metavh2}, {mheh0, mheh1, mheh2});
      SetFaceProperties(fh, mf);
      mheh0 = meta_mesh_->halfedge_handle(mf);
      mheh1 = meta_mesh_->next_halfedge_handle(mheh0);
      mheh2 = meta_mesh_->next_halfedge_handle(mheh1);

      SetBorderProperties(meta_mesh_->property(mhe_border_, mheh0), mheh0);
      SetBorderProperties(meta_mesh_->property(mhe_border_, mheh1), mheh1);
      SetBorderProperties(meta_mesh_->property(mhe_border_, mheh2), mheh2);
    }
  }
  qDebug() << "Done adding all basic faces.";

  qDebug() << "Applying mesh fixes.";
  if (meta_mesh_->n_vertices()>0) {
    std::vector<int> genus = VoronoiGenus();
    for (auto mvh : meta_mesh_->vertices()) {
      if (genus.at(static_cast<unsigned long>(mvh.idx())) != 1) {
        qDebug() << "Voronoi Region " << mvh.idx() << " has a euler characteristic of "
                 << genus.at(static_cast<unsigned long>(mvh.idx()))
                 << " perhaps you should choose different seed points.";
        return false;
      }
    }
  }

  if (boundaries_) {
    qDebug() << "Collecting boundary faces.";
    for (auto mvh : meta_mesh_->vertices()) {
      if (base_mesh_->is_boundary(meta_mesh_->property(mv_connection_, mvh))) {
        auto mheh = meta_mesh_->halfedge_handle(mvh);
        if (!meta_mesh_->is_valid_handle(mheh)) {
          TraverseBoundary(mvh);
        } else {
          auto mvhnext = meta_mesh_->to_vertex_handle(mheh);
          // Traverse the boundary if there is still an undetected boundary halfedge
          if (!base_mesh_->is_boundary(meta_mesh_->property(mv_connection_, mvhnext))) {
            TraverseBoundary(mvh);
          }
        }
      }
    }
  }

  if (boundaries_) {
    qDebug() << "Collecting Voronoi borders near boundaries.";
    for (auto bfh : base_mesh_->faces()) {
      std::list<int> neighbors;
      for (auto bfhiter : base_mesh_->ff_range(bfh)) {
        if (!neighbors.empty()
            && std::binary_search(neighbors.begin(), neighbors.end(), bfhiter.idx())) {
          qDebug() << "Found a pair of Voronoi regions with more than one border,"
                      " try triangulating again with a different configuration.";
          return false;
        }
        neighbors.push_back(bfhiter.idx());
      }
    }
    VoronoiBorders();
  }

  bool correct_voronoi_borders = true;
  for (auto bheh : base_mesh_->halfedges()) {
    if ((base_mesh_->property(voronoiID_, base_mesh_->from_vertex_handle(bheh))
        != base_mesh_->property(voronoiID_, base_mesh_->to_vertex_handle(bheh)))
        && !meta_mesh_->is_valid_handle(base_mesh_->property(bhe_border_, bheh))) {
        qDebug() << "Invalid border mheh "
                 << base_mesh_->property(bhe_border_, bheh).idx();
      base_mesh_->status(bheh).set_selected(true);
      correct_voronoi_borders = false;
      debug_hard_stop_ = true;
    }
  }
  if (!correct_voronoi_borders) {
    qDebug() << "border detection failed, exiting triangulation.";
    return false;
  }
  if (boundaries_) {
    AddBoundaries();
  }
  return true;
}

/*!
 * \brief Embedding::TraverseBoundary traverse the boundary that mvh lies on and add faces
 * along the boundary that were missed by the voronoi triangulation
 * \param mvh
 */
void Embedding::TraverseBoundary(OpenMesh::VertexHandle mvh) {
  assert(base_mesh_->is_boundary(meta_mesh_->property(mv_connection_, mvh)));
  OpenMesh::HalfedgeHandle bhehstart = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  // Exactly one outgoing halfedge should be a boundary
  for (auto bvoheh : base_mesh_->voh_range(meta_mesh_->property(mv_connection_, mvh))) {
    if (base_mesh_->is_boundary(bvoheh)) {
      assert(!base_mesh_->is_valid_handle(bhehstart));
      bhehstart = bvoheh;
    }
  }
  assert(base_mesh_->is_valid_handle(bhehstart));
  auto bhehcurr = bhehstart;
  std::vector<OpenMesh::VertexHandle> newfacev = {mvh};
  std::vector<OpenMesh::HalfedgeHandle> newfaceh = {};
  std::vector<OpenMesh::HalfedgeHandle> boundaryborders = {};
  // Traverse the boundary
  do {
    assert(base_mesh_->is_boundary(bhehcurr));
    auto currVID = newfacev.back();
    auto newVID = base_mesh_->property(voronoiID_, base_mesh_->to_vertex_handle(bhehcurr));
    // Stepping over a voronoi region border: Register the new region as part of the face
    if (newVID != currVID) {
      newfacev.push_back(newVID);
      auto mhehnew = FindHalfedge(bhehcurr);
      newfaceh.push_back(mhehnew);
      boundaryborders.push_back(bhehcurr);
    }
    // Stepping into a meta vertex: Complete the face
    if (meta_mesh_->is_valid_handle(base_mesh_->property(bv_connection_,
                                    base_mesh_->to_vertex_handle(bhehcurr)))) {
      assert(newVID == base_mesh_->property(bv_connection_,
                                            base_mesh_->to_vertex_handle(bhehcurr)));
      // Less than 3 means no new face was found
      if (newfacev.size() > 2) {
        newfaceh.push_back(OpenMesh::PolyConnectivity::InvalidHalfedgeHandle);
        auto mfh = AddFace(newfacev, newfaceh);
        CopyFaceProperties(mfh, boundaryborders);
        auto mheh = meta_mesh_->halfedge_handle(mfh);
        auto mhehiter = mheh;
        do {
          if (base_mesh_->is_valid_handle(meta_mesh_->property(mhe_border_, mhehiter))) {
            SetBorderProperties(meta_mesh_->property(mhe_border_, mhehiter), mhehiter);
          }
          mhehiter = meta_mesh_->next_halfedge_handle(mhehiter);
        } while (mhehiter != mheh);
      }
      // Start collecting the next face, starting with the current mvh
      newfacev.clear();
      newfaceh.clear();
      boundaryborders.clear();
      newfacev.push_back(newVID);
    }

    bhehcurr = base_mesh_->next_halfedge_handle(bhehcurr);
  } while (bhehcurr != bhehstart);
}

/*!
 * \brief Embedding::fact
 * \param n
 * \return n!
 */
int Embedding::fact(int n)
{
  return (n == 1 || n == 0) ? 1 : fact(n - 1) * n;
}

/*!
 * \brief Embedding::nCk
 * \param n
 * \param k
 * \return nCk(n,k)
 */
int Embedding::nCk(int n, int k) {
  return (fact(n)/(fact(k)*fact(n-k)));
}

/*!
 * \brief Embedding::VoronoiGenus
 * \return the vector with the geni (or euler characteristics? one of those)
 * of the voronoi regions
 */
std::vector<int> Embedding::VoronoiGenus() {
  // Initialize faces as 1 to make this work, disk topology
  std::vector<int> faces(meta_mesh_->n_vertices(), 0);
  std::vector<int> edges(meta_mesh_->n_vertices(), 0);
  std::vector<int> vertices(meta_mesh_->n_vertices(), 0);
  std::vector<int> genus;

  for (auto fh : base_mesh_->faces()) {
    auto heh0 = base_mesh_->halfedge_handle(fh);
    auto heh1 = base_mesh_->next_halfedge_handle(heh0);
    auto id0 = base_mesh_->property(voronoiID_, base_mesh_->from_vertex_handle(heh0));
    auto id1 = base_mesh_->property(voronoiID_, base_mesh_->to_vertex_handle(heh0));
    auto id2 = base_mesh_->property(voronoiID_, base_mesh_->to_vertex_handle(heh1));
    if (id0 == id1 && id1 == id2) {
      faces.at(static_cast<unsigned long>(id0.idx())) += 1;
    }
  }
  for (auto eh : base_mesh_->edges()) {
    auto vh0 = base_mesh_->from_vertex_handle(base_mesh_->halfedge_handle(eh, 0));
    auto vh1 = base_mesh_->to_vertex_handle(base_mesh_->halfedge_handle(eh, 0));
    auto id0 = base_mesh_->property(voronoiID_, vh0);
    auto id1 = base_mesh_->property(voronoiID_, vh1);
    if (id0 == id1) {
      edges.at(static_cast<unsigned long>(id0.idx())) += 1;
    }
  }
  for (auto vh : base_mesh_->vertices()) {
    auto id = base_mesh_->property(voronoiID_, vh);
    vertices.at(static_cast<unsigned long>(id.idx())) += 1;
  }
  for (unsigned long i=0; i<meta_mesh_->n_vertices(); ++i) {
    int g = vertices.at(i) - edges.at(i) + faces.at(i);
    genus.push_back(g);
  }
  return genus;
}

/*!
 * \brief Embedding::SetFaceProperties link a base face with a meta face (connect their
 * property handles etc.)
 * \param bf
 * \param mf
 * \param voronoidistance
 * \param to_heh
 * \param type
 */
void Embedding::SetFaceProperties(OpenMesh::FaceHandle bf,
                                  OpenMesh::FaceHandle mf) {
  auto bheh0 = base_mesh_->halfedge_handle(bf);
  auto bheh1 = base_mesh_->next_halfedge_handle(bheh0);
  auto bheh2 = base_mesh_->next_halfedge_handle(bheh1);
  auto mheh0 = meta_mesh_->halfedge_handle(mf);
  auto mheh1 = meta_mesh_->next_halfedge_handle(mheh0);
  auto mheh2 = meta_mesh_->next_halfedge_handle(mheh1);
  meta_mesh_->property(mhe_connection_, mheh0) = bheh0;
  meta_mesh_->property(mhe_connection_, mheh1) = bheh1;
  meta_mesh_->property(mhe_connection_, mheh2) = bheh2;
  meta_mesh_->property(mhe_border_, mheh0) = bheh0;
  meta_mesh_->property(mhe_border_, mheh1) = bheh1;
  meta_mesh_->property(mhe_border_, mheh2) = bheh2;
}

/*!
 * \brief Embedding::CopyFaceProperties
 * \param mfh
 * \param boundaryborders
 */
void Embedding::CopyFaceProperties(OpenMesh::FaceHandle mfh,
                std::vector<OpenMesh::HalfedgeHandle> boundaryborders) {
  auto mheh = meta_mesh_->halfedge_handle(mfh);
  auto mhehiter = mheh;
  uint borderctr = 0;
  uint mfval = meta_mesh_->valence(mfh);
  for (uint i=0; i<boundaryborders.size(); ++i) {
    auto bhehb = boundaryborders.at(i);
    assert(meta_mesh_->from_vertex_handle(mhehiter)
           == base_mesh_->property(voronoiID_, base_mesh_->from_vertex_handle(bhehb)));
    assert(meta_mesh_->to_vertex_handle(mhehiter)
           == base_mesh_->property(voronoiID_, base_mesh_->to_vertex_handle(bhehb)));
    mhehiter = meta_mesh_->next_halfedge_handle(mhehiter);
  }
  mhehiter = mheh;
  do {
    if (meta_mesh_->is_valid_handle(meta_mesh_->property(mhe_connection_,
                                    meta_mesh_->opposite_halfedge_handle(mhehiter)))) {
      meta_mesh_->property(mhe_connection_, mhehiter) = base_mesh_->opposite_halfedge_handle(
        meta_mesh_->property(mhe_connection_, meta_mesh_->opposite_halfedge_handle(mhehiter)));
    } else if (borderctr < mfval-1) {
      auto bhehb = boundaryborders.at(borderctr);
      assert(base_mesh_->property(voronoiID_, base_mesh_->from_vertex_handle(bhehb))
             == meta_mesh_->from_vertex_handle(mhehiter));
      assert(base_mesh_->property(voronoiID_, base_mesh_->to_vertex_handle(bhehb))
             == meta_mesh_->to_vertex_handle(mhehiter));
      meta_mesh_->property(mhe_connection_, mhehiter) = boundaryborders.at(borderctr);
    }
    if (meta_mesh_->is_valid_handle(meta_mesh_->property(mhe_border_,
                                    meta_mesh_->opposite_halfedge_handle(mhehiter)))) {
      meta_mesh_->property(mhe_border_, mhehiter) = base_mesh_->opposite_halfedge_handle(
        meta_mesh_->property(mhe_border_, meta_mesh_->opposite_halfedge_handle(mhehiter)));
    } else if (borderctr < mfval-1) {
      meta_mesh_->property(mhe_border_, mhehiter) = boundaryborders.at(borderctr);
    }
    assert(base_mesh_->is_valid_handle(meta_mesh_->property(mhe_connection_, mhehiter))
           || meta_mesh_->is_boundary(meta_mesh_->opposite_halfedge_handle(mhehiter)));
    ++borderctr;
    mhehiter = meta_mesh_->next_halfedge_handle(mhehiter);
  } while (mheh != mhehiter);
}

/*!
 * \brief Embedding::AddFace adds a meta face
 * \param mvh vertices
 * \param mheh halfedges, going mvh(i) -> mheh(i) -> mvh(i+1)
 * Use the opposite halfedges of adjacent faces for these, or an invalid handle if they don't exist
 * yet and in that case AddFace will make them.
 * \return the facehandle
 */
OpenMesh::FaceHandle Embedding::AddFace(std::vector<OpenMesh::VertexHandle> mvh,
                       std::vector<OpenMesh::HalfedgeHandle> mheh) {
  assert(mvh.size() == mheh.size());
  for (uint i = 0; i<mheh.size(); ++i) {
    if (!meta_mesh_->is_valid_handle(mheh.at(i))) {
      mheh.at(i) = meta_mesh_->new_edge(mvh.at(i), mvh.at((i+1)%mvh.size()));
    }
  }
  for (uint i = 0; i<mheh.size(); ++i) {
    assert(meta_mesh_->from_vertex_handle(mheh.at(i)) == mvh.at(i));
    assert(meta_mesh_->to_vertex_handle(mheh.at(i)) == mvh.at((i+1)%mvh.size()));
  }

  // consistent with mesh_->add_face()
  auto fnew(meta_mesh_->new_face());

  // Face->Halfedge pointer
  meta_mesh_->set_halfedge_handle(fnew, mheh.front());

  // Halfedge->Face Pointers
  for (uint i = 0; i<mheh.size(); ++i) {
    meta_mesh_->set_face_handle(mheh.at(i), fnew);
  }

  // Halfedge->Next Halfedge pointers
  for (uint i = 0; i<mheh.size(); ++i) {
    meta_mesh_->set_next_halfedge_handle(mheh.at(i), mheh.at((i+1)%mvh.size()));
  }

  // Halfedge->Vertex Pointers
  for (uint i = 0; i<mheh.size(); ++i) {
    meta_mesh_->set_vertex_handle(mheh.at(i), mvh.at((i+1)%mvh.size()));
  }

  // Vertex->Halfedge Pointers
  //meta_mesh_->set_isolated(mvh0);
  for (uint i = 0; i<mheh.size(); ++i) {
    auto mheho = meta_mesh_->opposite_halfedge_handle(mheh.at(i));
    if (!meta_mesh_->is_valid_handle(meta_mesh_->halfedge_handle(mvh.at(i)))
        || meta_mesh_->is_boundary(mheho)) {
      meta_mesh_->set_halfedge_handle(mvh.at((i+1)%mvh.size()), mheho);
    }
  }

  assert(meta_mesh_->is_valid_handle(meta_mesh_->halfedge_handle(fnew)));  
  for (uint i = 0; i<mheh.size(); ++i) {
    assert(meta_mesh_->is_valid_handle(meta_mesh_->next_halfedge_handle(mheh.at(i))));
    assert(meta_mesh_->is_valid_handle((meta_mesh_->halfedge_handle(mvh.at(i)))));
  }
  return fnew;
}

/*!
 * \brief Embedding::AddBoundaries set meta properties and pointers for boundaries.
 */
void Embedding::AddBoundaries() {
  for (auto mheh : meta_mesh_->halfedges()) {
    if (!meta_mesh_->is_valid_handle(meta_mesh_->next_halfedge_handle(mheh))
        && base_mesh_->is_boundary(base_mesh_->halfedge_handle(
           meta_mesh_->property(mv_connection_, meta_mesh_->from_vertex_handle(mheh))))) {
      meta_mesh_->set_boundary(mheh);
      meta_mesh_->set_next_halfedge_handle(mheh, meta_mesh_->halfedge_handle(
                                             meta_mesh_->to_vertex_handle(mheh)));
      meta_mesh_->set_halfedge_handle(meta_mesh_->from_vertex_handle(mheh), mheh);
    }
  }
}

/*!
 * \brief Embedding::FindHalfedge
 * \param bheh bheh lying on a mheh
 * \return the opposite meta halfedge of the meta halfedge bheh lies on, or nothing
 * if this is invalid
 */
OpenMesh::HalfedgeHandle Embedding::FindHalfedge(OpenMesh::HalfedgeHandle bheh) {
  auto oppmheh = base_mesh_->property(bhe_border_, base_mesh_->opposite_halfedge_handle(bheh));
  if (!meta_mesh_->is_valid_handle(oppmheh)) {
    return OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  } else {
    return meta_mesh_->opposite_halfedge_handle(oppmheh);
  }
}

/*!
 * \brief Embedding::VoronoiBorders iteration over all border regions
 */
void Embedding::VoronoiBorders() {
  // Considering the inclusion of boundaries in the meta mesh a local border traversal
  // becomes more complicated since it would involve iterating around boundaries.
  // Considering that, iterating over all base halfedges should be faster.
  // However this leaves us unable to determine which border between regions a halfedge
  // lies on if those two regions have two borders with each other.
  // So keep in mind that using a global solution requires forbidding initial triangulations
  // where two faces have more than 1 boundary to each other.
  for (auto mheh : meta_mesh_->halfedges()) {
    if (!meta_mesh_->is_boundary(mheh)
        && meta_mesh_->is_valid_handle(meta_mesh_->property(mhe_border_, mheh))) {
      SetBorderProperties(meta_mesh_->property(mhe_border_, mheh), mheh);
    }
  }
  if (boundaries_) {
    for (auto beh : base_mesh_->edges()) {
      auto bheh0 = base_mesh_->halfedge_handle(beh, 0);
        if (!base_mesh_->is_valid_handle(base_mesh_->property(bhe_border_, bheh0))) {
        auto bheh1 = base_mesh_->halfedge_handle(beh, 1);
        auto mvh0 = base_mesh_->property(voronoiID_, base_mesh_->from_vertex_handle(bheh0));
        auto mvh1 = base_mesh_->property(voronoiID_, base_mesh_->to_vertex_handle(bheh0));
        if (mvh0 != mvh1) {
          auto mheh0 = meta_mesh_->find_halfedge(mvh0, mvh1);
          if (!meta_mesh_->is_valid_handle(mheh0)) {
            qDebug() << "Didn't find a halfedge between mvh " << mvh0.idx()
                     << "and " << mvh1.idx();
          }
          auto mheh1 = meta_mesh_->opposite_halfedge_handle(mheh0);
          base_mesh_->property(bhe_border_, bheh0) = mheh0;
          base_mesh_->property(bhe_border_, bheh1) = mheh1;
        }
      }
    }
  }
}

/*!
 * \brief Embedding::SetBorderProperties iteration over one specific border region
 * TODO: improve this for complex borders, those may break this function at the current state.
 * \param bheh
 * \param mheh
 */
void Embedding::SetBorderProperties(OpenMesh::HalfedgeHandle bheh,
                               OpenMesh::HalfedgeHandle mheh) {
  assert(base_mesh_->is_valid_handle(bheh));
  if (base_mesh_->property(bhe_border_, bheh) == mheh) {
    return;
  }
  base_mesh_->property(bhe_border_, bheh) = mheh;
  base_mesh_->property(bhe_border_, base_mesh_->opposite_halfedge_handle(bheh))
      = meta_mesh_->opposite_halfedge_handle(mheh);
  auto bheh0 = base_mesh_->next_halfedge_handle(base_mesh_->opposite_halfedge_handle(bheh));
  auto bheh1 = base_mesh_->next_halfedge_handle(bheh0);
  auto mv0 = meta_mesh_->from_vertex_handle(mheh);
  auto mv1 = meta_mesh_->to_vertex_handle(mheh);
  auto bvIDprop0 = base_mesh_->property(voronoiID_, base_mesh_->from_vertex_handle(bheh));
  auto bvIDprop1 = base_mesh_->property(voronoiID_, base_mesh_->to_vertex_handle(bheh));
  assert(mv0 == bvIDprop0);
  assert(mv1 == bvIDprop1);
  if (base_mesh_->property(voronoiID_, base_mesh_->from_vertex_handle(bheh0)) == mv0
      && base_mesh_->property(voronoiID_, base_mesh_->to_vertex_handle(bheh0)) == mv1) {
    SetBorderProperties(bheh0, mheh);
  } else if (base_mesh_->property(voronoiID_, base_mesh_->from_vertex_handle(bheh1)) == mv0
      && base_mesh_->property(voronoiID_, base_mesh_->to_vertex_handle(bheh1)) == mv1) {
    SetBorderProperties(bheh1, mheh);
  }
}

/*!
 * \brief Embedding::A_StarTriangulation
 * \param type
 */
void Embedding::A_StarTriangulation() {
  for (auto mheh : meta_mesh_->halfedges()) {
    if (meta_mesh_->is_boundary(mheh)
        || meta_mesh_->is_boundary(meta_mesh_->opposite_halfedge_handle(mheh))) {
      if (debug_hard_stop_)
        return;
      Trace(mheh, true, false);
    }
  }
  for (auto mheh : meta_mesh_->halfedges()) {
    if (!(meta_mesh_->is_boundary(mheh)
        || meta_mesh_->is_boundary(meta_mesh_->opposite_halfedge_handle(mheh)))) {
      if (base_mesh_->property(bhe_connection_, meta_mesh_->property(mhe_connection_, mheh))
          != mheh) {
        if (!meta_mesh_->is_boundary(meta_mesh_->from_vertex_handle(mheh))
            && !meta_mesh_->is_boundary(meta_mesh_->to_vertex_handle(mheh))) {
          if (debug_hard_stop_)
            return;
          Trace(mheh, true, false);
          assert(base_mesh_->is_valid_handle(meta_mesh_->property(mhe_connection_, mheh)));
        }
      }
    }
  }
  // Triangulate non-triangular faces
  if (boundaries_) {
    // switch initial_triangulation_ off temporarily or correct paths can't be found
    initial_triangulation_ = false;
    for (auto mheh : meta_mesh_->halfedges()) {
      if (!(meta_mesh_->is_boundary(mheh)
          || meta_mesh_->is_boundary(meta_mesh_->opposite_halfedge_handle(mheh)))) {
        if (base_mesh_->property(bhe_connection_, meta_mesh_->property(mhe_connection_, mheh))
            != mheh) {
          if (meta_mesh_->is_boundary(meta_mesh_->from_vertex_handle(mheh))
              || meta_mesh_->is_boundary(meta_mesh_->to_vertex_handle(mheh))) {
            if (debug_hard_stop_)
              return;
            Trace(mheh, false, false);
          }
        }
      }
    }
    for (auto mfh : meta_mesh_->faces()) {
      if (meta_mesh_->valence(mfh) != 3) {
        auto mhehstart = meta_mesh_->halfedge_handle(mfh);
        auto mheh0 = meta_mesh_->next_halfedge_handle(
              meta_mesh_->next_halfedge_handle(mhehstart));
        auto mheh1 = meta_mesh_->next_halfedge_handle(mheh0);
        while (mheh1 != mhehstart) {
          auto mhehnew = AddMetaEdge(mhehstart, mheh0);
          Trace(mhehnew, false, false);
          mheh0 = mheh1;
          mheh1 = meta_mesh_->next_halfedge_handle(mheh1);
        }
      }
    }
    initial_triangulation_ = true;
  }

  base_mesh_->update_normals();
}

/*!
 * \brief Embedding::NaiveVoronoi build voronoi regions
 * \param queue
 * \param voronoidistance
 * \param to_heh
 */
void Embedding::NaiveVoronoi(std::queue<OpenMesh::VertexHandle> queue,
                             OpenMesh::VPropHandleT<double> voronoidistance,
                             OpenMesh::VPropHandleT<OpenMesh::HalfedgeHandle> to_heh) {
  while (!queue.empty()) {
    auto vh = queue.front();
    for (auto vvh : base_mesh_->vv_range(vh)){
      if (!meta_mesh_->is_valid_handle(base_mesh_->property(voronoiID_, vvh))) {
        base_mesh_->property(voronoiID_, vvh) = base_mesh_->property(voronoiID_, vh);
        base_mesh_->property(voronoidistance, vvh) = base_mesh_->property(voronoidistance, vh) +
            base_mesh_->calc_edge_length(base_mesh_->find_halfedge(vh,vvh));
        base_mesh_->property(to_heh, vvh) = base_mesh_->find_halfedge(vh,vvh);
        queue.push(vvh);
      } else  {
        auto newdist = base_mesh_->property(voronoidistance, vh) +
            base_mesh_->calc_edge_length(base_mesh_->find_halfedge(vh,vvh));
        if (newdist < base_mesh_->property(voronoidistance, vvh)) {
          base_mesh_->property(voronoidistance, vvh) = newdist;
          base_mesh_->property(voronoiID_, vvh) = base_mesh_->property(voronoiID_, vh);
          base_mesh_->property(to_heh, vvh) = base_mesh_->find_halfedge(vh,vvh);
          queue.push(vvh);
          if (base_mesh_->property(voronoiID_, vvh) != base_mesh_->property(voronoiID_, vh)) {
            base_mesh_->property(voronoiID_, vvh) = base_mesh_->property(voronoiID_, vh);
          }
        }
      }
    }
    queue.pop();
  }

  ColorizeVoronoiRegions();
}

/*!
 * \brief Embedding::GetMetaEdge
 * \param bvh
 * \return the meta edge bvh lies on or InvalidHandle if it lies on none
 */
OpenMesh::EdgeHandle Embedding::GetMetaEdge(OpenMesh::VertexHandle bvh) {
  if(meta_mesh_->is_valid_handle(base_mesh_->property(bv_connection_, bvh))) {
    //qDebug() << "Called GetMetaEdge on a Meta Vertex";
    return OpenMesh::PolyConnectivity::InvalidEdgeHandle;
  }
  for (auto bheh : base_mesh_->voh_range(bvh)) {
    auto mheh = base_mesh_->property(bhe_connection_, bheh);
    if (meta_mesh_->is_valid_handle(mheh)) {
      return meta_mesh_->edge_handle(mheh);
    }
  }
  return OpenMesh::PolyConnectivity::InvalidEdgeHandle;
}

/*!
 * \brief Embedding::MetaHalfedgeWeight
 * \param mheh
 * \return the log of the product of edge weights of the base halfedges in mheh
 */
double Embedding::MetaHalfedgeWeight(OpenMesh::HalfedgeHandle mheh) {
  double weight = 0.0;
  std::vector<OpenMesh::HalfedgeHandle> hes = GetBaseHalfedges(mheh);
  for (auto bheh : hes) {
    weight += base_mesh_->property(halfedge_weight_, bheh);
  }
  return weight;
}

/*!
 * \brief Embedding::MetaVertexWeight
 * \param mvh
 * \return Vertex weight based on halfedge weight of outgoing base halfedges
 */
double Embedding::MetaVertexWeight(OpenMesh::VertexHandle mvh) {
  double weight = 0.0;
  for (auto moh : meta_mesh_->voh_range(mvh)) {
    auto bheh = meta_mesh_->property(mhe_connection_, moh);
    weight += base_mesh_->property(halfedge_weight_, bheh);
  }
  return weight;
}

/*!
 * \brief Embedding::CalculateEdgeLength
 * \param mheh
 * \return edge length of mheh
 */
double Embedding::CalculateEdgeLength(OpenMesh::HalfedgeHandle mheh) {
  assert(meta_mesh_->is_valid_handle(mheh));
  auto bheh = meta_mesh_->property(mhe_connection_, mheh);
  assert(base_mesh_->is_valid_handle(bheh));
  double length = base_mesh_->calc_edge_length(bheh);
  bheh = base_mesh_->property(next_heh_, bheh);
  while (base_mesh_->is_valid_handle(bheh)) {
    length += base_mesh_->calc_edge_length(bheh);
    bheh = base_mesh_->property(next_heh_, bheh);
  }
  return length;
}

/*!
 * \brief Embedding::CalculateEdgeLength
 * \param meh
 * \return edge length of meh
 */
double Embedding::CalculateEdgeLength(OpenMesh::EdgeHandle meh) {
  return CalculateEdgeLength(meta_mesh_->halfedge_handle(meh, 0));
}

/*!
 * \brief Embedding::CalculateFlippedEdgeLength
 * Calculates the length mheh has after rotation.
 * This method is only safe if the incident faces of mheh are triangles
 * mheh will also be retraced in the process (avoiding this would be difficult
 * without changing a lot of code and removing base mesh cleanup operations)
 * \param mheh
 * \return length of flipped mheh
 */
double Embedding::CalculateFlippedEdgeLength(OpenMesh::HalfedgeHandle mheh) {
  Rotate(meta_mesh_->edge_handle(mheh));
  double length = CalculateEdgeLength(mheh);
  Rotate(meta_mesh_->edge_handle(mheh));

  return length;
}

/*!
 * \brief Embedding::CalculateFlippedEdgeLength
 * \param meh
 * \return length of flipped mehs
 */
double Embedding::CalculateFlippedEdgeLength(OpenMesh::EdgeHandle meh) {
  return CalculateFlippedEdgeLength(meta_mesh_->halfedge_handle(meh, 0));
}

/*!
 * \brief Embedding::CleanMetaMesh
 * Delete all meta mesh face handles with valence <3 so that OpenFlipper can display it
 * Not recommended outside of debugging since behavior afterwards is undefined. This only
 * serves for visualization of strange edge cases.
 */
void Embedding::CleanMetaMesh() {
  for (auto mfh : meta_mesh_->faces()) {
    auto mheh = meta_mesh_->halfedge_handle(mfh);
    auto mvh0 = meta_mesh_->from_vertex_handle(mheh);
    auto mvh1 = meta_mesh_->to_vertex_handle(mheh);
    auto mvh2 = meta_mesh_->to_vertex_handle(meta_mesh_->next_halfedge_handle(mheh));
    if (mvh0 == mvh1) {
      meta_mesh_->status(mfh).set_deleted(true);
      meta_mesh_->set_face_handle(mheh, OpenMesh::PolyConnectivity::InvalidFaceHandle);
    } else if (mvh0 == mvh2) {
      meta_mesh_->status(mfh).set_deleted(true);
      meta_mesh_->set_face_handle(mheh, OpenMesh::PolyConnectivity::InvalidFaceHandle);
      meta_mesh_->set_face_handle(meta_mesh_->next_halfedge_handle(mheh)
                                  , OpenMesh::PolyConnectivity::InvalidFaceHandle);
    }
  }
  for (auto mheh : meta_mesh_->halfedges()) {
    if (!meta_mesh_->is_valid_handle(meta_mesh_->face_handle(mheh))) {
      meta_mesh_->set_boundary(mheh);
    }
  }
}

/*!
 * \brief Embedding::MetaGarbageCollection
 * \param vh_update
 * \param heh_update
 * \param fh_update
 */
void Embedding::MetaGarbageCollection(std::vector<OpenMesh::VertexHandle*> vh_update,
                                  std::vector<OpenMesh::HalfedgeHandle*> heh_update,
                                  std::vector<OpenMesh::FaceHandle*> fh_update)
{
  for (const auto& bheh : base_mesh_->halfedges()) {
    if (meta_mesh_->is_valid_handle(base_mesh_->property(bhe_connection_, bheh))) {
      heh_update.push_back(&base_mesh_->property(bhe_connection_, bheh));
    }
  }
  for (const auto& bvh : base_mesh_->vertices()) {
    if (meta_mesh_->is_valid_handle(base_mesh_->property(bv_connection_, bvh))) {
      vh_update.push_back(&base_mesh_->property(bv_connection_, bvh));
    }
  }

  meta_mesh_->garbage_collection(vh_update, heh_update, fh_update);
}

/*!
 * \brief Embedding::BaseGarbageCollection
 * \param vh_update
 * \param heh_update
 * \param fh_update
 */
void Embedding::BaseGarbageCollection(std::vector<OpenMesh::VertexHandle*> vh_update,
                                      std::vector<OpenMesh::HalfedgeHandle*> heh_update,
                                      std::vector<OpenMesh::FaceHandle*> fh_update)
{
  OpenMesh::VPropHandleT<unsigned long> bsplithandle_temp;
  OpenMesh::HPropHandleT<unsigned long> next_heh_temp;
  std::vector<OpenMesh::HalfedgeHandle> bsplithandles(base_mesh_->n_vertices());
  std::vector<OpenMesh::HalfedgeHandle> next_hehs(base_mesh_->n_halfedges());
  base_mesh_->add_property(bsplithandle_temp, "Temporary property for storing vertex "
      "indices for the bsplithandle_ property since the garbage collection is useless");
  base_mesh_->add_property(next_heh_temp, "Temporary property for storing vertex "
      "indices for the next_heh_ property since the garbage collection is useless");
  for (const auto& bvh : base_mesh_->vertices()) {
    auto bsprop = base_mesh_->property(bsplithandle_, bvh);
    //assert(base_mesh_->is_valid_handle(bsprop)
    //       || !bsprop.is_valid());
    unsigned long index = static_cast<unsigned long>(bvh.idx());
    bsplithandles.at(index) = bsprop;
    base_mesh_->property(bsplithandle_temp, bvh) = index;
    if (bsprop != OpenMesh::PolyConnectivity::InvalidHalfedgeHandle) {
      heh_update.push_back(&bsplithandles.at(index));
    }
  }
  for (const auto& bheh : base_mesh_->halfedges()) {
    auto nhprop = base_mesh_->property(next_heh_, bheh);
    //assert(base_mesh_->is_valid_handle(nhprop)
    //       || !nhprop.is_valid());
    unsigned long index = static_cast<unsigned long>(bheh.idx());
    next_hehs.at(index) = nhprop;
    base_mesh_->property(next_heh_temp, bheh) = index;
    if (nhprop != OpenMesh::PolyConnectivity::InvalidHalfedgeHandle) {
      heh_update.push_back(&next_hehs.at(index));
    }
  }

  for (const auto& mheh : meta_mesh_->halfedges()) {
    if (base_mesh_->is_valid_handle(meta_mesh_->property(mhe_connection_, mheh))) {
      heh_update.push_back(&meta_mesh_->property(mhe_connection_, mheh));
    }
  }
  for (const auto& mvh : meta_mesh_->vertices()) {
    if (base_mesh_->is_valid_handle(meta_mesh_->property(mv_connection_, mvh))) {
      vh_update.push_back(&meta_mesh_->property(mv_connection_, mvh));
    }
  }

  base_mesh_->garbage_collection(vh_update, heh_update, fh_update);

  // Write bsplithandles and next_hehs back into their properties.
  for (const auto& bvh : base_mesh_->vertices()) {
    base_mesh_->property(bsplithandle_, bvh) = bsplithandles.at(
          base_mesh_->property(bsplithandle_temp, bvh));
    //assert(base_mesh_->is_valid_handle(base_mesh_->property(bsplithandle_, bvh))
    //       || !base_mesh_->property(bsplithandle_, bvh).is_valid());
  }
  for (const auto& bheh : base_mesh_->halfedges()) {
    base_mesh_->property(next_heh_, bheh) = next_hehs.at(
          base_mesh_->property(next_heh_temp, bheh));
    //assert(base_mesh_->is_valid_handle(base_mesh_->property(next_heh_, bheh))
    //       || !base_mesh_->property(next_heh_, bheh).is_valid());
  }
  base_mesh_->remove_property(bsplithandle_temp);
  base_mesh_->remove_property(next_heh_temp);
}

/*!
 * \brief Embedding::ColorizeMetaMesh
 */
void Embedding::ColorizeMetaMesh() {
  /*/ enable this to mark all meta mesh vertices in the base mesh
  ColorizeMetaMeshVertices();
  //*/
  ColorizeMetaEdges();
  /*/ enable this to mark all voronoi borders in the base mesh
  ColorizeBorders();
  //*/
}

/*!
 * \brief Embedding::ColorizeMetaMeshVertices
 */
void Embedding::ColorizeMetaMeshVertices() {
  if (!debug_hard_stop_) {
    for (auto bvh : base_mesh_->vertices()) {
      base_mesh_->status(bvh).set_selected(false);
    }
  }
  for (auto mvh : meta_mesh_->vertices()) {
    auto bvh = meta_mesh_->property(mv_connection_, mvh);
    base_mesh_->status(bvh).set_selected(true);
  }
}

/*!
 * \brief Embedding::ColorizeMetaEdges
 */
void Embedding::ColorizeMetaEdges() {
  //*
  draw_.clear();
  auto colorgenerator = new ACG::HaltonColors();
  auto numcolors = meta_mesh_->n_edges();
  if (numcolors != 0) {
    std::vector<ACG::Vec4f> colors;
    colorgenerator->generateNextNColors(static_cast<int>(numcolors)
                                        , std::back_inserter(colors));
    for (auto meh : meta_mesh_->edges()) {
      //qDebug() << meh.idx() << " < " << numcolors;
      auto mheh = meta_mesh_->halfedge_handle(meh, 0);
      auto frompoint = meta_mesh_->point(meta_mesh_->from_vertex_handle(mheh));
      auto topoint = meta_mesh_->point(meta_mesh_->to_vertex_handle(mheh));
      draw_.line(frompoint, topoint, colors.at(
                   static_cast<unsigned long>(meh.idx())));
    }
    for (auto beh : base_mesh_->edges()) {
      auto bheh = base_mesh_->halfedge_handle(beh, 0);
      auto mheh = base_mesh_->property(bhe_connection_, bheh);
      if (meta_mesh_->is_valid_handle(mheh)) {
        auto meh = meta_mesh_->edge_handle(mheh);
        //qDebug() << meh.idx() << " < " << numcolors;
        assert(base_mesh_->is_valid_handle(base_mesh_->from_vertex_handle(bheh)));
        assert(base_mesh_->is_valid_handle(base_mesh_->to_vertex_handle(bheh)));
        auto frompoint = base_mesh_->point(base_mesh_->from_vertex_handle(bheh));
        auto topoint = base_mesh_->point(base_mesh_->to_vertex_handle(bheh));
        draw_.line(frompoint, topoint, colors.at(
                     static_cast<unsigned long>(meh.idx())));
      }
    }
  }
  /*/ Alternative: selection instead of colorizing.
  for (auto bheh : base_mesh_->halfedges()) {
    base_mesh_->status(bheh).set_selected(false);
  }
  for (auto bheh : base_mesh_->halfedges()) {
    if (meta_mesh_->is_valid_handle(base_mesh_->property(bhe_connection_, bheh))
      base_mesh_->status(bheh).set_selected(true);
  }
  //*/
}

/*!
 * \brief Embedding::ColorizeVoronoiRegions
 */
void Embedding::ColorizeVoronoiRegions() {
  auto vertex_colors_pph = base_mesh_->vertex_colors_pph();
  auto colorgenerator = new ACG::HaltonColors();
  auto numcolors = meta_mesh_->n_vertices();
  if (numcolors != 0) {
    std::vector<ACG::Vec4f> colors;
    colorgenerator->generateNextNColors(static_cast<int>(numcolors)
                                        , std::back_inserter(colors));
    for (auto vh : base_mesh_->vertices()) {
      auto voronoiregion = base_mesh_->property(voronoiID_, vh).idx();
      base_mesh_->property(vertex_colors_pph, vh) =
          colors.at(static_cast<unsigned long>(voronoiregion));
    }
  }
}

/*!
 * \brief Embedding::ColorizeBorders
 */
void Embedding::ColorizeBorders() {
  /* Comments: Switch the code to these to colorize with haltoncolors instead of selecting.
  auto halfedge_colors_pph = base_mesh_->halfedge_colors_pph();
  auto colorgenerator = new ACG::HaltonColors();
  */
  auto numcolors = meta_mesh_->n_halfedges();
  if (numcolors != 0) {
    /*
    std::vector<ACG::Vec4f> colors;
    colorgenerator->generateNextNColors(static_cast<int>(numcolors)
                                        , std::back_inserter(colors));
                                        */
    for (auto heh : base_mesh_->halfedges()) {
      if (meta_mesh_->is_valid_handle(base_mesh_->property(bhe_border_, heh))) {
        /*
        auto border = base_mesh_->property(bhe_border_, heh).idx();
        base_mesh_->property(halfedge_colors_pph, heh) =
            colors.at(static_cast<unsigned long>(border));
            */
        base_mesh_->status(heh).set_selected(true);
      } else {
        /*
        base_mesh_->property(halfedge_colors_pph, heh) =
            ACG::Vec4f(0.0f, 0.0f, 0.0f, 1.0f);
            */
        //base_mesh_->status(heh).set_selected(false);
      }
    }
  }
}

/*!
 * \brief Embedding::TestHalfedgeConsistency expensive test, will also only work in debug mode.
 * Could probably be deleted but I'll leave it here just in case.
 */
void Embedding::TestHalfedgeConsistency() {
  for (auto eh : base_mesh_->edges()) {
    auto heh0 = base_mesh_->halfedge_handle(eh, 0);
    auto heh1 = base_mesh_->halfedge_handle(eh, 1);
    bool halfedgeconnectedprop0 =
        (meta_mesh_->is_valid_handle(base_mesh_->property(bhe_connection_, heh0)));
    bool halfedgeconnectedprop1 =
        (meta_mesh_->is_valid_handle(base_mesh_->property(bhe_connection_, heh1)));
    assert(halfedgeconnectedprop0 == halfedgeconnectedprop1);
  }
}

/*!
 * \brief Embedding::Trace Most important function: Traces a meta edge into the base mesh
 * \param mheh the meta edge to be traced
 * \param cleanup switch: cleanup after tracing or not
 * \param mark_trace switch: mark the trace or not
 * \param facetype preprocessedface or not? Current implementation calls the pre-processing
 * in Trace itself. UNPROCESSEDFACE should call a trace method that does its own base edge
 * splits where needed: this is not implemented yet.
 * TODO: Implement Advanced trace or remove the switch.
 */
void Embedding::Trace(OpenMesh::HalfedgeHandle mheh, bool cleanup, bool mark_trace,
                      TraceFaceAttr facetype)  {
  auto bheh = meta_mesh_->property(mhe_connection_, mheh);
  if (base_mesh_->is_valid_handle(bheh)
      && base_mesh_->property(bhe_connection_, bheh) == mheh) {
    return;
  }

  if (meta_mesh_->is_boundary(mheh)
      || meta_mesh_->is_boundary(meta_mesh_->opposite_halfedge_handle(mheh))) {
    BoundaryTrace(mheh);
  } else {
    switch (facetype) {
      case UNPROCESSEDFACE :
        AdvancedTrace(mheh);
        break;
      case PREPROCESSEDFACE :
        SimpleTrace(mheh, cleanup, mark_trace);
        break;
    }
  }
}

/*!
 * \brief Embedding::Retrace
 * Retraces the given meta halfedge
 * \param mheh
 * \param cleanup
 * \param mark_trace
 * \param facetype
 */
void Embedding::Retrace(OpenMesh::HalfedgeHandle mheh, bool cleanup, bool mark_trace,
                        TraceFaceAttr facetype) {
  if (!meta_mesh_->is_valid_handle(mheh)
      || meta_mesh_->status(mheh).deleted()) {
    qDebug() << "Invalid or deleted halfedge mheh cannot be retraced";
    return;
  }
  RemoveMetaEdgeFromBaseMesh(mheh);
  Trace(mheh, cleanup, mark_trace, facetype);
}

/*!
 * \brief Embedding::BoundaryTrace Trace a boundary edge. This is a special case but also
 * much easier since the meta edge must then also lie on the boundary by definition.
 * \param mheh
 * \param mark_trace
 */
void Embedding::BoundaryTrace(OpenMesh::HalfedgeHandle mheh, bool mark_trace) {
  //qDebug() << "Tracing boundary halfedge mheh " << mheh.idx();
  // align the edge along the boundary
  if (!meta_mesh_->is_boundary(mheh)) {
    mheh = meta_mesh_->opposite_halfedge_handle(mheh);
  }
  assert(meta_mesh_->is_boundary(mheh));

  auto mvh0 = meta_mesh_->from_vertex_handle(mheh);
  auto mvh1 = meta_mesh_->to_vertex_handle(mheh);
  auto bvh0 = meta_mesh_->property(mv_connection_, mvh0);
  auto bvh1 = meta_mesh_->property(mv_connection_, mvh1);

  auto bheh = base_mesh_->halfedge_handle(bvh0);
  assert(base_mesh_->is_boundary(bheh));

  if (meta_mesh_->is_valid_handle(base_mesh_->property(bhe_connection_, bheh))) {
    qDebug() << "Boundary mheh" << mheh.idx() << " is already traced or blocked by "
                                                 " another edge.";
    return;
  }

  if (mark_trace) {
    base_mesh_->status(bheh).set_selected(true);
  }
  std::vector<OpenMesh::HalfedgeHandle> path = {bheh};
  while (!meta_mesh_->is_valid_handle(base_mesh_->property(bv_connection_,
                                     base_mesh_->to_vertex_handle(bheh)))) {
    bheh = base_mesh_->next_halfedge_handle(bheh);
    if (mark_trace) {
      base_mesh_->status(bheh).set_selected(true);
    }
    assert(base_mesh_->is_boundary(bheh));
    path.push_back(bheh);
    //base_mesh_->status(bheh).set_selected(true);
  }
  auto bvhe = base_mesh_->to_vertex_handle(bheh);
  assert(bvhe == bvh1);

  InsertPath(mheh, path);

  if (initial_triangulation_) {
    ProcessEdge(meta_mesh_->edge_handle(mheh));
  }
}

/*!
 * \brief Embedding::SimpleTrace Trace function, traces mheh.
 * \param mheh
 * \param cleanup
 * \param mark_trace
 */
void Embedding::SimpleTrace(OpenMesh::HalfedgeHandle mheh, bool cleanup, bool mark_trace) {
  //qDebug() << "Entering A*";
  ProcessFace(mheh);

  auto bheh = FindA_StarStartingHalfedges(mheh);
  auto path = A_StarBidirectional(bheh.first, bheh.second, mark_trace, mheh);
  if (debug_hard_stop_) {
    return;
  }
  //qDebug() << "Entering Insertpath";
  if (!path.empty()) {
    InsertPath(mheh, path);
  }

  //qDebug() << "Entering ProcessEdge";
  // post processing during initial triangulation
  if (initial_triangulation_) {
    ProcessEdge(meta_mesh_->edge_handle(mheh));
  } else if (cleanup) {
    CleanupFace(mheh);
    CleanupFace(meta_mesh_->opposite_halfedge_handle(mheh));
  }
  //base_mesh_->update_normals();
}

/*!
 * \brief Embedding::AdvancedTrace
 * Unimplemented method. This Trace method should handle all neccessary split operations
 * inside the sector it traces in by itself. I will leave this here since implementing it
 * used to be an idea, but there was no time left.
 * TODO: Implement this
 * \param mheh
 */
void Embedding::AdvancedTrace(OpenMesh::HalfedgeHandle mheh) {
  meta_mesh_->status(mheh).deleted();
}

/*!
 * \brief Embedding::A_StarBidirectional Bidirectional A* Algorithm to find the
 * best path quickly, see these:
 *  https://en.wikipedia.org/wiki/A*_search_algorithm
 *  https://www.cs.princeton.edu/courses/archive/spr06/cos423/Handouts/EPP%20shortest%20path%20algorithms.pdf
 * \param bheh0 Starting halfedge 0 found by FindA_StarStartingHalfedges
 * \param bheh1 Starting halfedge 1 found by FindA_StarStartingHalfedges
 * \param mark_trace mark the trace y/n?
 * \param mheh meta halfedge to be traced
 * \return a vector of ordered base halfedges representing the mheh. these still
 * need to be input and their pointers adjusted afterwards.
 */
std::vector<OpenMesh::HalfedgeHandle> Embedding::A_StarBidirectional(
    OpenMesh::HalfedgeHandle bheh0, OpenMesh::HalfedgeHandle bheh1,
    bool mark_trace, OpenMesh::HalfedgeHandle mheh) {
  if (mark_trace) {
    for (auto bheh : base_mesh_->halfedges()) {
      base_mesh_->status(bheh).set_selected(false);
    }
  }
  auto bvh0 = base_mesh_->from_vertex_handle(bheh0);
  auto bvh1 = base_mesh_->from_vertex_handle(bheh1);
  auto mheho = base_mesh_->opposite_halfedge_handle(mheh);

  const double inf = std::numeric_limits<double>::infinity();
  double shortest = inf;
  OpenMesh::VertexHandle middle;

  // Heaps that always display the current vertex with lowest value from direction 0 and 1
  // Heap data structure allows easy access for the next element needed in the algorithm
  struct cmp {
    bool operator()(const std::pair<OpenMesh::VertexHandle, double> &a,
                    const std::pair<OpenMesh::VertexHandle, double> &b) {
      return a.second > b.second;
    }
  };
  std::priority_queue<std::pair<OpenMesh::VertexHandle, double>,
      std::vector<std::pair<OpenMesh::VertexHandle, double>>, cmp> minheap0;
  std::priority_queue<std::pair<OpenMesh::VertexHandle, double>,
      std::vector<std::pair<OpenMesh::VertexHandle, double>>, cmp> minheap1;

  OpenMesh::VPropHandleT<double> gscore0;
  OpenMesh::VPropHandleT<double> gscore1;
  base_mesh_->add_property(gscore0, "Distance from bvh0");
  base_mesh_->add_property(gscore1, "Distance from bvh1");

  minheap0.push(std::make_pair(bvh0, A_StarHeuristic(bvh0, bvh1, bvh0)));
  assert(minheap0.top().second == 0.0);
  minheap1.push(std::make_pair(bvh1, A_StarHeuristic(bvh1, bvh0, bvh1)));
  assert(minheap1.top().second == 0.0);

  // Saving the previous vh for each vh operated through; used for path reconstruction
  OpenMesh::VPropHandleT<OpenMesh::VertexHandle> predecessor0;
  OpenMesh::VPropHandleT<OpenMesh::VertexHandle> predecessor1;
  base_mesh_->add_property(predecessor0, "Previous vertex on shortest path");
  base_mesh_->add_property(predecessor1, "Previous vertex on shortest path");

  OpenMesh::VPropHandleT<bool> closed_set0;
  base_mesh_->add_property(closed_set0, "Vertices in the first closed set");
  OpenMesh::VPropHandleT<bool> closed_set1;
  base_mesh_->add_property(closed_set1, "Vertices in the second closed set");
  for (auto vh : base_mesh_->vertices()) {
    base_mesh_->property(closed_set0, vh) = false;
    base_mesh_->property(closed_set1, vh) = false;
    base_mesh_->property(predecessor0, vh) = OpenMesh::PolyConnectivity::InvalidVertexHandle;
    base_mesh_->property(predecessor1, vh) = OpenMesh::PolyConnectivity::InvalidVertexHandle;
    base_mesh_->property(gscore0, vh) = inf;
    base_mesh_->property(gscore1, vh) = inf;
  }
  base_mesh_->property(gscore0, bvh0) = 0.0;
  base_mesh_->property(gscore1, bvh1) = 0.0;


  // Search while a shortest path hasnt been found and the heaps are not empty
  while (!minheap0.empty()
         && !minheap1.empty()
         && shortest + A_StarHeuristic(bvh0, bvh1, bvh1)
         > base_mesh_->property(gscore0, minheap0.top().first)
         + base_mesh_->property(gscore1, minheap1.top().first)) {
    //qDebug() << "Start of A* Loop";
    OpenMesh::VertexHandle bvhc;
    // One side of the search
    if (minheap0.top().second < minheap1.top().second) {
      bvhc = minheap0.top().first;
      //qDebug() << "-Heap Value0: " << minheap0.top().second << " -A_Star_Heuristic0: "
      //         << A_StarHeuristic(bvh0, bvh1, curr) << " -gScore0: " << gscore0.at(curr);
      minheap0.pop();
      // Only deal with vertices not in the closed set yet
      if (base_mesh_->property(closed_set0, bvhc) == false) {
        base_mesh_->property(closed_set0, bvhc) = true;
        // Iterate over the neighboring vertices
        auto neighbors0 = A_StarNeighbors(bvhc, mheh, closed_set0, bvh1, bheh0);
        //if (neighbors0.empty())
        //  qDebug() << "Empty A* Neighborhood0.";
        for (auto bvhn : neighbors0) {
          // If a better gscore is found set it for neighbors
          auto tentative_gScore = inf;
          if (!base_mesh_->is_valid_handle(base_mesh_->find_halfedge(bvhc, bvhn))) {
            qDebug() << "Invalid seed halfedge bheh0";
          } else {
            //base_mesh_->status(base_mesh_->find_halfedge(curr, vh)).set_selected(true);
            auto bhehw = base_mesh_->find_halfedge(bvhc, bvhn);
            // use reduced cost (modified graph)
            double edge_weight = base_mesh_->calc_edge_length(bhehw);
            // Increase the weight of non-original halfedges
            edge_weight *= base_mesh_->property(halfedge_weight_, bhehw);
            tentative_gScore = base_mesh_->property(gscore0, bvhc) + edge_weight;
          }
          //qDebug() << "n_vertices: " << base_mesh_->n_vertices() << " - vh.idx(): "
          //         << vh.idx();
          if (base_mesh_->property(gscore0, bvhn) > tentative_gScore) {
            base_mesh_->property(gscore0, bvhn) = tentative_gScore;
            base_mesh_->property(predecessor0, bvhn) = bvhc;

            // Push the new neighbors into the heap adding the heuristic value for speed
            // The same value may be added twice but since a minheap is used and only the
            // first time it comes through it will not be in the closed set yet this is ok
            minheap0.push(std::make_pair(bvhn, tentative_gScore + A_StarHeuristic(bvh0, bvh1, bvhn)));
            if (mark_trace) {
              base_mesh_->status(base_mesh_->find_halfedge(bvhc, bvhn)).set_selected(true);
            }
          }
        }
        // Check if a connection with the other side has been made, if so check if it is minimal
        if (bvhc != bvh0 &&
            base_mesh_->property(gscore1, bvhc) < inf) {
          // Special case: the found "middle" is one of the starting vertices.
          // In this case the connection between bvh0 and bvh1 MUST only be one edge long,
          // otherwise the trace found a path from the wrong direction.
          // This should've been the bug messing up Relocation on complex narrow features of meshes.
          bool validvert = true;
          if (bvhc == bvh1) {
            validvert = false;
            for (auto bvhit : base_mesh_->vv_range(bvhc)) {
              if (bvhit == bvh0) {
                validvert = true;
              }
            }
          }
          auto newdist = base_mesh_->property(gscore1, bvhc)
              + base_mesh_->property(gscore0, bvhc);
          if (validvert && shortest > newdist) {
            shortest = newdist;
            middle = bvhc;
            //qDebug() << "found new shortest path of length: " << shortest;
          }
        }
      }
    // Other side of the search
    } else {
      bvhc = minheap1.top().first;
      //qDebug() << "-Heap Value1: " << minheap1.top().second << " -A_Star_Heuristic1: "
      //         << A_StarHeuristic(bvh1, bvh0, curr) << " -gScore1: " << gscore1.at(curr);
      minheap1.pop();
      // Only deal with vertices not in the closed set yet
      if (base_mesh_->property(closed_set1, bvhc) == false) {
        base_mesh_->property(closed_set1, bvhc) = true;
        // Iterate over the neighboring vertices
        auto neighbors1 = A_StarNeighbors(bvhc, mheho, closed_set1, bvh0, bheh1);
        //if (neighbors1.empty())
        //  qDebug() << "Empty A* Neighborhood1.";
        for (auto bvhn : neighbors1) {
          // If a better gscore is found set it for neighbors
          auto tentative_gScore = inf;
          if (!base_mesh_->is_valid_handle(base_mesh_->find_halfedge(bvhc, bvhn))) {
            qDebug() << "Invalid seed halfedge bheh1";
          } else {
            //base_mesh_->status(base_mesh_->find_halfedge(curr, vh)).set_selected(true);
            auto bhehw = base_mesh_->find_halfedge(bvhc, bvhn);
            // use reduced cost (modified graph)
            double edge_weight = base_mesh_->calc_edge_length(bhehw);
            // Increase the weight of non-original halfedges
            edge_weight *= base_mesh_->property(halfedge_weight_, bhehw);
            tentative_gScore = base_mesh_->property(gscore1, bvhc) + edge_weight;
          }
          //qDebug() << "n_vertices: " << base_mesh_->n_vertices() << " - vh.idx(): "
          //         << vh.idx();
          if (base_mesh_->property(gscore1, bvhn) > tentative_gScore) {
            base_mesh_->property(gscore1, bvhn) = tentative_gScore;
            base_mesh_->property(predecessor1, bvhn) = bvhc;

            // Push the new neighbors into the heap adding the heuristic value for speed
            // The same value may be added twice but since a minheap is used and only the
            // first time it comes through it will not be in the closed set yet this is ok
            minheap1.push(std::make_pair(bvhn, tentative_gScore + A_StarHeuristic(bvh1, bvh0, bvhn)));
            if (mark_trace) {
              base_mesh_->status(base_mesh_->find_halfedge(bvhc, bvhn)).set_selected(true);
            }
          }
        }
        // Check if a connection with the other side has been made, if so check if it is minimal
        if (bvhc != bvh1 &&
            base_mesh_->property(gscore0, bvhc) < inf) {
          // Special case: the found "middle" is one of the starting vertices.
          // In this case the connection between bvh0 and bvh1 MUST only be one edge long,
          // otherwise the trace found a path from the wrong direction.
          // This should've been the bug messing up Relocation on complex narrow features of meshes.
          bool validvert = true;
          if (bvhc == bvh0) {
            validvert = false;
            for (auto bvhit : base_mesh_->vv_range(bvhc)) {
              if (bvhit == bvh1) {
                validvert = true;
              }
            }
          }

          auto newdist = base_mesh_->property(gscore1, bvhc)
              + base_mesh_->property(gscore0, bvhc);
          if (validvert && shortest > newdist) {
            shortest = newdist;
            middle = bvhc;
            //qDebug() << "found new shortest path of length: " << shortest;
          }
        }
      }
    }
  }
  //qDebug() << "Finished A* Loop";
  if (!base_mesh_->is_valid_handle(middle)) {
    debug_hard_stop_=true;
    if (!mark_trace) {
      qWarning() << "A* Tracing failed for vertices " << bvh0.idx() << " and "
                 << bvh1.idx() << " retracing to mark.";
      A_StarBidirectional(bheh0, bheh1, true, mheh);
      qDebug() << "Marking the closed set";
      for (auto bvh : base_mesh_->vertices()) {
        if (base_mesh_->property(closed_set0, bvh) ||
            base_mesh_->property(closed_set1, bvh)) {
          base_mesh_->status(bvh).set_selected(true);
        }
      }
      qDebug() << "Marking bheh0 " << bheh0.idx() << "and bheh1 " << bheh1.idx()
               << "they are opposites: "
               << (bheh0 == base_mesh_->opposite_halfedge_handle(bheh1));
      base_mesh_->status(bheh0).set_selected(true);
      base_mesh_->status(bheh1).set_selected(true);
    } else {
      MarkVertex(bvh0);
      MarkVertex(bvh1);
    }
  }
  base_mesh_->remove_property(closed_set0);
  base_mesh_->remove_property(closed_set1);
  base_mesh_->remove_property(gscore0);
  base_mesh_->remove_property(gscore1);
  return A_StarReconstructPath(middle, predecessor0, predecessor1);
}

/*!
 * \brief Embedding::Distance
 * \param bvhf
 * \param bvhv
 * \return direct spatial distance between vertices bvhf and bvhv
 */
double Embedding::Distance(OpenMesh::VertexHandle bvhf, OpenMesh::VertexHandle bvhv) {
  auto pf = base_mesh_->point(bvhf);
  auto pv = base_mesh_->point(bvhv);
  double distfv = std::sqrt((pf[0]-pv[0])*(pf[0]-pv[0])
      +(pf[1]-pv[1])*(pf[1]-pv[1])
      +(pf[2]-pv[2])*(pf[2]-pv[2]));
  return distfv;
}

/*!
 * \brief Embedding::A_StarHeuristic The heuristic used in A* here, detailed explanation:
 * https://www.cs.princeton.edu/courses/archive/spr06/cos423/Handouts/EPP%20shortest%20path%20algorithms.pdf
 * Averaging the distance heuristic from both sides makes it admissible and feasible
 * Variables corresponding to page 4, Bidirectional A* Search
 * p_f(v): 1/2(pi_f(v)-pi_r(v)) + (pi_r(t)/2)
 * Distance(bvhf, bvhv) : pi_f(v)
 * Distance(bvhr, bvhv) : pi_r(v)
 * Distance(bvhr, bvhf) : pi_r(t)
 * Then call it with flipped bvhf and bvhr for the opposite direction p_r(v).
 * p_r(v): 1/2(pi_r(v)-pi_f(v)) + (pi_r(s)/2)
 * \param bvhf
 * \param bvhr
 * \param bvhv
 * \return heuristical values to sort the heaps by
 */
double Embedding::A_StarHeuristic(OpenMesh::VertexHandle bvhf,
                                   OpenMesh::VertexHandle bvhr,
                                   OpenMesh::VertexHandle bvhv) {
  return 0.5 * (Distance(bvhf, bvhv) - Distance(bvhr, bvhv))
      + (Distance(bvhr, bvhf) / 2);
}

/*!
 * \brief Embedding::A_StarNeighbors
 * \param bvh
 * \param mheh
 * \param closed_set
 * \param towardsbvh
 * \param bheh
 * \return the VV_Neighborhood excluding vertices already closed by A* and border vertices
 */
std::vector<OpenMesh::VertexHandle> Embedding::A_StarNeighbors(OpenMesh::VertexHandle bvh,
               OpenMesh::HalfedgeHandle mheh,
               OpenMesh::VPropHandleT<bool> closed_set,
               OpenMesh::VertexHandle towardsbvh,
               OpenMesh::HalfedgeHandle bheh) {
  std::vector<OpenMesh::VertexHandle> neighbors;
  if (bvh == towardsbvh && base_mesh_->from_vertex_handle(bheh) != bvh) {
    return neighbors;
  }
  if (meta_mesh_->is_valid_handle(base_mesh_->property(bv_connection_, bvh))) {
    // Ensure that the seed halfedge originates from the initial vertex
    assert(base_mesh_->from_vertex_handle(bheh) == bvh);
    // Ensure that the seed halfedge is empty
    assert(!meta_mesh_->is_valid_handle(base_mesh_->property(bhe_connection_, bheh)));
    auto bhehiter = bheh;
    do {
      if (!meta_mesh_->is_valid_handle(base_mesh_->property(bhe_connection_, bhehiter))
          && (!IsSectorBorder(base_mesh_->to_vertex_handle(bhehiter))
              || base_mesh_->to_vertex_handle(bhehiter) == towardsbvh)) {
        if (!(base_mesh_->to_vertex_handle(bhehiter) == towardsbvh)) {
          neighbors.push_back(base_mesh_->to_vertex_handle(bhehiter));
        } else if (SectorEntry(mheh, bhehiter)) {
            neighbors.push_back(base_mesh_->to_vertex_handle(bhehiter));
        }
      }
      bhehiter = base_mesh_->next_halfedge_handle(
            base_mesh_->opposite_halfedge_handle(bhehiter));
    } while (!meta_mesh_->is_valid_handle(base_mesh_->property(bhe_connection_, bhehiter))
             && bhehiter != bheh);
  } else {
    for (auto voh_it: base_mesh_->voh_range(bvh)) {
      auto vv_it = base_mesh_->to_vertex_handle(voh_it);
      if ((base_mesh_->property(closed_set, vv_it) == false)
          && !IsSectorBorder(vv_it)
          && ValidA_StarEdge(voh_it, mheh)) {
        neighbors.push_back(vv_it);
      }
    }
  }

  return neighbors;
}

/*!
 * \brief Embedding::SectorEntry check if entry into the to_vertex of mheh
 * is allowed via bheh
 * \param mheh
 * \param bheh
 * \return
 */
bool Embedding::SectorEntry(OpenMesh::HalfedgeHandle mheh, OpenMesh::HalfedgeHandle bheh) {
  auto mheho = meta_mesh_->opposite_halfedge_handle(mheh);
  auto mhehn = meta_mesh_->opposite_halfedge_handle(meta_mesh_->prev_halfedge_handle(
               mheho));
  while (!base_mesh_->is_valid_handle(meta_mesh_->property(mhe_connection_, mhehn))
         && mhehn != mheho) {
    mhehn = meta_mesh_->opposite_halfedge_handle(meta_mesh_->prev_halfedge_handle(mhehn));
  }
  auto bhehc = base_mesh_->opposite_halfedge_handle(bheh);
  assert (base_mesh_->property(bv_connection_, base_mesh_->from_vertex_handle(bhehc))
                            == meta_mesh_->from_vertex_handle(mheho));
  auto bhehn = base_mesh_->opposite_halfedge_handle(base_mesh_->prev_halfedge_handle(bhehc));
  while (!meta_mesh_->is_valid_handle(base_mesh_->property(bhe_connection_, bhehn))
         && bhehn != bhehc) {
    bhehn = base_mesh_->opposite_halfedge_handle(base_mesh_->prev_halfedge_handle(bhehn));
  }
  if (((bhehn == meta_mesh_->property(mhe_connection_, mhehn))
      && (base_mesh_->property(bhe_connection_, bhehn) == mhehn))
      || (bhehn == bhehc && mhehn == mheho)) {
    return true;
  }
  return false;
}

/*!
 * \brief Embedding::ValidA_StarEdge
 * \param bheh
 * \param mheh
 * \return whether bheh is a Valid edge for A* traversal based on the bhe_border_
 * properties. These properties describe voronoi regions, so this method is only
 * really used during initial triangulation to ensure traces don't block each other.
 */
bool Embedding::ValidA_StarEdge(OpenMesh::HalfedgeHandle bheh,
                                OpenMesh::HalfedgeHandle mheh) {
  if (!initial_triangulation_) {
    return true;
  }
  return (!meta_mesh_->is_valid_handle(base_mesh_->property(bhe_border_, bheh))
          || (base_mesh_->property(bhe_border_, bheh) == mheh)
          || (base_mesh_->property(bhe_border_, bheh) ==
              meta_mesh_->opposite_halfedge_handle(mheh)));
}

/*!
 * \brief Embedding::A_StarReconstructPath reconstructs a meta halfedge based on the output
 * of the A_StarBidirectional function
 * \param middle the middle halfedge of mheh
 * \param predecessor0 predecessor property in one direction from the middle
 * \param predecessor1 predecessor property in the other direction from the middle
 * \return An ordered vector of halfedges representing the meta edge that was traced
 */
std::vector<OpenMesh::HalfedgeHandle> Embedding::A_StarReconstructPath(
    OpenMesh::VertexHandle middle,
    OpenMesh::VPropHandleT<OpenMesh::VertexHandle> predecessor0,
    OpenMesh::VPropHandleT<OpenMesh::VertexHandle> predecessor1)  {
  std::list<OpenMesh::VertexHandle> total_path;
  std::vector<OpenMesh::HalfedgeHandle> returnpath;
  if (!base_mesh_->is_valid_handle(middle)) {
    // qDebug() << "A_StarReconstructPath returning empty path";
    return returnpath;
  }
  total_path.push_back(middle);
  auto curr = base_mesh_->property(predecessor0, middle);
  while (base_mesh_->is_valid_handle(curr)) {
    total_path.push_front(curr);
    curr = base_mesh_->property(predecessor0, curr);
  }
  curr = middle;
  curr = base_mesh_->property(predecessor1, middle);
  while (base_mesh_->is_valid_handle(curr)) {
    total_path.push_back(curr);
    curr = base_mesh_->property(predecessor1, curr);
  }
  OpenMesh::VertexHandle vh0 = total_path.front();
  OpenMesh::VertexHandle vh1;
  total_path.pop_front();
  while (!total_path.empty()) {
    vh1 = vh0;
    vh0 = total_path.front();
    auto heh = base_mesh_->find_halfedge(vh1, vh0);
    returnpath.push_back(heh);
    total_path.pop_front();
  }
  base_mesh_->remove_property(predecessor0);
  base_mesh_->remove_property(predecessor1);
  return returnpath;
}

/*!
 * \brief Embedding::IsSectorBorder
 * \param bvh
 * \param exceptions
 * \return true if bvh lies on a meta_mesh element (vertex or edge) which is NOT
 * included in the list of exception
 */
bool Embedding::IsSectorBorder(OpenMesh::VertexHandle bvh,
                               std::list<OpenMesh::HalfedgeHandle> exceptions) {
  if (meta_mesh_->is_valid_handle(base_mesh_->property(bv_connection_, bvh))) {
    if (!exceptions.empty() && meta_mesh_->to_vertex_handle(exceptions.front()) ==
        base_mesh_->property(bv_connection_, bvh)) {
      return false;
    }
    return true;
  }
  for (auto voh_it : base_mesh_->voh_range(bvh)) {
    auto mhehprop = base_mesh_->property(bhe_connection_, voh_it);
    if (meta_mesh_->is_valid_handle(mhehprop)) {
      for (auto mheh : exceptions) {
        if (mheh == mhehprop) {
          return false;
        }
        if (meta_mesh_->opposite_halfedge_handle(mheh) == mhehprop) {
          return false;
        }
      }
      return true;
    }
  }
  return false;
}

/*!
 * \brief Embedding::InsertPath Inserts a list of base halfedges saved in path into the mesh
 * as meta halfedge mheh (called after A* triangulation)
 * \param mheh
 * \param path
 */
void Embedding::InsertPath(OpenMesh::HalfedgeHandle mheh,
                           std::vector<OpenMesh::HalfedgeHandle> path) {
  if (path.empty()) {
    qDebug() << "Couldn't insert empty path";
  } else {
    auto opp = meta_mesh_->opposite_halfedge_handle(mheh);
    meta_mesh_->property(mhe_connection_, mheh) = path.front();
    meta_mesh_->property(mhe_connection_, opp) =
        base_mesh_->opposite_halfedge_handle(path.back());
    auto prev = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
    while (!path.empty()) {
      auto curr = path.back();
      base_mesh_->property(next_heh_, curr) = prev;
      base_mesh_->property(bhe_connection_, curr) = mheh;
      base_mesh_->property(bhe_connection_, base_mesh_->opposite_halfedge_handle(curr)) = opp;
      path.pop_back();
      if (base_mesh_->is_valid_handle(prev)) {
        base_mesh_->property(next_heh_, base_mesh_->opposite_halfedge_handle(prev))
            = base_mesh_->opposite_halfedge_handle(curr);
      }
      prev = curr;
    }
    base_mesh_->property(next_heh_, base_mesh_->opposite_halfedge_handle(prev))
      = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  }
}

/*!
 * \brief Embedding::FindA_StarStartingHalfedges  Find A* Starting halfedges through a series
 * of halfedge pointer operations. This is more complicated than it first sounds because it has
 * to handle an initial triangulation where edges are traced in an arbitrary order and some edges
 * may already be traced while others arent. Traces starting in the wrong sectors will fail so
 * the starting halfedges need to be right.
 * Assumes a pre-processed sector to trace in.
 * \param mheh the halfedge to be traced.
 * \param verbose
 * \return Two starting halfedges for use in A* tracing.
 */
std::pair<OpenMesh::HalfedgeHandle, OpenMesh::HalfedgeHandle>
Embedding::FindA_StarStartingHalfedges(OpenMesh::HalfedgeHandle mheh, bool verbose) {
  std::pair<OpenMesh::HalfedgeHandle, OpenMesh::HalfedgeHandle> output;
  //ProcessNeighbors(meta_mesh_->edge_handle(mheh));
  auto mvh0 = meta_mesh_->from_vertex_handle(mheh);
  auto mvh1 = meta_mesh_->to_vertex_handle(mheh);
  output.first = base_mesh_->halfedge_handle(meta_mesh_->property(mv_connection_, mvh0));
  output.second = base_mesh_->halfedge_handle(meta_mesh_->property(mv_connection_, mvh1));
  assert(mvh0 == base_mesh_->property(bv_connection_,
                                      base_mesh_->from_vertex_handle(output.first)));
  assert(mvh1 == base_mesh_->property(bv_connection_,
                                      base_mesh_->from_vertex_handle(output.second)));
  auto iter = meta_mesh_->opposite_halfedge_handle(meta_mesh_->prev_halfedge_handle(mheh));
  while (iter != mheh) {
    assert(meta_mesh_->from_vertex_handle(iter) == mvh0);
    if (base_mesh_->is_valid_handle(meta_mesh_->property(mhe_connection_, iter))) {
      if (base_mesh_->property(bhe_connection_, (meta_mesh_->property(mhe_connection_, iter)))
          == iter) {
        output.first = base_mesh_->next_halfedge_handle(base_mesh_->opposite_halfedge_handle(
                                      meta_mesh_->property(mhe_connection_, iter)));
        assert(mvh0 == base_mesh_->property(bv_connection_,
                                            base_mesh_->from_vertex_handle(output.first)));
        goto endloop1;
      }
    }
    /*
    auto bvhx = meta_mesh_->property(mv_connection_, meta_mesh_->to_vertex_handle(iter));
    for (auto bhehiter : base_mesh_->voh_range(meta_mesh_->property(mv_connection_,
                                              meta_mesh_->from_vertex_handle(iter)))) {
      if (base_mesh_->to_vertex_handle(bhehiter) == bvhx) {
        output.first = base_mesh_->next_halfedge_handle(base_mesh_->opposite_halfedge_handle(
                                      bhehiter));
        assert(mvh0 == base_mesh_->property(bv_connection_,
                                            base_mesh_->from_vertex_handle(output.first)));
        goto endloop1;
      }
    }
    */
    iter = meta_mesh_->opposite_halfedge_handle(meta_mesh_->prev_halfedge_handle(iter));
  }
  endloop1:

  if (meta_mesh_->is_valid_handle(base_mesh_->property(bhe_connection_, output.first)) ||
      (meta_mesh_->is_valid_handle(base_mesh_->property(bv_connection_,
          base_mesh_->to_vertex_handle(output.first)))
       && base_mesh_->property(bv_connection_, base_mesh_->to_vertex_handle(output.first))
          != mvh1)) {
    SplitBaseHe(base_mesh_->next_halfedge_handle(output.first));
    output.first = base_mesh_->opposite_halfedge_handle(
          base_mesh_->prev_halfedge_handle(output.first));
    assert(mvh0 == base_mesh_->property(bv_connection_,
                                        base_mesh_->from_vertex_handle(output.first)));
  }

  auto opp = meta_mesh_->opposite_halfedge_handle(mheh);
  iter = meta_mesh_->opposite_halfedge_handle(meta_mesh_->prev_halfedge_handle(opp));
  while (iter != opp) {
    assert(meta_mesh_->from_vertex_handle(iter) == mvh1);
    if (base_mesh_->is_valid_handle(meta_mesh_->property(mhe_connection_, iter))) {
      if (base_mesh_->property(bhe_connection_, (meta_mesh_->property(mhe_connection_, iter)))
          == iter) {
        output.second = base_mesh_->next_halfedge_handle(base_mesh_->opposite_halfedge_handle(
                                      meta_mesh_->property(mhe_connection_, iter)));
        goto endloop2;
      }
    }
    /*
    auto bvhx = meta_mesh_->property(mv_connection_, meta_mesh_->to_vertex_handle(iter));
    for (auto bhehiter : base_mesh_->voh_range(meta_mesh_->property(mv_connection_,
                                              meta_mesh_->from_vertex_handle(iter)))) {
      if (base_mesh_->to_vertex_handle(bhehiter) == bvhx) {
        output.second = base_mesh_->next_halfedge_handle(base_mesh_->opposite_halfedge_handle(
                                      bhehiter));
        goto endloop2;
      }
    }
    */
    iter = meta_mesh_->opposite_halfedge_handle(meta_mesh_->prev_halfedge_handle(iter));
  }
  endloop2:

  if (meta_mesh_->is_valid_handle(base_mesh_->property(bhe_connection_, output.second)) ||
      (meta_mesh_->is_valid_handle(base_mesh_->property(bv_connection_,
          base_mesh_->to_vertex_handle(output.second)))
       && base_mesh_->property(bv_connection_, base_mesh_->to_vertex_handle(output.second))
          != mvh0)) {
    assert(mvh0 == base_mesh_->property(bv_connection_,
                                        base_mesh_->from_vertex_handle(output.first)));
    SplitBaseHe(base_mesh_->next_halfedge_handle(output.second));
    assert(mvh0 == base_mesh_->property(bv_connection_,
                                        base_mesh_->from_vertex_handle(output.first)));
    output.second = base_mesh_->opposite_halfedge_handle(
          base_mesh_->prev_halfedge_handle(output.second));
  }

  // Make sure the chosen starting halfedges start at the given vertices and are still empty.
  assert(base_mesh_->is_valid_handle(output.first));
  assert(base_mesh_->is_valid_handle(output.second));
  assert(!meta_mesh_->is_valid_handle(base_mesh_->property(bhe_connection_, output.first)));
  assert(!meta_mesh_->is_valid_handle(base_mesh_->property(bhe_connection_, output.second)));
  auto bvcp1 = base_mesh_->property(bv_connection_, base_mesh_->from_vertex_handle(output.first));
  auto bvcp2 = base_mesh_->property(bv_connection_, base_mesh_->from_vertex_handle(output.second));
  assert(bvcp1 ==  mvh0);
  assert(bvcp2 ==  mvh1);

  if (verbose
      && meta_mesh_->from_vertex_handle(mheh) == meta_mesh_->to_vertex_handle(mheh)) {
    qDebug() << "Trace from mvertex " << meta_mesh_->from_vertex_handle(mheh).idx()
             << " to itself using starting halfedges " << output.first.idx()
             << " and " << output.second.idx();
  }

  return output;
}

/*!
 * \brief Embedding::MarkVertex Marks a meta vertex in black while coloring its
 * vv_neighborhood white, making it easy to see for visual debugging.
 * \param bvh
 */
void Embedding::MarkVertex(OpenMesh::VertexHandle bvh) {
  qDebug() << "Marking vertex: " << bvh.idx() << "in white.";
  auto vertex_colors_pph = base_mesh_->vertex_colors_pph();
  base_mesh_->property(vertex_colors_pph, bvh) = ACG::Vec4f(0.0f, 0.0f, 0.0f, 1.0f);
  for (auto vvh : base_mesh_->vv_range(bvh)) {
    if (base_mesh_->property(vertex_colors_pph, vvh) != ACG::Vec4f(0.0f, 0.0f, 0.0f, 1.0f)) {
      base_mesh_->property(vertex_colors_pph, vvh) = ACG::Vec4f(1.0f, 1.0f, 1.0f, 1.0f);
    }
  }
}

/*!
 * \brief Embedding::AddMetaEdge Adds a meta edge (it will still need to be traced
 * into the base mesh)
 * \param mheh0 the previous edge of mheh
 * \param mheh1 the previous edge of the opposite edge of mheh
 * \return a halfedge of the new edge, specifically the next halfedge of mheh0
 */
OpenMesh::HalfedgeHandle Embedding::AddMetaEdge(OpenMesh::HalfedgeHandle mheh0,
                                                OpenMesh::HalfedgeHandle mheh1) {
  auto mhehnew0 = meta_mesh_->new_edge(meta_mesh_->to_vertex_handle(mheh0),
                                      meta_mesh_->to_vertex_handle(mheh1));
  auto mhehnew1 = meta_mesh_->opposite_halfedge_handle(mhehnew0);
  auto fnew0 = meta_mesh_->face_handle(mheh0);
  auto fnew1(meta_mesh_->new_face());
  auto next0 = meta_mesh_->next_halfedge_handle(mheh0);
  auto next1 = meta_mesh_->next_halfedge_handle(mheh1);
  auto mvh0 = meta_mesh_->to_vertex_handle(mheh0);
  auto mvh1 = meta_mesh_->to_vertex_handle(mheh1);

  meta_mesh_->set_next_halfedge_handle(mheh0, mhehnew0);
  meta_mesh_->set_next_halfedge_handle(mheh1, mhehnew1);
  meta_mesh_->set_next_halfedge_handle(mhehnew0, next1);
  meta_mesh_->set_next_halfedge_handle(mhehnew1, next0);

  meta_mesh_->set_vertex_handle(mhehnew0, mvh1);
  meta_mesh_->set_vertex_handle(mhehnew1, mvh0);

  meta_mesh_->set_halfedge_handle(fnew0, mhehnew0);
  meta_mesh_->set_halfedge_handle(fnew1, mhehnew1);

  meta_mesh_->set_face_handle(mhehnew0, fnew0);
  meta_mesh_->set_face_handle(mhehnew1, fnew1);

  auto iter = meta_mesh_->next_halfedge_handle(mhehnew0);
  while (iter != mhehnew0) {
    meta_mesh_->set_face_handle(iter, fnew0);
    iter = meta_mesh_->next_halfedge_handle(iter);
  }
  iter = meta_mesh_->next_halfedge_handle(mhehnew1);
  while (iter != mhehnew1) {
    meta_mesh_->set_face_handle(iter, fnew1);
    iter = meta_mesh_->next_halfedge_handle(iter);
  }

  // Properties
  meta_mesh_->property(mhe_connection_, mhehnew0) =
      OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  meta_mesh_->property(mhe_connection_, mhehnew1) =
      OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  meta_mesh_->property(mhe_border_, mhehnew0) =
      OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  meta_mesh_->property(mhe_border_, mhehnew1) =
      OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;

  return mhehnew0;
}

/*!
 * \brief Embedding::Rotate
 * \param meh
 */
void Embedding::Rotate(OpenMesh::EdgeHandle meh) {
  if (!IsRotationOkay(meh)) {
    qDebug() << "Meta edge " << meh.idx() << " cannot be rotated";
  } else {
    MetaRotation(meh);
    auto mheh = meta_mesh_->halfedge_handle(meh, 0);
    RemoveMetaEdgeFromBaseMesh(mheh);
    Trace(mheh, true, false);
  }
}

/*!
 * \brief Embedding::IsRotationOkay
 * \param meh
 * \return whether meh can be rotateds
 */
bool Embedding::IsRotationOkay(OpenMesh::EdgeHandle meh) {
  auto mheh0 = meta_mesh_->halfedge_handle(meh, 0);
  auto mheh1 = meta_mesh_->halfedge_handle(meh, 1);

  // existence check
  if (!meta_mesh_->is_valid_handle(meh)
      || !meta_mesh_->is_valid_handle(mheh0)
      || !meta_mesh_->is_valid_handle(mheh1)
      || !base_mesh_->is_valid_handle(meta_mesh_->property(mhe_connection_, mheh0))
      || !base_mesh_->is_valid_handle(meta_mesh_->property(mhe_connection_, mheh1))) {
    return false;
  }

  // boundary check
  if (meta_mesh_->is_boundary(meta_mesh_->halfedge_handle(meh, 0))
      || meta_mesh_->is_boundary(meta_mesh_->halfedge_handle(meh, 1))) {
    return false;
  }

  // loop check
  return (meta_mesh_->next_halfedge_handle(mheh0) != mheh1
          && meta_mesh_->next_halfedge_handle(mheh1) != mheh0);
}

// Flips an edge in the meta mesh, but not in the underlying base mesh
void Embedding::MetaRotation(OpenMesh::EdgeHandle meh) {
  //qDebug() << "MetaFlip function called";
  assert(IsRotationOkay(meh));
  auto mheh0 = meta_mesh_->halfedge_handle(meh, 0);
  auto mheh1 = meta_mesh_->halfedge_handle(meh, 1);
  auto mhehprev0 = meta_mesh_->prev_halfedge_handle(mheh0);
  auto mhehprev1 = meta_mesh_->prev_halfedge_handle(mheh1);
  auto mhehnext0 = meta_mesh_->next_halfedge_handle(mheh0);
  auto mhehnext1 = meta_mesh_->next_halfedge_handle(mheh1);
  auto mhehnextnext0 = meta_mesh_->next_halfedge_handle(mhehnext0);
  auto mhehnextnext1 = meta_mesh_->next_halfedge_handle(mhehnext1);
  auto fh0 = meta_mesh_->face_handle(mheh0);
  auto fh1 = meta_mesh_->face_handle(mheh1);
  auto mvh0 = meta_mesh_->to_vertex_handle(mheh0);
  auto mvh1 = meta_mesh_->to_vertex_handle(mheh1);
  auto mvh01 = meta_mesh_->to_vertex_handle(mhehnext0);
  auto mvh11 = meta_mesh_->to_vertex_handle(mhehnext1);

  // Flip next_halfedge connections
  meta_mesh_->set_next_halfedge_handle(mhehprev0, mhehnext1);
  meta_mesh_->set_next_halfedge_handle(mhehprev1, mhehnext0);
  meta_mesh_->set_next_halfedge_handle(mhehnext0, mheh0);
  meta_mesh_->set_next_halfedge_handle(mhehnext1, mheh1);
  meta_mesh_->set_next_halfedge_handle(mheh0, mhehnextnext1);
  meta_mesh_->set_next_halfedge_handle(mheh1, mhehnextnext0);

  // Ensure correct face->heh connections
  meta_mesh_->set_halfedge_handle(fh0, mheh0);
  meta_mesh_->set_halfedge_handle(fh1, mheh1);

  // Ensure correct heh->face connections
  meta_mesh_->set_face_handle(mhehprev1, fh0);
  meta_mesh_->set_face_handle(mhehprev0, fh1);
  meta_mesh_->set_face_handle(mhehnext0, fh0);
  meta_mesh_->set_face_handle(mhehnext1, fh1);

  // Ensure correct heh->vertex connections
  meta_mesh_->set_vertex_handle(mheh0, mvh11);
  meta_mesh_->set_vertex_handle(mheh1, mvh01);

  // Ensure correct vertex->heh connections while preserving boundary rule
  if (meta_mesh_->halfedge_handle(mvh0) == mheh1) {
    meta_mesh_->set_halfedge_handle(mvh0, mhehnext0);
  }
  if (meta_mesh_->halfedge_handle(mvh1) == mheh0) {
    meta_mesh_->set_halfedge_handle(mvh1, mhehnext1);
  }

  // Check for correctness
  auto mheh03 = meta_mesh_->next_halfedge_handle(meta_mesh_->next_halfedge_handle(
                                                 meta_mesh_->next_halfedge_handle(mheh0)));
  auto mheh13 = meta_mesh_->next_halfedge_handle(meta_mesh_->next_halfedge_handle(
                                                 meta_mesh_->next_halfedge_handle(mheh1)));
  assert(mheh0 == mheh03);
  assert(mheh1 == mheh13);
  assert(meta_mesh_->from_vertex_handle(mheh0) == meta_mesh_->to_vertex_handle(mhehnext0));
  assert(meta_mesh_->from_vertex_handle(mheh1) == meta_mesh_->to_vertex_handle(mhehnext1));
  assert(meta_mesh_->to_vertex_handle(mheh0) == meta_mesh_->to_vertex_handle(mhehnext1));
  assert(meta_mesh_->to_vertex_handle(mheh1) == meta_mesh_->to_vertex_handle(mhehnext0));
}

/*!
 * \brief Embedding::RemoveMetaEdgeFromBaseMesh
 * \param mheh meta halfedge to be removed from the basemesh (it stays in the meta mesh)
 * \param cleanup
 */
void Embedding::RemoveMetaEdgeFromBaseMesh(OpenMesh::HalfedgeHandle mheh, bool cleanup) {
  assert(meta_mesh_->is_valid_handle(mheh));
  assert(base_mesh_->is_valid_handle(meta_mesh_->property(mhe_connection_, mheh)));
  auto halfedges = GetBaseHalfedges(mheh);
  assert(base_mesh_->property(bhe_connection_, halfedges.front()) == mheh);
  auto mheho = meta_mesh_->opposite_halfedge_handle(mheh);
  assert(base_mesh_->property(bhe_connection_,
         base_mesh_->opposite_halfedge_handle(halfedges.back())) == mheho);
  meta_mesh_->property(mhe_connection_, mheh)
      = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  meta_mesh_->property(mhe_connection_, mheho)
      = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;

  for (auto bheh : halfedges) {
    base_mesh_->property(bhe_connection_, bheh)
        = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
    base_mesh_->property(bhe_connection_, base_mesh_->opposite_halfedge_handle(bheh))
        = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
    base_mesh_->property(next_heh_, bheh)
        = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
    base_mesh_->property(next_heh_, base_mesh_->opposite_halfedge_handle(bheh))
        = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
  }

  // Cleanup
  if (cleanup) {
    for (auto bheh : halfedges) {
      std::queue<OpenMesh::VertexHandle> cleanupqueue;
      for (auto bvhit : base_mesh_->vv_range(base_mesh_->to_vertex_handle(bheh))) {
        if (!base_mesh_->status(bvhit).deleted()
            && base_mesh_->is_valid_handle(base_mesh_->property(bsplithandle_, bvhit))) {
          cleanupqueue.push(bvhit);
        }
      }
      while (!cleanupqueue.empty()) {
        if (base_mesh_->is_valid_handle(cleanupqueue.front())
            && !base_mesh_->status(cleanupqueue.front()).deleted()) {
          ConditionalCollapse(cleanupqueue.front());
        }
        cleanupqueue.pop();
      }
    }
  }
}

/*!
 * \brief Embedding::RemoveMetaEdgeFromMetaMesh
 * \param mheh meta halfedge to be removed, from the meta mesh only (stays in the base mesh
 * if not removed from there first which it usually should be)
 */
void Embedding::RemoveMetaEdgeFromMetaMesh(OpenMesh::HalfedgeHandle mheh) {
  auto mheho = meta_mesh_->opposite_halfedge_handle(mheh);
  auto mhehn = meta_mesh_->next_halfedge_handle(mheh);
  auto mhehp = meta_mesh_->prev_halfedge_handle(mheh);
  auto mhehon = meta_mesh_->next_halfedge_handle(mheho);
  auto mhehop = meta_mesh_->prev_halfedge_handle(mheho);
  auto mvh0 = meta_mesh_->from_vertex_handle(mheh);
  auto mvh1 = meta_mesh_->to_vertex_handle(mheh);
  auto mfh0 = meta_mesh_->face_handle(mheh);
  auto mfh1 = meta_mesh_->face_handle(mheho);

  // back
  if (mhehp != mheho) {
    // he - he
    meta_mesh_->set_next_halfedge_handle(mhehp, mhehon);
    // v - he
    if (meta_mesh_->halfedge_handle(mvh0) == mheh) {
      meta_mesh_->set_halfedge_handle(mvh0, mhehon);
    }
  } else {
    assert(base_mesh_->is_valid_handle(meta_mesh_->property(mv_connection_, mvh0)));
    base_mesh_->property(bv_connection_, meta_mesh_->property(mv_connection_, mvh0))
        = OpenMesh::PolyConnectivity::InvalidVertexHandle;
    meta_mesh_->set_isolated(mvh0);
    meta_mesh_->status(mvh0).set_deleted(true);
  }
  // front
  if (mhehn != mheho) {
    // he - he
    meta_mesh_->set_next_halfedge_handle(mhehop, mhehn);
    // v - he
    if (meta_mesh_->halfedge_handle(mvh1) == mheho) {
      meta_mesh_->set_halfedge_handle(mvh1, mhehn);
    }
  } else {
    assert(base_mesh_->is_valid_handle(meta_mesh_->property(mv_connection_, mvh1)));
    base_mesh_->property(bv_connection_, meta_mesh_->property(mv_connection_, mvh1))
        = OpenMesh::PolyConnectivity::InvalidVertexHandle;
    meta_mesh_->set_isolated(mvh1);
    meta_mesh_->status(mvh1).set_deleted(true);
  }
  if (meta_mesh_->is_valid_handle(mfh0)) {
    // f - he
    meta_mesh_->set_halfedge_handle(mfh0, mhehn);
    // he - f
    auto mhehiter = mhehn;
    do {
      meta_mesh_->set_face_handle(mhehiter, mfh0);
      mhehiter = meta_mesh_->next_halfedge_handle(mhehiter);
    } while (mhehn != mhehiter);
  }
  // delete stuff
  if (meta_mesh_->is_valid_handle(mfh1) && mfh1 != mfh0) {
    meta_mesh_->status(mfh1).set_deleted(true);
  }
  meta_mesh_->status(meta_mesh_->edge_handle(mheh)).set_deleted(true);
  if (meta_mesh_->has_halfedge_status()) {
    meta_mesh_->status(mheh).set_deleted(true);
    meta_mesh_->status(mheho).set_deleted(true);
  }
}

/*!
 * \brief Embedding::MiddleBvh
 * \param meh
 * \return the base vertex at the middle of meh (roughly since the embedding is discrete)
 */
OpenMesh::VertexHandle Embedding::MiddleBvh(OpenMesh::EdgeHandle meh) {
  auto bhehleft = meta_mesh_->property(mhe_connection_, meta_mesh_->halfedge_handle(meh, 0));
  auto bhehright = meta_mesh_->property(mhe_connection_, meta_mesh_->halfedge_handle(meh, 1));
  if (bhehleft == base_mesh_->opposite_halfedge_handle(bhehright)) {
    return base_mesh_->to_vertex_handle(SplitBaseHe(bhehleft));
  }
  double leftdist = base_mesh_->calc_edge_length(bhehleft);
  double rightdist = base_mesh_->calc_edge_length(bhehright);

  while(base_mesh_->to_vertex_handle(bhehleft)
        != base_mesh_->to_vertex_handle(bhehright)) {
    if (leftdist < rightdist) {
      bhehleft = base_mesh_->property(next_heh_, bhehleft);
      leftdist += base_mesh_->calc_edge_length(bhehleft);
    } else {
      bhehright = base_mesh_->property(next_heh_, bhehright);
      rightdist += base_mesh_->calc_edge_length(bhehright);
    }
  }
  return base_mesh_->to_vertex_handle(bhehleft);
}

/*!
 * \brief Embedding::MiddleBvh
 * \param mheh
 * \return the base vertex at the middle of mheh (roughly since the embedding is discrete)
 */
OpenMesh::VertexHandle Embedding::MiddleBvh(OpenMesh::HalfedgeHandle mheh) {
  return MiddleBvh(meta_mesh_->edge_handle(mheh));
}

/*!
 * \brief Embedding::EdgeSplit splits meh at bvh
 * \param meh
 * \param bvh
 * \param verbose
 */
void Embedding::EdgeSplit(OpenMesh::EdgeHandle meh, OpenMesh::VertexHandle bvh, bool verbose) {
  assert(meta_mesh_->is_valid_handle(meh));
  if (!base_mesh_->is_valid_handle(bvh)) {
    bvh = MiddleBvh(meh);
  }

  if (verbose) {
    qDebug() << "Split the edge in the meta mesh";
  }
  MetaEdgeSplit(meh, bvh);
  auto mvh = base_mesh_->property(bv_connection_, bvh);
  auto start = meta_mesh_->halfedge_handle(mvh);
  auto iter = start;
  if (verbose) {
    qDebug() << "Trace the new edges in the base mesh";
  }
  do {
    if (!base_mesh_->is_valid_handle(meta_mesh_->property(mhe_connection_, iter))) {
      Trace(iter);
      if (debug_hard_stop_) return;
    }
    iter = meta_mesh_->opposite_halfedge_handle(meta_mesh_->prev_halfedge_handle(iter));
  } while (iter != start);
}

/*!
 * \brief Embedding::MetaEdgeSplit the part of the split performed on the meta mesh
 * \param meh
 * \param bvh
 */
void Embedding::MetaEdgeSplit(OpenMesh::EdgeHandle meh, OpenMesh::VertexHandle bvh) {
  OpenMesh::HalfedgeHandle mhehb;
  auto mheha = meta_mesh_->halfedge_handle(meh, 0);
  auto mvh1 = meta_mesh_->to_vertex_handle(mheha);

  // Add bvh into the meta mesh
  auto mvhnew = meta_mesh_->add_vertex(base_mesh_->point(bvh));

  // ////////////////
  // Connect mhehb
  mhehb = meta_mesh_->new_edge(mvhnew, mvh1);

  // Boundary properties:
  if (meta_mesh_->is_boundary(mheha)) {
    meta_mesh_->set_boundary(mhehb);
  }
  if (meta_mesh_->is_boundary(meta_mesh_->opposite_halfedge_handle(mheha))) {
    meta_mesh_->set_boundary(meta_mesh_->opposite_halfedge_handle(mhehb));
  }

  // he - he
  if (meta_mesh_->next_halfedge_handle(mheha)
      == meta_mesh_->opposite_halfedge_handle(mheha)) {
    meta_mesh_->set_next_halfedge_handle(mhehb, meta_mesh_->opposite_halfedge_handle(mhehb));
  } else {
    meta_mesh_->set_next_halfedge_handle(mhehb, meta_mesh_->next_halfedge_handle(mheha));
    meta_mesh_->set_next_halfedge_handle(meta_mesh_->prev_halfedge_handle(
                                         meta_mesh_->opposite_halfedge_handle(mheha)),
                                         meta_mesh_->opposite_halfedge_handle(mhehb));
  }
  // he - v
  meta_mesh_->set_vertex_handle(mhehb, mvh1);
  meta_mesh_->set_vertex_handle(meta_mesh_->opposite_halfedge_handle(mhehb), mvhnew);
  // v - he
  if (meta_mesh_->is_boundary(meta_mesh_->opposite_halfedge_handle(mheha))) {
    meta_mesh_->set_halfedge_handle(mvhnew, meta_mesh_->opposite_halfedge_handle(mheha));
  } else {
    meta_mesh_->set_halfedge_handle(mvhnew, mhehb);
  }
  if (meta_mesh_->halfedge_handle(mvh1) == meta_mesh_->opposite_halfedge_handle(mheha)) {
    meta_mesh_->set_halfedge_handle(mvh1, meta_mesh_->opposite_halfedge_handle(mhehb));
  }
  // he - f
  meta_mesh_->set_face_handle(mhehb, meta_mesh_->face_handle(mheha));
  meta_mesh_->set_face_handle(meta_mesh_->opposite_halfedge_handle(mhehb),
       meta_mesh_->face_handle(meta_mesh_->opposite_halfedge_handle(mheha)));
  // Rewire mheha
  // he - he
  meta_mesh_->set_next_halfedge_handle(mheha, mhehb);
  meta_mesh_->set_next_halfedge_handle(meta_mesh_->opposite_halfedge_handle(mhehb),
                                       meta_mesh_->opposite_halfedge_handle(mheha));
  // he - v
  meta_mesh_->set_vertex_handle(mheha, mvhnew);

  // ///////////////////
  // Properties
  // vertex properties
  meta_mesh_->property(mv_connection_, mvhnew) = bvh;
  base_mesh_->property(bv_connection_, bvh) = mvhnew;
  // halfedge properties
  for (auto bvoheh : base_mesh_->voh_range(bvh)) {
    if (base_mesh_->property(bhe_connection_, bvoheh)
        == meta_mesh_->opposite_halfedge_handle(mheha)) {
      meta_mesh_->property(mhe_connection_, meta_mesh_->opposite_halfedge_handle(mheha))
          = bvoheh;
      base_mesh_->property(next_heh_, base_mesh_->opposite_halfedge_handle(bvoheh))
          = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
    }
    if (base_mesh_->property(bhe_connection_, bvoheh) == mheha) {
      meta_mesh_->property(mhe_connection_, mhehb) = bvoheh;
      base_mesh_->property(next_heh_, base_mesh_->opposite_halfedge_handle(bvoheh))
          = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
    }
  }
  OverwriteHalfedgeConnection(mhehb, meta_mesh_->property(mhe_connection_, mhehb));

  // Triangulate the two faces adjacent to the new vertex.
  auto start = mheha;
  auto iter = meta_mesh_->next_halfedge_handle(meta_mesh_->next_halfedge_handle(
        meta_mesh_->next_halfedge_handle(start)));
  while (iter != start) {
    AddMetaEdge(start, meta_mesh_->prev_halfedge_handle(iter));
    iter = meta_mesh_->next_halfedge_handle(iter);
  }
  start = meta_mesh_->opposite_halfedge_handle(mhehb);
  iter = meta_mesh_->next_halfedge_handle(meta_mesh_->next_halfedge_handle(
        meta_mesh_->next_halfedge_handle(start)));
  while (iter != start) {
    if (meta_mesh_->is_valid_handle(meta_mesh_->face_handle(iter))) {
      AddMetaEdge(start, meta_mesh_->prev_halfedge_handle(iter));
    }
    iter = meta_mesh_->next_halfedge_handle(iter);
  }
}

/*!
 * \brief Embedding::OverwriteHalfedgeConnection
 * Overwrites the bhe_connection_ parameters of an embedded halfedge starting at bheh
 * to correspond to meta halfedge mheh.
 * This is useful for splits since it saves us two Trace calls.
 * \param mheh
 * \param bheh
 */
void Embedding::OverwriteHalfedgeConnection(OpenMesh::HalfedgeHandle mheh,
                                            OpenMesh::HalfedgeHandle bheh) {
  assert(base_mesh_->is_valid_handle(base_mesh_->property(bv_connection_,
                                     base_mesh_->from_vertex_handle(bheh))));
  assert(base_mesh_->property(bv_connection_, base_mesh_->from_vertex_handle(bheh))
         == meta_mesh_->from_vertex_handle(mheh));
  meta_mesh_->property(mhe_connection_, mheh) = bheh;
  base_mesh_->property(bhe_connection_, bheh) = mheh;
  base_mesh_->property(bhe_connection_, base_mesh_->opposite_halfedge_handle(bheh))
      = meta_mesh_->opposite_halfedge_handle(mheh);
  assert(base_mesh_->property(next_heh_, base_mesh_->opposite_halfedge_handle(bheh))
         == OpenMesh::PolyConnectivity::InvalidHalfedgeHandle);
  while (!meta_mesh_->is_valid_handle(base_mesh_->property(bv_connection_,
             base_mesh_->to_vertex_handle(bheh)))) {
    bheh = base_mesh_->property(next_heh_, bheh);
    base_mesh_->property(bhe_connection_, bheh) = mheh;
    base_mesh_->property(bhe_connection_, base_mesh_->opposite_halfedge_handle(bheh))
        = meta_mesh_->opposite_halfedge_handle(mheh);
  }
  assert(base_mesh_->property(next_heh_, bheh)
         == OpenMesh::PolyConnectivity::InvalidHalfedgeHandle);
  meta_mesh_->property(mhe_connection_, meta_mesh_->opposite_halfedge_handle(mheh))
      = base_mesh_->opposite_halfedge_handle(bheh);
  // Ensure connectivity
  assert(base_mesh_->property(bv_connection_, base_mesh_->from_vertex_handle(
                                meta_mesh_->property(mhe_connection_, mheh)))
         == meta_mesh_->from_vertex_handle(mheh));
  assert(base_mesh_->property(bv_connection_, base_mesh_->from_vertex_handle(
         meta_mesh_->property(mhe_connection_, meta_mesh_->opposite_halfedge_handle(mheh))))
         == meta_mesh_->from_vertex_handle(meta_mesh_->opposite_halfedge_handle(mheh)));
}

/*!
 * \brief Embedding::FaceSplit Splits the meta face of mheh at base vertex bvhs
 * \param bvh
 * \param mheh
 * \param verbose
 */
void Embedding::FaceSplit(OpenMesh::VertexHandle bvh, OpenMesh::HalfedgeHandle mheh,
                          bool verbose) {
  assert(!IsSectorBorder(bvh));
  if (!meta_mesh_->is_valid_handle(mheh)) {
    mheh = FindFaceHalfedge(bvh);
  }
  if (verbose) {
    qDebug() << "Meta Face split";
  }
  MetaFaceSplit(mheh, bvh);
  if (verbose) {
    qDebug() << "Vertex processing";
  }
  ProcessVertex(bvh);
  auto mvh = base_mesh_->property(bv_connection_, bvh);
  if (verbose) {
    qDebug() << "Face split loop";
  }
  for (auto mvoheh : meta_mesh_->voh_range(mvh)) {
    if (!base_mesh_->is_valid_handle(meta_mesh_->property(mhe_connection_, mvoheh))) {
      Trace(mvoheh);
      if (debug_hard_stop_) return;
    }
  }
}

/*!
 * \brief Embedding::FindFaceHalfedge
 * \param bvh
 * \return a halfedge of the meta face bvh lies on. or InvalidHandle if bvh does not
 * lie on a meta face.
 */
OpenMesh::HalfedgeHandle Embedding::FindFaceHalfedge(OpenMesh::VertexHandle bvh) {
  std::queue<OpenMesh::VertexHandle> queue;
  OpenMesh::VPropHandleT<bool> marked;
  base_mesh_->add_property(marked, "Marked search traversal.");
  queue.push(bvh);
  while (!queue.empty()) {
    auto vh = queue.front();
    queue.pop();
    if (!base_mesh_->property(marked, vh)) {
      base_mesh_->property(marked, vh) = true;
      for (auto bvoheh : base_mesh_->voh_range(vh)) {
        if (meta_mesh_->is_valid_handle(base_mesh_->property(bhe_connection_, bvoheh))) {
          auto iter = base_mesh_->opposite_halfedge_handle(
                base_mesh_->prev_halfedge_handle(bvoheh));
          while (!meta_mesh_->is_valid_handle(base_mesh_->property(bhe_connection_, iter))) {
            if (base_mesh_->property(marked, base_mesh_->to_vertex_handle(iter))) {
              base_mesh_->remove_property(marked);
              return base_mesh_->property(bhe_connection_, bvoheh);
            }
            iter = base_mesh_->opposite_halfedge_handle(
                            base_mesh_->prev_halfedge_handle(iter));
          }
        }
        if (!base_mesh_->property(marked, base_mesh_->to_vertex_handle(bvoheh))) {
          queue.push(base_mesh_->to_vertex_handle(bvoheh));
        }
      }
    }
  }
  base_mesh_->remove_property(marked);
  return OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
}

/*!
 * \brief Embedding::MetaFaceSplit meta mesh part of the face split
 * \param mheh
 * \param bvh
 */
void Embedding::MetaFaceSplit(OpenMesh::HalfedgeHandle mheh, OpenMesh::VertexHandle bvh) {
  // Add bvh into the meta mesh
  // try highlevel method, implement low level if this does not work
  meta_mesh_->split(meta_mesh_->face_handle(mheh), base_mesh_->point(bvh));

  // add properties
  auto mvh = meta_mesh_->to_vertex_handle(meta_mesh_->next_halfedge_handle(mheh));
  meta_mesh_->property(mv_connection_, mvh) = bvh;
  base_mesh_->property(bv_connection_, bvh) = mvh;
}

/*!
 * \brief Embedding::Relocate
 * Relocates mvh to bvh
 * The lowlevel method is better for most purposes, but less robust to extreme edge cases
 * If you are relocating a vertex with edges that cannot be traced properly regardless of order
 * eg. a vertex with 3 self-edges in a genus 4 mesh, do not use the low level method
 * the high level method is a concatenation of a split and a collapse and should thus always work
 * but it doesnt clean the base mesh up nearly as well.
 * \param mvh
 * \param bvh
 * \param verbose
 * \param lowlevel enabled by default, but disable it where this fails. The low level method still
 * needs a bit of work and I might not have enough time to make it handle all edge cases (some
 * edge cases will remain impossible regardless)
 */
void Embedding::Relocate(OpenMesh::VertexHandle mvh, OpenMesh::VertexHandle bvh, bool verbose,
                         bool lowlevel) {
  if (!base_mesh_->is_valid_handle(bvh)) {
    return;
  }
  if (meta_mesh_->is_valid_handle(base_mesh_->property(bv_connection_, bvh))
      && !lowlevel) {
    return;
  }
  // Disallow relocation of boundary vertices away from their boundary.
  if (meta_mesh_->is_boundary(mvh)) {
    auto mehb = GetMetaEdge(bvh);
    if (!base_mesh_->is_boundary(bvh)) {
      if (verbose) {
        qDebug() << "Cannot relocate a boundary vertex away from its boundary.";
      }
      return;
    } else if (mehb != meta_mesh_->edge_handle(meta_mesh_->halfedge_handle(mvh))
                && mehb != meta_mesh_->edge_handle(
                 meta_mesh_->prev_halfedge_handle(meta_mesh_->halfedge_handle(mvh)))) {
      return;
    }
  }

  if (lowlevel) {
    LowLevelRelocate(mvh, bvh, verbose);
  } else {
    CompositeRelocate(mvh, bvh, verbose);
  }
}

/*!
 * \brief Embedding::LowLevelRelocate
 * Relocation as a lower level function than CompositeRelocate
 * Fails if all edges around mvh are self-edges (eg. genus 4 object with 1 vertex)
 * \param mvh
 * \param bvh
 * \param verbose
 */
void Embedding::LowLevelRelocate(OpenMesh::VertexHandle mvh, OpenMesh::VertexHandle bvh,
                                 bool verbose) {
  // This method does not do edge splits, relocate only inside the patch.
  auto meh = GetMetaEdge(bvh);
  if (IsSectorBorder(bvh)
      && meta_mesh_->is_valid_handle(meh)
      && !(meta_mesh_->to_vertex_handle(meta_mesh_->halfedge_handle(meh,0)) == mvh
         || meta_mesh_->to_vertex_handle(meta_mesh_->halfedge_handle(meh,1)) == mvh)) {
    if (verbose) {
      qDebug() << "Cannot relocate meta vertex " << mvh.idx() << " to base vertex "
               << bvh.idx() << " since it doesn't lie in an incident face";
    }
    return;
  }

  // Ensure that bvh lies in the patch around mvh
  auto mheh = FindFaceHalfedge(bvh);
  auto mhehiter = mheh;
  bool correct = false;
  do {
    if (meta_mesh_->to_vertex_handle(mhehiter) == mvh) {
      correct = true;
      mheh = mhehiter;
    } else {
      mhehiter = meta_mesh_->next_halfedge_handle(mhehiter);
    }
  } while (mhehiter!=mheh);
  if (!correct) {
    if (verbose) {
      qDebug() << "Cannot relocate meta vertex " << mvh.idx() << " to base vertex "
               << bvh.idx() << " since it doesn't lie in an incident face";
    }
    return;
  }

  // Do the relocation
  // Relocate mvh to bvh
  base_mesh_->property(bv_connection_, meta_mesh_->property(mv_connection_, mvh))
      = OpenMesh::PolyConnectivity::InvalidVertexHandle;
  meta_mesh_->property(mv_connection_, mvh) = bvh;
  base_mesh_->property(bv_connection_, bvh) = mvh;
  // Delete the edges inside the patch:
  for (auto moh : meta_mesh_->voh_range(mvh)) {
    if (!(meta_mesh_->from_vertex_handle(moh) == meta_mesh_->to_vertex_handle(moh))
        || base_mesh_->is_valid_handle(meta_mesh_->property(mhe_connection_, moh))) {
      RemoveMetaEdgeFromBaseMesh(moh);
    }
  }
  // Use this opportunity to clean up the mesh while the patch is empty
  CleanUpBaseMesh();
  // Retrace the patch around mvh
  // Order can be very important when doing this, as otherwise vertices can be blocked off
  // by edges. Order of retracing:
  // 1: Boundary edges
  // 2: A non-duplicate representative of each connected component in the patch around mvh.
  // In most cases there will only be one component
  // 3: Duplicate representatives of unrepresented connected components
  // 4: Self-edges. The representatives of components should wall those off and confine them
  // to areas so that they cannot vanish
  // 5: All other edges that are non-duplicate
  // 6: All other duplicates
  OpenMesh::HPropHandleT<bool> he_selected;
  OpenMesh::VPropHandleT<uint> v_selected;
  OpenMesh::VPropHandleT<bool> group_selected;
  std::queue<OpenMesh::HalfedgeHandle> tracingqueue;
  meta_mesh_->add_property(he_selected, "select edges for tracing order");
  meta_mesh_->add_property(group_selected, "select groups for tracing order");
  meta_mesh_->add_property(v_selected, "select vertices for tracing order");
  // 0: initialize properties
  meta_mesh_->property(group_selected, mvh) = false;
  std::vector<OpenMesh::HalfedgeHandle> moh_range;

  for (auto moh : meta_mesh_->voh_range(mvh)) {
    meta_mesh_->property(he_selected, moh) = false;
    meta_mesh_->property(he_selected, meta_mesh_->opposite_halfedge_handle(moh)) = false;
    meta_mesh_->property(group_selected, meta_mesh_->to_vertex_handle(moh)) = false;
    meta_mesh_->property(v_selected, meta_mesh_->to_vertex_handle(moh)) = 0;
    moh_range.push_back(moh);
  }
  for (auto moh : meta_mesh_->voh_range(mvh)) {
    meta_mesh_->property(v_selected, meta_mesh_->to_vertex_handle(moh)) += 1;
  }
  std::random_shuffle(moh_range.begin(), moh_range.end());

  // 1: find boundary edges.
  for (auto moh : moh_range) {
    meta_mesh_->property(he_selected, moh) = false;
    meta_mesh_->property(he_selected, meta_mesh_->opposite_halfedge_handle(moh)) = false;
    if (!meta_mesh_->property(he_selected, moh)
        && (meta_mesh_->is_boundary(moh)
        || meta_mesh_->is_boundary(meta_mesh_->opposite_halfedge_handle(moh)))) {
      tracingqueue.push(moh);
      meta_mesh_->property(he_selected, moh) = true;
      meta_mesh_->property(he_selected, meta_mesh_->opposite_halfedge_handle(moh)) = true;
      meta_mesh_->property(group_selected, meta_mesh_->to_vertex_handle(moh)) = true;
      TraverseGroup(mvh, moh, group_selected);
    }
  }

  // 2: find non-duplicate group representatives
  for (auto moh : moh_range) {
    // non-self-edges
    if (meta_mesh_->from_vertex_handle(moh)
        != meta_mesh_->to_vertex_handle(moh)) {
      // unselected vertex -> new group found -> take the representative and select it
      if (!meta_mesh_->property(he_selected, moh)
          && !meta_mesh_->property(group_selected, meta_mesh_->to_vertex_handle(moh))
          && meta_mesh_->property(v_selected,
                        meta_mesh_->to_vertex_handle(moh)) == 1) {
        tracingqueue.push(moh);
        meta_mesh_->property(he_selected, moh) = true;
        meta_mesh_->property(he_selected, meta_mesh_->opposite_halfedge_handle(moh)) = true;
        meta_mesh_->property(group_selected, meta_mesh_->to_vertex_handle(moh)) = true;
        TraverseGroup(mvh, moh, group_selected);
      }
    }
  }

  // 3: find duplicate group representatives
  for (auto moh : moh_range) {
    // non-self-edges
    if (meta_mesh_->from_vertex_handle(moh)
        != meta_mesh_->to_vertex_handle(moh)) {
      // unselected vertex -> new group found -> take the representative and select it
      if (!meta_mesh_->property(he_selected, moh)
          && !meta_mesh_->property(group_selected, meta_mesh_->to_vertex_handle(moh))
          && meta_mesh_->property(v_selected,
                        meta_mesh_->to_vertex_handle(moh)) > 1) {
        tracingqueue.push(moh);
        meta_mesh_->property(he_selected, moh) = true;
        meta_mesh_->property(he_selected, meta_mesh_->opposite_halfedge_handle(moh)) = true;
        meta_mesh_->property(group_selected, meta_mesh_->to_vertex_handle(moh)) = true;
        TraverseGroup(mvh, moh, group_selected);
      }
    }
  }

  // 4: find self-edges
  for (auto moh : moh_range) {
    // Also make sure the self-edge is not selected (can happen if boundary self-edge)
    if (meta_mesh_->from_vertex_handle(moh) != meta_mesh_->to_vertex_handle(moh)
        && !meta_mesh_->property(he_selected, moh)) {
      tracingqueue.push(moh);
      meta_mesh_->property(he_selected, moh) = true;
      meta_mesh_->property(he_selected, meta_mesh_->opposite_halfedge_handle(moh)) = true;
    }
  }

  // 5: add the remaining non-duplicate edges to the qeuue
  for (auto moh : moh_range) {
    if (!meta_mesh_->property(he_selected, moh) && meta_mesh_->property(v_selected,
              meta_mesh_->to_vertex_handle(moh)) == 1) {
      tracingqueue.push(moh);
      meta_mesh_->property(he_selected, moh) = true;
      meta_mesh_->property(he_selected, meta_mesh_->opposite_halfedge_handle(moh)) = true;
    }
  }

  // 6: add the remaining duplicate edges
  for (auto moh : moh_range) {
    if (!meta_mesh_->property(he_selected, moh)) {
      tracingqueue.push(moh);
      meta_mesh_->property(he_selected, moh) = true;
      meta_mesh_->property(he_selected, meta_mesh_->opposite_halfedge_handle(moh)) = true;
    }
  }

  // Now that the order is set: finally do the tracing
  std::vector<int> IDs;
  while (!tracingqueue.empty()) {
    auto mhehc = tracingqueue.front();
    if (std::binary_search(IDs.begin(), IDs.end(), mhehc.idx())) {
      qDebug() << "Duplicate edge in tracing queue";
    }
    IDs.push_back(mhehc.idx());
    Trace(mhehc);

    tracingqueue.pop();
    if (debug_hard_stop_) {
      qDebug() << "Relocation failed";
      base_mesh_->status(bvh).set_selected(true);
      return;
    }
  }

  meta_mesh_->remove_property(he_selected);
  meta_mesh_->remove_property(v_selected);
  meta_mesh_->remove_property(group_selected);
}

/*!
 * \brief Embedding::TraverseGroup
 * Traverses a patch border group and marks its members
 * \param mvh
 * \param moh
 * \param groupselected
 */
void Embedding::TraverseGroup(OpenMesh::VertexHandle mvh,
                              OpenMesh::HalfedgeHandle moh,
                              OpenMesh::VPropHandleT<bool> groupselected) {
  assert(meta_mesh_->from_vertex_handle(moh) == mvh);
  assert(meta_mesh_->property(groupselected, meta_mesh_->to_vertex_handle(moh)));
  auto mheh = meta_mesh_->next_halfedge_handle(moh);
  while (!meta_mesh_->property(groupselected, meta_mesh_->to_vertex_handle(mheh))
         && meta_mesh_->from_vertex_handle(mheh) != meta_mesh_->to_vertex_handle(mheh)) {
    if (meta_mesh_->to_vertex_handle(mheh) == mvh) {
      mheh = meta_mesh_->next_halfedge_handle(
            meta_mesh_->opposite_halfedge_handle(mheh));
    } else {
      meta_mesh_->property(groupselected, meta_mesh_->to_vertex_handle(mheh)) = true;
      mheh = meta_mesh_->next_halfedge_handle(mheh);
    }
  }
}

/*!
 * \brief Embedding::CompositeRelocate
 * Relocation as a composite of a split and a collapse
 * \param mvh
 * \param bvh
 * \param verbose
 */
void Embedding::CompositeRelocate(OpenMesh::VertexHandle mvh,
                                  OpenMesh::VertexHandle bvh, bool verbose) {
  if (!IsSectorBorder(bvh)) {
    //qDebug() << "Relocation to Face";
    //qDebug() << "Find Face halfedge";
    auto mheh = FindFaceHalfedge(bvh);
    auto mhehiter = mheh;
    bool correct = false;
    do {
      if (meta_mesh_->to_vertex_handle(mhehiter) == mvh) {
        correct = true;
        mheh = mhehiter;
      } else {
        mhehiter = meta_mesh_->next_halfedge_handle(mhehiter);
      }
    } while (mhehiter!=mheh);
    if (!correct) {
      if (verbose) {
        qDebug() << "Cannot relocate meta vertex " << mvh.idx() << " to base vertex "
                 << bvh.idx() << " since it doesn't lie in an incident face";
      }
      return;
    }

    //qDebug() << "Relocate: Face Split";
    // Insert the new point
    Split(bvh, mheh, verbose);
    if (debug_hard_stop_) return;
    auto mhehc = meta_mesh_->next_halfedge_handle(mheh);
    assert(meta_mesh_->is_valid_handle(mhehc));
    auto mvhn = meta_mesh_->to_vertex_handle(mhehc);
    // Swap to ensure the relocated vertex has the same ID as before relocation
    //qDebug() << "Relocate: Swap";
    MetaSwap(meta_mesh_->from_vertex_handle(mhehc), meta_mesh_->to_vertex_handle(mhehc));
    if (debug_hard_stop_) return;
    //qDebug() << "Relocate: Collapse";
    // Collapse the old point into the new point
    Collapse(mhehc, verbose);
    if (debug_hard_stop_) return;
    for (auto miheh : meta_mesh_->vih_range(mvhn)) {
      //qDebug() << "Relocate: Retracing halfedge " << miheh.idx();
      Retrace(miheh);
    }
  } else {
    //qDebug() << "Relocation to Edge";
    auto meh = GetMetaEdge(bvh);
    auto mvh0 = meta_mesh_->to_vertex_handle(meta_mesh_->halfedge_handle(meh, 0));
    auto mvh1 = meta_mesh_->to_vertex_handle(meta_mesh_->halfedge_handle(meh, 1));
    if (mvh0 != mvh && mvh1 != mvh) {
      if (verbose) {
        qDebug() << "Cannot relocate meta vertex " << mvh.idx() << " to base vertex "
                 << bvh.idx() << " since it doesn't lie in an incident face";
      }
      return;
    }
    if (!meta_mesh_->is_valid_handle(base_mesh_->property(bv_connection_, bvh))) {
      //qDebug() << "Relocate: Edge Split";
      // Insert the new point
      Split(bvh, OpenMesh::PolyConnectivity::InvalidHalfedgeHandle, verbose);
      if (debug_hard_stop_) return;
      auto mhehc = meta_mesh_->opposite_halfedge_handle(
            meta_mesh_->find_halfedge(base_mesh_->property(bv_connection_, bvh), mvh));
      assert(meta_mesh_->is_valid_handle(mhehc));
      auto mvhn = meta_mesh_->to_vertex_handle(mhehc);
      // Swap to ensure the relocated vertex has the same ID as before relocation
      //qDebug() << "Relocate: Swap";
      MetaSwap(meta_mesh_->from_vertex_handle(mhehc), meta_mesh_->to_vertex_handle(mhehc));
      if (debug_hard_stop_) return;
      //qDebug() << "Relocate: Collapse";
      // Collapse the old point into the new point
      Collapse(mhehc, verbose);
      if (debug_hard_stop_) return;
      for (auto miheh : meta_mesh_->vih_range(mvhn)) {
        //qDebug() << "Relocate: Retracing halfedge " << miheh.idx();
        Retrace(miheh);
      }
    }
    //qDebug() << "Relocation finished.";
  }
}

/*!
 * \brief Embedding::Split
 * Splits at point bvh; if it is on an edge performs an edge split otherwise
 * a face split. mheh is an optional parameter for face splits denoting a
 * half-edge of the nearest face. Use for optimization, otherwise the nearest
 * half-edge is searched for from bvh.
 * \param bvh
 * \param mheh
 * \param verbose
 */
void Embedding::Split(OpenMesh::VertexHandle bvh, OpenMesh::HalfedgeHandle mheh,
                      bool verbose) {
  if (meta_mesh_->is_valid_handle(base_mesh_->property(bv_connection_, bvh))) {
    qDebug() << "Cannot split at vertex " << bvh.idx() << " since it is already a meta vertex";
  } else if (IsSectorBorder(bvh)) {
    //qDebug() << "Edge split";
    EdgeSplit(GetMetaEdge(bvh), bvh, verbose);
  } else {
    //qDebug() << "Face split";
    FaceSplit(bvh, mheh, verbose);
  }
}

/*!
 * \brief Embedding::Collapse
 * Collapses the FROM vertex into the TO vertex
 * \param mheh
 * \param verbose
 */
void Embedding::Collapse(OpenMesh::HalfedgeHandle mheh, bool verbose) {
  if (debug_hard_stop_) return;

  //qDebug() << "Calling collapse";
  // Check if collapsing is ok
  if (!IsCollapseOkay(mheh, verbose)) {
    if (verbose) {
      qDebug() << "Cannot collapse halfedge " << mheh.idx();
    }
    return;
  }

  auto mvh = meta_mesh_->to_vertex_handle(mheh);
  auto mheho = meta_mesh_->opposite_halfedge_handle(mheh);
  auto mhehn = meta_mesh_->next_halfedge_handle(mheh);
  auto mhehp = meta_mesh_->prev_halfedge_handle(mheh);
  auto mhehon = meta_mesh_->next_halfedge_handle(mheho);
  auto mhehop = meta_mesh_->prev_halfedge_handle(mheho);
  auto mvhdel = meta_mesh_->from_vertex_handle(mheh);
  auto bvhdel = meta_mesh_->property(mv_connection_, mvhdel);

  if (verbose
      && !meta_mesh_->is_boundary(mheh)
      && !meta_mesh_->is_boundary(mheho)) {
    int ringeuler = OneRingEuler(mvhdel);
    if (ringeuler != 1) {
      qDebug() << "The patch around the vertex " << mvhdel.idx() << " that is to be collapsed"
                  " has euler characteristic " << ringeuler << ".";
    }
  }

  //  Two pass tracing
  //qDebug() << "First Trace Pass.";
  // First pass : trace into the right topological area, bend in the meta mesh
  // this step implicitly collapses the meta edge
  if (!meta_mesh_->is_boundary(mheh)) {
    while (!meta_mesh_->status(mheh).deleted()
           &&  meta_mesh_->prev_halfedge_handle(mheh) !=
               meta_mesh_->opposite_halfedge_handle(mheh)) {
      auto mhehcurr = meta_mesh_->prev_halfedge_handle(mheh);
      BendMetaEdgeForwards(mhehcurr);
      if (meta_mesh_->is_boundary(meta_mesh_->opposite_halfedge_handle(mhehcurr))
          || (meta_mesh_->prev_halfedge_handle(mheh) ==
          meta_mesh_->opposite_halfedge_handle(mheh)
          && meta_mesh_->from_vertex_handle(mhehcurr)
              != meta_mesh_->to_vertex_handle(mhehcurr))) {
        assert(meta_mesh_->prev_halfedge_handle(mheh) ==
               meta_mesh_->opposite_halfedge_handle(mheh));
        // Remove the A->B edge
        RemoveMetaEdgeFromBaseMesh(mheh);
        RemoveMetaEdgeFromMetaMesh(mheh);
      }
      if (meta_mesh_->is_valid_handle(mhehcurr)
          && !meta_mesh_->status(mhehcurr).deleted()) {
        RemoveMetaEdgeFromBaseMesh(mhehcurr);
        if (debug_hard_stop_) return;
        Trace(mhehcurr, true, false);
        if (debug_hard_stop_) return;
      } else {
        assert(!base_mesh_->is_valid_handle(meta_mesh_->property(mhe_connection_, mhehcurr)));
      }
    }
  } else {
    while (!meta_mesh_->status(mheh).deleted()
           &&  meta_mesh_->next_halfedge_handle(
               meta_mesh_->opposite_halfedge_handle(mheh)) != mheh) {
      auto mhehcurr = meta_mesh_->next_halfedge_handle(
            meta_mesh_->opposite_halfedge_handle(mheh));
      BendMetaEdgeBackwards(mhehcurr);
      if (meta_mesh_->is_boundary(meta_mesh_->opposite_halfedge_handle(mhehcurr))
          || (meta_mesh_->prev_halfedge_handle(mheh) ==
          meta_mesh_->opposite_halfedge_handle(mheh)
          && meta_mesh_->from_vertex_handle(mhehcurr)
              != meta_mesh_->to_vertex_handle(mhehcurr))) {
        assert(meta_mesh_->next_halfedge_handle(
               meta_mesh_->opposite_halfedge_handle(mheh)) == mheh);
        // Remove the A->B edge
        RemoveMetaEdgeFromBaseMesh(mheh);
        RemoveMetaEdgeFromMetaMesh(mheh);
      }
      if (meta_mesh_->is_valid_handle(mhehcurr)
          && !meta_mesh_->status(mhehcurr).deleted()) {
        RemoveMetaEdgeFromBaseMesh(mhehcurr);
        if (debug_hard_stop_) return;
        Trace(mhehcurr, true, false);
        if (debug_hard_stop_) return;
      } else {
        assert(!base_mesh_->is_valid_handle(meta_mesh_->property(mhe_connection_, mhehcurr)));
      }
    }
  }

  if (!meta_mesh_->status(mheh).deleted()) {
    //qDebug() << "Foo";
    // Remove the A->B edge here if it was encased by a self-edge
    RemoveMetaEdgeFromBaseMesh(mheh);
    RemoveMetaEdgeFromMetaMesh(mheh);
  }

  // Remove 1-Loop (case: self-edge)
  if (mhehn == mhehop && mhehon == mheh) {
    auto mhehouter = meta_mesh_->next_halfedge_handle(
          meta_mesh_->opposite_halfedge_handle(mhehn));
    assert(meta_mesh_->from_vertex_handle(mhehn) == meta_mesh_->to_vertex_handle(mhehn));
    if (meta_mesh_->is_valid_handle(mhehn)
        && !meta_mesh_->status(mhehn).deleted()) {
      RemoveSelfLoop(mhehn);
    }
    if (meta_mesh_->next_halfedge_handle(meta_mesh_->next_halfedge_handle(mhehouter))
        == mhehouter) {
      RemoveLoop(mhehouter);
      Retrace(mhehouter, true, false);
      if (debug_hard_stop_) return;
    }
  } else if (mhehon == mhehp && mhehn == mheho) {
    auto mhehouter = meta_mesh_->next_halfedge_handle(
          meta_mesh_->opposite_halfedge_handle(mhehon));
    assert(meta_mesh_->from_vertex_handle(mhehon) == meta_mesh_->to_vertex_handle(mhehon));
    if (meta_mesh_->is_valid_handle(mhehon)
        && !meta_mesh_->status(mhehon).deleted()) {
      RemoveSelfLoop(mhehon);
    }
    if (meta_mesh_->next_halfedge_handle(meta_mesh_->next_halfedge_handle(mhehouter))
        == mhehouter) {
      RemoveLoop(mhehouter);
      Retrace(mhehouter, true, false);
      if (debug_hard_stop_) return;
    }
  } else {
    // Remove 2-Loops
    if (meta_mesh_->next_halfedge_handle(meta_mesh_->next_halfedge_handle(mhehn))
        == mhehn) {
      RemoveLoop(mhehn);
    }
    if (meta_mesh_->next_halfedge_handle(meta_mesh_->next_halfedge_handle(mhehop))
        == mhehop) {
      RemoveLoop(mhehop);
    }
    if (meta_mesh_->next_halfedge_handle(meta_mesh_->next_halfedge_handle(mhehp))
        == mhehp) {
      RemoveLoop(mhehp);
    }
    if (meta_mesh_->next_halfedge_handle(meta_mesh_->next_halfedge_handle(mhehon))
        == mhehon) {
      RemoveLoop(mhehon);
    }
  }

  //qDebug() << "Second Trace Pass.";
  // Second pass : Retrace to look nicer
  for (auto mhehit : meta_mesh_->voh_range(mvh)) {
    if (!meta_mesh_->status(mhehit).deleted()) {
      Retrace(mhehit, true, false);
      if (debug_hard_stop_) return;
    }
  }

  // ensure disconnection of the collapsed vertex
  assert(!meta_mesh_->is_valid_handle(base_mesh_->property(bv_connection_, bvhdel)));
  assert(meta_mesh_->status(mvhdel).deleted());

  // Sanity tests
  for (auto mvoheh : meta_mesh_->voh_range(mvh)) {
    assert(!meta_mesh_->status(mvoheh).deleted());
    assert(base_mesh_->is_valid_handle(meta_mesh_->property(mhe_connection_, mvoheh)));
    assert(base_mesh_->property(bhe_connection_, meta_mesh_->property(mhe_connection_, mvoheh))
           == mvoheh);
  }

  //qDebug() << "Collapse finished";
}

/*!
 * \brief Embedding::IsCollapseOkay
 * \param mheh
 * \param verbose
 * \return whether mheh can be collapsed
 */
bool Embedding::IsCollapseOkay(OpenMesh::HalfedgeHandle mheh, bool verbose) {
  if (!meta_mesh_->is_valid_handle(meta_mesh_->edge_handle(mheh)))
  {
    if (verbose) {
      qDebug() << "Invalid halfedge handle";
    }
    return false;
  }
  // is edge already deleted?
  if (meta_mesh_->status(meta_mesh_->edge_handle(mheh)).deleted())
  {
    if (verbose) {
      qDebug() << "Deleted halfedge";
    }
    return false;
  }

  // is a boundary vertex being collapsed along a non-boundary edge?
  if (meta_mesh_->is_boundary(meta_mesh_->from_vertex_handle(mheh))
      && !meta_mesh_->is_boundary(mheh)
      && !meta_mesh_->is_boundary(meta_mesh_->opposite_halfedge_handle(mheh))) {
    if (verbose) {
      qDebug() << "Cannot collapse boundary into non-boundary";
    }
    return false;
  }

  if (!base_mesh_->is_valid_handle(meta_mesh_->property(mhe_connection_, mheh)))
  {
    if (verbose) {
      qDebug() << "Connected to an invalid halfedge, this should NOT happen";
    }
    return false;
  }
  if (!base_mesh_->is_valid_handle(meta_mesh_->property(mhe_connection_, mheh)))
  {
    if (verbose) {
      qDebug() << "Unconnected halfedge, this should NOT happen";
    }
    return false;
  }

  // Don't allow collapses when the number of halfedges of the face is equal to the
  // total number of edges (equivalent to the same faces condition, but includes
  // cases where there are self-edges).
  size_t ctr = 1;
  auto mhehnext = meta_mesh_->next_halfedge_handle(mheh);
  while (mheh != mhehnext) {
    ++ctr;
    mhehnext = meta_mesh_->next_halfedge_handle(mhehnext);
  }
  if (ctr >= meta_mesh_->n_edges()) {
    if (verbose) {
      qDebug() << "minimal mesh reached";
    }
    return false;
  }

  // is vertex connected to itself?
  if (meta_mesh_->from_vertex_handle(mheh) == meta_mesh_->to_vertex_handle(mheh)) {
    if (verbose) {
     qDebug() << "self-edge";
    }
    return false;
  }

  // Special case, two non-boundary edges encased by two boundary edges:
  //    A    eg. here: collapsing the A->B halfedge with A->C
  //   /|\   and C->A being boundaries. Allowing this collapse would result
  //  | B |  in 3 connections between A and C, the middle edge is removed as a loop,
  //   \|/   and the remaining face is no longer a triangle.
  //    C    If C->A and A->C are not both boundaries this works fine as two loop edges
  // can be collapsed.
  if (meta_mesh_->next_halfedge_handle(meta_mesh_->opposite_halfedge_handle(
      meta_mesh_->next_halfedge_handle(meta_mesh_->opposite_halfedge_handle(mheh))))
      == mheh
      && meta_mesh_->is_boundary(meta_mesh_->opposite_halfedge_handle(
           meta_mesh_->next_halfedge_handle(mheh)))
      && meta_mesh_->is_boundary(meta_mesh_->opposite_halfedge_handle(
           meta_mesh_->prev_halfedge_handle(
             meta_mesh_->opposite_halfedge_handle(mheh))))
      && meta_mesh_->valence(meta_mesh_->face_handle(mheh)) == 3
      && meta_mesh_->valence(meta_mesh_->face_handle(
                             meta_mesh_->opposite_halfedge_handle(mheh))) == 3) {
    if (verbose) {
      qDebug() << "encased between two boundary faces";
    }
    return false;
  }

  // Are the two faces the same triangles?
  // Deprecated test with the new addition counting edges
  auto mhehiter0 = meta_mesh_->next_halfedge_handle(mheh);
  auto mhehiter1 = meta_mesh_->prev_halfedge_handle(
        meta_mesh_->opposite_halfedge_handle(mheh));
  bool sameface = true;
  while (mhehiter0 != mheh) {
    if (mhehiter0 != meta_mesh_->opposite_halfedge_handle(mhehiter1)) {
      sameface = false;
    }
    mhehiter0 = meta_mesh_->next_halfedge_handle(mhehiter0);
    mhehiter1 = meta_mesh_->prev_halfedge_handle(mhehiter1);
  }
  if (sameface && meta_mesh_->valence(meta_mesh_->face_handle(mheh)) == 3) {
    if (verbose) {
      qDebug() << "same triangle";
    }
    return false;
  }

  // TODO: think of more exceptions
  return true;
}

/*!
 * \brief Embedding::BendMetaEdgeForwards
 * A trick used in collapse, bending edges so as to always trace inside disk topology areas
 * \param mheh
 */
void Embedding::BendMetaEdgeForwards(OpenMesh::HalfedgeHandle mheh) {
  if (meta_mesh_->status(mheh).deleted()) {
    assert(meta_mesh_->from_vertex_handle(mheh) == meta_mesh_->to_vertex_handle(mheh));
    return;
  }
  auto newtomvh = meta_mesh_->to_vertex_handle(meta_mesh_->next_halfedge_handle(mheh));

  auto mhehopp = meta_mesh_->opposite_halfedge_handle(mheh);
  auto mhehoppprev = meta_mesh_->prev_halfedge_handle(mhehopp);
  auto mhehnext = meta_mesh_->next_halfedge_handle(mheh);
  auto mhehnewnext = meta_mesh_->next_halfedge_handle(mhehnext);

  auto mfh = meta_mesh_->face_handle(mheh);
  auto mfho = meta_mesh_->face_handle(mhehopp);

  // sever old connections
  // he - he
  meta_mesh_->set_next_halfedge_handle(mhehoppprev, mhehnext);
  // v - he
  if (meta_mesh_->halfedge_handle(meta_mesh_->to_vertex_handle(mheh)) == mhehopp) {
    meta_mesh_->set_halfedge_handle(meta_mesh_->to_vertex_handle(mheh),
    meta_mesh_->next_halfedge_handle(meta_mesh_->opposite_halfedge_handle(mhehnext)));
  }


  // add new connections
  // he - he
  meta_mesh_->set_next_halfedge_handle(mhehnext, mhehopp);
  meta_mesh_->set_next_halfedge_handle(mheh, mhehnewnext);

  // he - v
  meta_mesh_->set_vertex_handle(mheh, newtomvh);

  // f - v
  meta_mesh_->set_halfedge_handle(mfh, mheh);

  // v - f
  meta_mesh_->set_face_handle(mhehnext, mfho);
}

/*!
 * \brief Embedding::BendMetaEdgeBackwards
 * A trick used in collapse, bending edges so as to always trace inside disk topology areas
 * \param mheh
 */
void Embedding::BendMetaEdgeBackwards(OpenMesh::HalfedgeHandle mheh) {
  if (meta_mesh_->status(mheh).deleted()) {
    assert(meta_mesh_->from_vertex_handle(mheh) == meta_mesh_->to_vertex_handle(mheh));
    return;
  }
  auto newfrommvh = meta_mesh_->from_vertex_handle(meta_mesh_->prev_halfedge_handle(mheh));

  auto mhehprev = meta_mesh_->prev_halfedge_handle(mheh);
  auto mhehopp = meta_mesh_->opposite_halfedge_handle(mheh);
  auto mhehoppnext = meta_mesh_->next_halfedge_handle(mhehopp);
  auto mhehprevopp = meta_mesh_->opposite_halfedge_handle(mhehprev);
  auto mhehprevprev = meta_mesh_->prev_halfedge_handle(mhehprev);

  auto mfh = meta_mesh_->face_handle(mheh);
  auto mfho = meta_mesh_->face_handle(mhehopp);

  // sever old connections
  // he - he
  meta_mesh_->set_next_halfedge_handle(mhehprev, mhehoppnext);
  // v - he
  if (meta_mesh_->halfedge_handle(meta_mesh_->from_vertex_handle(mheh)) == mheh) {
    meta_mesh_->set_halfedge_handle(meta_mesh_->from_vertex_handle(mheh), mhehprevopp);
  }


  // add new connections
  // he - he
  meta_mesh_->set_next_halfedge_handle(mhehopp, mhehprev);
  meta_mesh_->set_next_halfedge_handle(mhehprevprev, mheh);

  // he - v
  meta_mesh_->set_vertex_handle(mhehopp, newfrommvh);

  // f - v
  meta_mesh_->set_halfedge_handle(mfh, mheh);

  // v - f
  meta_mesh_->set_face_handle(mhehprev, mfho);
}

/*!
 * \brief Embedding::RemoveLoop
 * removes a loop of edges AB->BA->AB.. by deleting BA
 * \param mheh is AB
 */
void Embedding::RemoveLoop(OpenMesh::HalfedgeHandle mheh) {
  // make sure this is a loop
  if(meta_mesh_->next_halfedge_handle(meta_mesh_->next_halfedge_handle(mheh)) != mheh) {
    return;
  }

  // if the loop is already deleted, return  (should not happen=
  if (meta_mesh_->status(mheh).deleted()
      || meta_mesh_->status(meta_mesh_->next_halfedge_handle(mheh)).deleted()) {
    qDebug() << "Found a halfedge pointing towards a deleted halfedge in RemoveLoop.";
    return;
  }

  // if the deleted edge would be a boundary, remove the other edge instead.
  if (meta_mesh_->is_boundary(meta_mesh_->next_halfedge_handle(mheh))
      || meta_mesh_->is_boundary(meta_mesh_->opposite_halfedge_handle(
                                   meta_mesh_->next_halfedge_handle(mheh)))) {
    // Avoid endless loops; if both edges are boundaries keep both.
    if (meta_mesh_->is_boundary(mheh)
        || meta_mesh_->is_boundary(meta_mesh_->opposite_halfedge_handle(mheh))) {
      return;
    }
    RemoveLoop(meta_mesh_->next_halfedge_handle(mheh));
    return;
  }

  auto mheho = meta_mesh_->opposite_halfedge_handle(mheh);
  auto mhehdel = meta_mesh_->next_halfedge_handle(mheh);
  auto mhehdelo = meta_mesh_->opposite_halfedge_handle(mhehdel);
  auto mvh0 = meta_mesh_->from_vertex_handle(mheh);
  auto mvh1 = meta_mesh_->to_vertex_handle(mheh);
  auto mfh = meta_mesh_->face_handle(mhehdelo);
  auto mfhdel = meta_mesh_->face_handle(mheh);

  // he -> he
  meta_mesh_->set_next_halfedge_handle(meta_mesh_->prev_halfedge_handle(mhehdelo), mheh);
  meta_mesh_->set_next_halfedge_handle(mheh, meta_mesh_->next_halfedge_handle(mhehdelo));

  if (meta_mesh_->next_halfedge_handle(mheho) == mhehdelo) {
    meta_mesh_->set_next_halfedge_handle(mheho, mheh);
  }

  // v -> he
  if (meta_mesh_->halfedge_handle(mvh0) == mhehdelo) {
    meta_mesh_->set_halfedge_handle(mvh0, mheh);
  }
  if (meta_mesh_->halfedge_handle(mvh1) == mhehdel) {
    meta_mesh_->set_halfedge_handle(mvh1, meta_mesh_->opposite_halfedge_handle(mheh));
  }

  // he -> f
  if (meta_mesh_->is_valid_handle(mfh)) meta_mesh_->set_face_handle(mheh, mfh);

  // f -> he
  if (meta_mesh_->is_valid_handle(mfh)) meta_mesh_->set_halfedge_handle(mfh, mheh);

  // delete stuff in the base mesh
  RemoveMetaEdgeFromBaseMesh(mhehdel);

  // delete stuff in the meta mesh
  meta_mesh_->status(mfhdel).set_deleted(true);
  meta_mesh_->status(meta_mesh_->edge_handle(mhehdel)).set_deleted(true);
  if (meta_mesh_->has_halfedge_status()) {
    meta_mesh_->status(mhehdel).set_deleted(true);
    meta_mesh_->status(mhehdelo).set_deleted(true);
  }

  assert(meta_mesh_->prev_halfedge_handle(mheh) != mhehdel);
  assert(meta_mesh_->prev_halfedge_handle(meta_mesh_->next_halfedge_handle(mheh)) != mhehdelo);
}

/*!
 * \brief Embedding::RemoveSelfLoop
 * removes a self loop mheh->mheh->mheh...
 * \param mheh
 */
void Embedding::RemoveSelfLoop(OpenMesh::HalfedgeHandle mheh) {
  // Avoid removing boundaries
  if (meta_mesh_->is_boundary(mheh)
      || meta_mesh_->is_boundary(meta_mesh_->opposite_halfedge_handle(mheh))) {
    return;
  }

  assert(meta_mesh_->next_halfedge_handle(mheh) == mheh);
  auto mheho = meta_mesh_->opposite_halfedge_handle(mheh);
  auto mhehop = meta_mesh_->prev_halfedge_handle(mheho);
  auto mhehon = meta_mesh_->next_halfedge_handle(mheho);
  // if this assert fails the face of mheho has only 2 edges, don't allow it
  assert(mhehop != mhehon);
  auto mfhdel = meta_mesh_->face_handle(mheh);
  auto mvh = meta_mesh_->to_vertex_handle(mheh);

  // he - he
  meta_mesh_->set_next_halfedge_handle(mhehop, mhehon);

  // v - he
  if (meta_mesh_->halfedge_handle(mvh) == mheh
      || meta_mesh_->halfedge_handle(mvh) == mheho) {
    meta_mesh_->set_halfedge_handle(mvh, mhehon);
  }

  // delete stuff in the base mesh
  RemoveMetaEdgeFromBaseMesh(mheh);

  // delete stuff
  meta_mesh_->status(mfhdel).set_deleted(true);
  meta_mesh_->status(meta_mesh_->edge_handle(mheh)).set_deleted(true);
  if (meta_mesh_->has_halfedge_status()) {
    meta_mesh_->status(mheh).set_deleted(true);
    meta_mesh_->status(mheho).set_deleted(true);
  }
}

/*!
 * \brief Embedding::OneRingEuler
 * \param mvh
 * \return the euler characteristic of the 1-ring around mvh
 */
int Embedding::OneRingEuler(OpenMesh::VertexHandle mvh) {
  auto mheh = meta_mesh_->halfedge_handle(mvh);
  // V - E + F = g
  // Find euler characteristic of mvhdel 1-ring
  OpenMesh::VPropHandleT<bool> v_marked;
  OpenMesh::HPropHandleT<bool> h_marked;
  OpenMesh::FPropHandleT<bool> f_marked;
  meta_mesh_->add_property(v_marked, "marked vertices");
  meta_mesh_->add_property(h_marked, "marked halfedges");
  meta_mesh_->add_property(f_marked, "marked faces");
  auto iter = mheh;
  do {
    meta_mesh_->property(h_marked, iter) = false;
    meta_mesh_->property(v_marked, meta_mesh_->to_vertex_handle(iter)) = false;
    meta_mesh_->property(f_marked, meta_mesh_->face_handle(iter)) = false;
    if (meta_mesh_->to_vertex_handle(iter) == mvh) {
      iter = meta_mesh_->opposite_halfedge_handle(iter);
    } else {
      iter = meta_mesh_->next_halfedge_handle(iter);
    }
  } while (iter != mheh);
  int n_v = 0;
  int n_f = 0;
  int n_e = 0;
  iter = mheh;
  do {
    if (!meta_mesh_->property(h_marked, iter)) {
      ++n_e;
      meta_mesh_->property(h_marked, iter) = true;
      meta_mesh_->property(h_marked, meta_mesh_->opposite_halfedge_handle(iter)) = true;
    }
    if (!meta_mesh_->property(v_marked, meta_mesh_->to_vertex_handle(iter))) {
      ++n_v;
      meta_mesh_->property(v_marked, meta_mesh_->to_vertex_handle(iter)) = true;
    }
    if (!meta_mesh_->property(f_marked, meta_mesh_->face_handle(iter))) {
      ++n_f;
      meta_mesh_->property(f_marked, meta_mesh_->face_handle(iter)) = true;
    }
    if (meta_mesh_->to_vertex_handle(iter) == mvh) {
      iter = meta_mesh_->opposite_halfedge_handle(iter);
    } else {
      iter = meta_mesh_->next_halfedge_handle(iter);
    }
  } while (iter != mheh);
  int euler = n_v-n_e+n_f;
  meta_mesh_->remove_property(v_marked);
  meta_mesh_->remove_property(h_marked);
  meta_mesh_->remove_property(f_marked);
  return euler;
}

/*!
 * \brief Embedding::MetaSwap
 * swaps all pointers and properties relevant to the embedding structure between mvh0 and mvh1
 * this is used in the composite relocation method to preserve vertex ids
 * \param mvh0
 * \param mvh1
 */
void Embedding::MetaSwap(OpenMesh::VertexHandle mvh0, OpenMesh::VertexHandle mvh1) {
  // Store mvh0 into temp
  auto tempcolor = meta_mesh_->color(mvh0);
  auto temppoint = meta_mesh_->point(mvh0);
  auto tempnormal = meta_mesh_->normal(mvh0);
  auto temphalfedgehandle = meta_mesh_->halfedge_handle(mvh0);
  auto tempmvconnection = meta_mesh_->property(mv_connection_, mvh0);
  std::queue<OpenMesh::HalfedgeHandle> to_halfedges;
  auto curr = meta_mesh_->opposite_halfedge_handle(temphalfedgehandle);
  while (meta_mesh_->to_vertex_handle(curr) == mvh0) {
    meta_mesh_->set_vertex_handle(curr, OpenMesh::PolyConnectivity::InvalidVertexHandle);
    to_halfedges.push(curr);
    curr = meta_mesh_->prev_halfedge_handle(meta_mesh_->opposite_halfedge_handle(curr));
  }

  // Write mvh1 into mvh0
  meta_mesh_->set_color(mvh0, meta_mesh_->color(mvh1));
  meta_mesh_->set_point(mvh0, meta_mesh_->point(mvh1));
  meta_mesh_->set_normal(mvh0, meta_mesh_->normal(mvh1));
  meta_mesh_->set_halfedge_handle(mvh0, meta_mesh_->halfedge_handle(mvh1));
  meta_mesh_->property(mv_connection_, mvh0) = meta_mesh_->property(mv_connection_, mvh1);
  base_mesh_->property(bv_connection_, meta_mesh_->property(mv_connection_, mvh0)) = mvh0;
  curr = meta_mesh_->opposite_halfedge_handle(meta_mesh_->halfedge_handle(mvh1));
  while (meta_mesh_->to_vertex_handle(curr) == mvh1) {
    meta_mesh_->set_vertex_handle(curr, mvh0);
    curr = meta_mesh_->prev_halfedge_handle(meta_mesh_->opposite_halfedge_handle(curr));
  }

  // write temp into mvh1
  meta_mesh_->set_color(mvh1, tempcolor);
  meta_mesh_->set_point(mvh1, temppoint);
  meta_mesh_->set_normal(mvh1, tempnormal);
  meta_mesh_->set_halfedge_handle(mvh1, temphalfedgehandle);
  meta_mesh_->property(mv_connection_, mvh1) = tempmvconnection;
  base_mesh_->property(bv_connection_, meta_mesh_->property(mv_connection_, mvh1)) = mvh1;
  while (!to_halfedges.empty()) {
    auto mheh = to_halfedges.front();
    to_halfedges.pop();
    meta_mesh_->set_vertex_handle(mheh, mvh1);
  }
}

/*!
 * \brief Embedding::DumpMetaMeshHalfedgeInfo Infodump, useful for some debugging.
 */
void Embedding::DumpMetaMeshHalfedgeInfo() {
  qDebug() << "The metamesh has " << meta_mesh_->n_halfedges() << " halfedges.";
  for (auto mheh : meta_mesh_->halfedges()) {
    qDebug() << "Meta Halfedge " << mheh.idx() << " pointing from vertex "
             << meta_mesh_->from_vertex_handle(mheh).idx() << " to vertex "
             << meta_mesh_->to_vertex_handle(mheh).idx() << " with next halfedge handle "
             << meta_mesh_->next_halfedge_handle(mheh).idx() << " and previous halfedge handle "
             << meta_mesh_->prev_halfedge_handle(mheh).idx() << " and opposite halfedge handle "
             << meta_mesh_->opposite_halfedge_handle(mheh).idx();
  }
}
