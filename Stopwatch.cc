#include "Stopwatch.hh"

/*!
 * \brief Stopwatch::Stopwatch a simple stopwatch, starts timer when initialized
 */
Stopwatch::Stopwatch() {
  Reset();
}

/*!
 * \brief Stopwatch::Delta
 * \return the time passed since the last timer reset
 */
double Stopwatch::Delta() {
  auto end = std::chrono::time_point_cast<
      std::chrono::milliseconds>(std::chrono::system_clock::now());
  auto delta = std::chrono::duration_cast<std::chrono::milliseconds>(end-start_).count();
  return delta;
}

/*!
 * \brief Stopwatch::Reset reset the timer
 */
void Stopwatch::Reset() {
  start_ = std::chrono::time_point_cast<
      std::chrono::milliseconds>(std::chrono::system_clock::now());
}
