#pragma once

#include <OpenMesh/Core/Mesh/PolyConnectivity.hh>
#include <ObjectTypes/PolyMesh/PolyMesh.hh>
#include <ObjectTypes/TriangleMesh/TriangleMesh.hh>
#include <OpenFlipper/common/Types.hh>
#include <ACG/Utils/HaltonColors.hh>

#include "Embedding.hh"
#include "Stopwatch.hh"

class Testing {
public:
  Testing(Embedding* embedding);
  ~Testing() {}

  void TriangulationTests();
  void MeshTests();
  void OperationTests();
  void ResetStop() {stop_=false;}
  void DisplayMeshInformation();

  // Triangulation
  void TestVertices();
  void TestHalfedges();
  void TestHalfedgesInitialTriangulation();
  void TestFaces();
  void TestMetaMesh();

  void TestVertexConnections();
  void TestHalfedgeConnections();
  void TestHalfedgeSymmetry();
  void TestVoronoiBorderSymmetry();
  void TestVoronoiBorderConnectivity();
  void TestTriMesh();
  void TestMetaMeshHalfedgeConnectivity();

  // Basic Operation Tests
  void TestFlips(bool nostop = false);
  void TestSplits(bool nostop = false);
  void TestCollapses(bool nostop = false);
  void TestRelocation(bool nostop = false);

private:
  void TestHalfedgeConnection(OpenMesh::HalfedgeHandle mheh);
  void TestBorderTraversal(OpenMesh::HalfedgeHandle bheh,
                           OpenMesh::HalfedgeHandle mheh);
  void VisualAssert(bool condition, std::string message,
                    std::vector<OpenMesh::VertexHandle> visualize);
  void VisualizeVertex(OpenMesh::VertexHandle bvh);

  bool stop_=false;
  Embedding* embedding_;
};
