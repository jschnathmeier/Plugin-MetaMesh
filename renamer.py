# A simple script to rename the output screenshots of IsotropicRemesher
# so that their names end in sorted numbers; this allows easy use as
# frames to render a video, eg. with ffmpeg:
# ffmpeg -f image2 -pix_fmt yuv420p -i target1-iterations20/IsoRemesh%d.png t1-i20.mp4

from glob import glob
import os

files_list = glob("*.png")
for file in files_list:
    numbers = [file]
    for s in file:
        if s.isdigit():
            numbers.append(s)
    l = len(numbers)
    if l == 2:
        numbers.append("0")
    if l == 3:
        numbers.append(str(int(numbers[1])*5+int(numbers[2])+1))
    if l == 4:
        numbers.append(str((int(numbers[1])*10+int(numbers[2]))*5+int(numbers[3])+1))
    os.rename(numbers[0], "IsoRemesh"+numbers[-1]+".png")
