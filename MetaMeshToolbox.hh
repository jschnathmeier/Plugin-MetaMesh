#pragma once

#include <QtGui>
#include <QGroupBox>
#include <QPushButton>
#include <QDoubleSpinBox>
#include <QVBoxLayout>
#include <QRadioButton>
#include <QComboBox>
#include <QCheckBox>

class MetaMeshToolbox : public QWidget
{
  Q_OBJECT

public:
  MetaMeshToolbox(QWidget* parent = nullptr);

    QComboBox* combo_box_tests_;

    QPushButton* button_triangulate_;
    QPushButton* button_randomize_ratio_;
    QPushButton* button_randomize_total_;
    QPushButton* button_run_test_;
    QCheckBox* checkbox_copy_markings_;
    QPushButton* button_retrace_;
    QPushButton* button_rotate_;
    QPushButton* button_split_;
    QPushButton* button_collapse_;
    QPushButton* button_relocate_;
    QPushButton* button_dummy1_;
    QPushButton* button_dummy2_;
    QPushButton* button_next_;
    QPushButton* button_opp_;
    QPushButton* button_prev_;
    QDoubleSpinBox* input_ratio_;

    // Remeshing
    QPushButton* button_remesh_;
    QComboBox* combo_box_remeshing_smtype_;
    QComboBox* combo_box_remeshing_strype_;
    QComboBox* combo_box_remeshing_crorder_;
    QCheckBox* checkbox_limit_flips_;
    QDoubleSpinBox* input_alpha_;
    QDoubleSpinBox* input_beta_;
    QCheckBox* checkbox_data_output_;
    QDoubleSpinBox* input_length_;
    QSpinBox* input_iterations_;
};

