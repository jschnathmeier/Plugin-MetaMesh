#include "Testing.hh"
#include <QDebug>
#include <math.h>
#include <random>
#include <math.h>

#define BASE_MESH_ embedding_->base_mesh_
#define META_MESH_ embedding_->meta_mesh_

/*!
 * \brief Testing::Testing
 * \param embedding
 */
Testing::Testing(Embedding* embedding) : embedding_(embedding) {

}

/*!
 * \brief Testing::TriangulationTests tests the triangulation, however the bhe_border
 * and mhe_border properties cannot be tested properly on meshes with boundaries, thus
 * this test is deprecated and disabled for now.
 */
void Testing::TriangulationTests() {
  ResetStop();
  if (META_MESH_ == nullptr) {
    qDebug() << "Uninitialized Meta Mesh, run some triangulation first.";
    return;
  }

  qDebug() << "Deprecated test";
  return;
  qDebug() << "Testing Meta Mesh properties and connectivity after an intial triangulation.";

  Stopwatch* swatch = new Stopwatch();

  TestVertices();
  if (stop_) return;
  TestHalfedgesInitialTriangulation();
  if (stop_) return;
  TestFaces();
  if (stop_) return;

  double delta = swatch->Delta();
  qDebug() << "All tests passed, time elapsed: " << delta;

  DisplayMeshInformation();
}

/*!
 * \brief Testing::MeshTests Calls various tests on the mesh that should always pass.
 * These tests do not change the meta mesh
 */
void Testing::MeshTests() {
  ResetStop();
  if (META_MESH_ == nullptr) {
    qDebug() << "Uninitialized Meta Mesh, run some triangulation first.";
    return;
  }
  qDebug() << "Testing Meta Mesh properties and connectivity (this should always pass).";

  Stopwatch* swatch = new Stopwatch();

  TestVertices();
  if (stop_) return;
  TestHalfedges();
  if (stop_) return;
  TestFaces();
  if (stop_) return;
  TestMetaMesh();
  if (stop_) return;

  double delta = swatch->Delta();
  qDebug() << "All tests passed, time elapsed: " << delta << "ms.";

  DisplayMeshInformation();
}

/*!
 * \brief Testing::OperationTests Spams basic operations on the mesh and tests for
 * inconsistencies. This will drastically change the meta mesh.
 */
void Testing::OperationTests() {
  ResetStop();
  if (META_MESH_ == nullptr) {
    qDebug() << "Uninitialized Meta Mesh, run some triangulation first.";
    return;
  }

  qDebug() << "Testing basic Trimesh operations";
  Stopwatch* swatch = new Stopwatch();

  TestFlips(true);
  if (stop_) return;
  TestSplits(true);
  if (stop_) return;
  TestCollapses(true);
  if (stop_) return;
  TestRelocation(true);
  if (stop_) return;

  double delta = swatch->Delta();
  qDebug() << "All tests passed, time elapsed: " << delta << "ms.";
}

/*!
 * \brief Testing::DisplayMeshInformation outputs various information on the meta mesh
 * such as numbers of elements, avg. edge valence and standard deviation from it etc.
 */
void Testing::DisplayMeshInformation() {
  unsigned long baseeuler = BASE_MESH_->n_vertices() - BASE_MESH_->n_edges()
      + BASE_MESH_->n_faces();
  unsigned long  metaeuler = META_MESH_->n_vertices() - META_MESH_->n_edges()
      + META_MESH_->n_faces();
  double basegenus = (2-baseeuler)/2.0;
  double metagenus = (2-metaeuler)/2.0;
  double avgmetaedgelength = 0.0;
  for (auto meh : META_MESH_->edges()) {
    avgmetaedgelength += embedding_->CalculateEdgeLength(meh);
  }
  avgmetaedgelength /= META_MESH_->n_edges();
  double metaedgelengthdeviation = 0.0;
  for (auto meh : META_MESH_->edges()) {
    metaedgelengthdeviation += (avgmetaedgelength-embedding_->CalculateEdgeLength(meh))
        *(avgmetaedgelength-embedding_->CalculateEdgeLength(meh));
  }
  metaedgelengthdeviation /= META_MESH_->n_edges();
  metaedgelengthdeviation = std::sqrt(metaedgelengthdeviation);
  double avgvalence = 0.0;
  double valencedeviation = 0.0;
  for (auto mvh : META_MESH_->vertices()) {
    avgvalence += META_MESH_->valence(mvh);
    valencedeviation += (6-META_MESH_->valence(mvh))*(6-META_MESH_->valence(mvh));
  }
  avgvalence /= META_MESH_->n_vertices();
  valencedeviation /= META_MESH_->n_vertices();
  valencedeviation = std::sqrt(valencedeviation);
  qDebug() << "Base Mesh- V: " << BASE_MESH_->n_vertices()
           << "E: " << BASE_MESH_->n_edges()
           << "F: " << BASE_MESH_->n_faces()
           << "Eul: " << baseeuler
           << "Gen: " << basegenus;
  qDebug() << "Meta Mesh- V: " << META_MESH_->n_vertices()
           << "E: " << META_MESH_->n_edges()
           << "F: " << META_MESH_->n_faces()
           << "Eul: " << metaeuler
           << "Gen: " << metagenus;
  qDebug() << "Embedding- Avg. Edge Length: " << avgmetaedgelength
           << " deviation: " << metaedgelengthdeviation
           << " avg valence: " << avgvalence
           << " deviation from 6: " << valencedeviation;
}

/*!
 * \brief Testing::TestVertices
 */
void Testing::TestVertices() {
  if (META_MESH_->n_vertices() == 0) {
    qDebug() << "Meta Mesh has no vertices.";
  } else {
    TestVertexConnections();
    if (stop_) return;

    //qDebug() << "Passed all vertex tests.";
  }
}

/*!
 * \brief Testing::TestHalfedges
 */
void Testing::TestHalfedges() {
  if (META_MESH_->n_halfedges() == 0) {
    qDebug() << "Meta Mesh has no halfedges.";
    return;
  } else {
    TestHalfedgeConnections();
    if (stop_) return;
    TestHalfedgeSymmetry();
    if (stop_) return;

    //qDebug() << "Passed all halfedge tests.";
  }
}

/*!
 * \brief Testing::TestHalfedgesInitialTriangulation
 */
void Testing::TestHalfedgesInitialTriangulation() {
  if (META_MESH_->n_halfedges() == 0) {
    qDebug() << "Meta Mesh has no halfedges.";
    return;
  } else {
    TestHalfedgeConnections();
    if (stop_) return;
    TestHalfedgeSymmetry();
    if (stop_) return;
    TestVoronoiBorderSymmetry();
    if (stop_) return;
    TestVoronoiBorderConnectivity();
    if (stop_) return;

    //qDebug() << "Passed all halfedge tests.";
  }
}

/*!
 * \brief Testing::TestFaces
 */
void Testing::TestFaces() {
  if (META_MESH_->n_faces() == 0) {
    qDebug() << "Meta Mesh has no faces.";
  } else {
    TestTriMesh();
    if (stop_) return;

    //qDebug() << "Passed all face tests.";
  }
}

/*!
 * \brief Testing::TestMetaMesh
 */
void Testing::TestMetaMesh() {
  if (META_MESH_->n_faces() == 0) {
    qDebug() << "Meta Mesh has no faces.";
  } else {
    TestMetaMeshHalfedgeConnectivity();
    if (stop_) return;

    //qDebug() << "Passed all meta mesh connectivity tests.";
  }
}


/*!
 * \brief Testing::TestVertexConnections
 * Test that each meta vertex is connected to a corresponding
 * base vertex that is also connected to it
 */
void Testing::TestVertexConnections() {
  Stopwatch* swatch = new Stopwatch();
  for (auto mvh : META_MESH_->vertices()) {
    VisualAssert(BASE_MESH_->is_valid_handle(
                   META_MESH_->property(embedding_->mv_connection_, mvh)),
        "Test failed: Unconnected Meta Vertex", {});
    if (stop_) return;
    VisualAssert(BASE_MESH_->property(embedding_->bv_connection_,
                 META_MESH_->property(embedding_->mv_connection_, mvh)) == mvh,
        "Test failed: Base Vertex that Meta Vertex is connected to is not connected back",
        {META_MESH_->property(embedding_->mv_connection_, mvh)});
    if (stop_) return;
  }
  double delta = swatch->Delta();
  qDebug() << "TestVertexConnections passed, time elapsed: " << delta << "ms.";
}

/*!
 * \brief Testing::TestHalfedgeConnections
 * Test that each meta halfedge is connected to a base halfedge that in turn
 * is also connected to it and forms a chain of base half_edges connected via
 * next_heh until it reaches the meta vertex.
 */
void Testing::TestHalfedgeConnections() {
  Stopwatch* swatch = new Stopwatch();
  for (auto mheh : META_MESH_->halfedges()) {
    TestHalfedgeConnection(mheh);
    if (stop_) return;
  }
  double delta = swatch->Delta();
  qDebug() << "TestHalfedgeConnections passed, time elapsed: " << delta << "ms.";
}

/*!
 * \brief Testing::TestHalfedgeConnection
 * \param mheh
 */
void Testing::TestHalfedgeConnection(OpenMesh::HalfedgeHandle mheh) {
  auto bheh = META_MESH_->property(embedding_->mhe_connection_, mheh);
  // Meta Halfedge has a valid connection that has not been deleted
  VisualAssert(BASE_MESH_->is_valid_handle(bheh)
               && !BASE_MESH_->status(bheh).deleted(),
               "Test failed: The meta halfedge " + std::to_string(mheh.idx()) +
               " has no valid base vertex connection at bheh "
               + std::to_string(bheh.idx()), {});
  if (stop_) return;
  // The connected base halfedge also has a connection to the meta halfedge
  VisualAssert(BASE_MESH_->property(embedding_->bhe_connection_, bheh) == mheh,
               "Test failed: The base halfedge is not connected back to the meta halfedge",
              {BASE_MESH_->from_vertex_handle(bheh), BASE_MESH_->to_vertex_handle(bheh)});
  if (stop_) return;
  // Both originate from the same vertex
  VisualAssert(BASE_MESH_->property(embedding_->bv_connection_,
    BASE_MESH_->from_vertex_handle(bheh)) == META_MESH_->from_vertex_handle(mheh),
    "Test failed: The base halfedge originates from a different vertex than the "
    "meta halfedge", {BASE_MESH_->from_vertex_handle(bheh),
    META_MESH_->property(embedding_->mv_connection_, META_MESH_->from_vertex_handle(mheh))});
  if (stop_) return;
  auto curr = BASE_MESH_->property(embedding_->next_heh_, bheh);
  // Test the connectivity of the meta halfedge, it should be a chain of
  // base halfedges connected to the next one via next_heh_ and connected to the meta
  // halfdge via bhe_connection_
  while (BASE_MESH_->is_valid_handle(curr)) {
    //BASE_MESH_->status(curr).set_selected(true);
    auto mheh0 = BASE_MESH_->property(embedding_->bhe_connection_, bheh);
    auto mheh1 = BASE_MESH_->property(embedding_->bhe_connection_, curr);
    VisualAssert(BASE_MESH_->property(embedding_->bhe_connection_, curr) == mheh,
            "Test failed: A base halfedge is not connected to its meta halfedge;"
            " The previous halfedge" + std::to_string(bheh.idx()) + " is connected"
            " to meta halfedge " + std::to_string(mheh0.idx()) + " and the current"
            " halfedge " + std::to_string(curr.idx()) + " is connected to meta"
            " halfedge " + std::to_string(mheh1.idx()) +".",
            {BASE_MESH_->from_vertex_handle(curr), BASE_MESH_->to_vertex_handle(curr)});
    if (stop_) return;
    bheh = curr;
    curr = BASE_MESH_->property(embedding_->next_heh_, curr);
  }
  if (curr.is_valid()) {
    BASE_MESH_->status(bheh).set_selected(true);
  }
  VisualAssert(!curr.is_valid(),
          "Test failed: A meta halfedge contains a halfedge not in the base mesh, the "
          "next_heh_ pointed to by bheh " + std::to_string(bheh.idx()) + " is "
          "valid but not in the base mesh. Marking bheh.",
          {BASE_MESH_->from_vertex_handle(bheh), BASE_MESH_->to_vertex_handle(bheh)});
  if (stop_) return;

  // Test that the final halfedge ends in a base vertex connected to the meta vertex the
  // meta halfedge ends in
  VisualAssert(BASE_MESH_->property(embedding_->bv_connection_,
     BASE_MESH_->to_vertex_handle(bheh)) == META_MESH_->to_vertex_handle(mheh),
     "Test failed: The base halfedge ends in a different vertex than the "
     "meta halfedge", {BASE_MESH_->to_vertex_handle(bheh),
     META_MESH_->property(embedding_->mv_connection_, META_MESH_->to_vertex_handle(mheh))});
  if (stop_) return;
}

/*!
 * \brief Testing::TestHalfedgeSymmetry
 */
void Testing::TestHalfedgeSymmetry() {
  Stopwatch* swatch = new Stopwatch();
  for (auto bheh : BASE_MESH_->halfedges()) {
    auto mheh0 = BASE_MESH_->property(embedding_->bhe_connection_, bheh);
    auto mheh1 = BASE_MESH_->property(embedding_->bhe_connection_,
                                      BASE_MESH_->opposite_halfedge_handle(bheh));
    if (!META_MESH_->is_valid_handle(mheh0)) {
      VisualAssert(!META_MESH_->is_valid_handle(mheh1),
        "Test failed: A base halfedge is connected but not its opposite halfedge",
        {BASE_MESH_->from_vertex_handle(bheh), BASE_MESH_->to_vertex_handle(bheh),
        BASE_MESH_->from_vertex_handle(BASE_MESH_->opposite_halfedge_handle(bheh)),
        BASE_MESH_->to_vertex_handle(BASE_MESH_->opposite_halfedge_handle(bheh))});
      if (stop_) return;
    } else {
      VisualAssert(META_MESH_->opposite_halfedge_handle(mheh0) == mheh1,
        "Test failed: A base halfedge is connected but its opposite halfedge is not"
        " connected the the opposite meta halfedge",
        {BASE_MESH_->from_vertex_handle(bheh), BASE_MESH_->to_vertex_handle(bheh),
        BASE_MESH_->from_vertex_handle(BASE_MESH_->opposite_halfedge_handle(bheh)),
        BASE_MESH_->to_vertex_handle(BASE_MESH_->opposite_halfedge_handle(bheh))});
      if (stop_) return;
    }
  }
  double delta = swatch->Delta();
  qDebug() << "TestHalfedgeSymmetry passed, time elapsed: " << delta << "ms.";
}

/*!
 * \brief Testing::TestVoronoiBorderSymmetry
 */
void Testing::TestVoronoiBorderSymmetry() {
  Stopwatch* swatch = new Stopwatch();
  for (auto bheh : BASE_MESH_->halfedges()) {
    auto mheh0 = BASE_MESH_->property(embedding_->bhe_border_, bheh);
    auto mheh1 = BASE_MESH_->property(embedding_->bhe_border_,
                                      BASE_MESH_->opposite_halfedge_handle(bheh));
    if (!META_MESH_->is_valid_handle(mheh0)) {
      VisualAssert(!META_MESH_->is_valid_handle(mheh1),
        "Test failed: A base halfedge is a border but not its opposite halfedge",
        {BASE_MESH_->from_vertex_handle(bheh), BASE_MESH_->to_vertex_handle(bheh),
        BASE_MESH_->from_vertex_handle(BASE_MESH_->opposite_halfedge_handle(bheh)),
        BASE_MESH_->to_vertex_handle(BASE_MESH_->opposite_halfedge_handle(bheh))});
      if (stop_) return;
    } else {
      VisualAssert(META_MESH_->opposite_halfedge_handle(mheh0) == mheh1,
        "Test failed: A base halfedge is a border but its opposite halfedge is not"
        " connected a border to the opposite meta halfedge",
        {BASE_MESH_->from_vertex_handle(bheh), BASE_MESH_->to_vertex_handle(bheh),
        BASE_MESH_->from_vertex_handle(BASE_MESH_->opposite_halfedge_handle(bheh)),
        BASE_MESH_->to_vertex_handle(BASE_MESH_->opposite_halfedge_handle(bheh))});
      if (stop_) return;
    }
  }
  double delta = swatch->Delta();
  qDebug() << "TestVoronoiBorderSymmetry passed, time elapsed: " << delta << "ms.";
}

/*!
 * \brief Testing::TestVoronoiBorderConnectivity
 * Ensure the correctness and consistency of the voronoi borders, marked by the
 * property handles bhe_border_ and mhe_border_
 */
void Testing::TestVoronoiBorderConnectivity() {
  Stopwatch* swatch = new Stopwatch();
  for (auto mheh : META_MESH_->halfedges()) {
    auto bheh = META_MESH_->property(embedding_->mhe_border_, mheh);
    // Traverse the border on the base mesh and test it
    TestBorderTraversal(bheh, mheh);
    if (stop_) return;
  }
  double delta = swatch->Delta();
  qDebug() << "TestVoronoiBorderConnectivity passed, time elapsed: " << delta << "ms.";
}

/*!
 * \brief Testing::TestBorderTraversal
 * \param bheh
 * \param mheh
 */
void Testing::TestBorderTraversal(OpenMesh::HalfedgeHandle bheh,
                                  OpenMesh::HalfedgeHandle mheh) {
  // Test for existence
  VisualAssert(BASE_MESH_->is_valid_handle(bheh),
               "Test failed: Base halfedge " + std::to_string(bheh.idx()) + " should be"
               " a border region but does not exist", {});
  if (stop_) return;
  // Test for reciprocality
  VisualAssert(BASE_MESH_->property(embedding_->bhe_border_, bheh) == mheh,
               "Test failed: Base halfedge " + std::to_string(bheh.idx()) + " is not "
               "connected to the proper meta halfedge demarking its border",
               {BASE_MESH_->from_vertex_handle(bheh), BASE_MESH_->to_vertex_handle(bheh)});
  if (stop_) return;
  auto bheh0 = BASE_MESH_->next_halfedge_handle(BASE_MESH_->opposite_halfedge_handle(bheh));
  auto bheh1 = BASE_MESH_->next_halfedge_handle(bheh0);
  auto mv0 = META_MESH_->from_vertex_handle(mheh);
  auto mv1 = META_MESH_->to_vertex_handle(mheh);
  // Test for correctness based on voronoi region
  VisualAssert(mv0 == BASE_MESH_->property(embedding_->voronoiID_,
                                     BASE_MESH_->from_vertex_handle(bheh)),
               "Test failed: The voronoiID of the halfedges does not correspond "
               "to its border property",
               {BASE_MESH_->from_vertex_handle(bheh), BASE_MESH_->to_vertex_handle(bheh)});
  if (stop_) return;
  VisualAssert(mv1 == BASE_MESH_->property(embedding_->voronoiID_,
                                     BASE_MESH_->to_vertex_handle(bheh)),
         "Test failed: The voronoiID of the halfedges does not correspond "
         "to its border property",
         {BASE_MESH_->from_vertex_handle(bheh), BASE_MESH_->to_vertex_handle(bheh)});
  if (stop_) return;
  // Traverse further
  if (BASE_MESH_->property(embedding_->voronoiID_, BASE_MESH_->from_vertex_handle(bheh0))
      == mv0
      && BASE_MESH_->property(embedding_->voronoiID_, BASE_MESH_->to_vertex_handle(bheh0))
      == mv1) {
    if (!stop_) TestBorderTraversal(bheh0, mheh);
  } else if (BASE_MESH_->property(embedding_->voronoiID_, BASE_MESH_->from_vertex_handle(bheh1))
             == mv0
      && BASE_MESH_->property(embedding_->voronoiID_, BASE_MESH_->to_vertex_handle(bheh1))
             == mv1) {
    if (!stop_) TestBorderTraversal(bheh1, mheh);
  }
}

/*!
 * \brief Testing::TestTriMesh
 * Make sure the meta mesh is a TriMesh by testing each face
 */
void Testing::TestTriMesh() {
  Stopwatch* swatch = new Stopwatch();
  for (auto mfh : META_MESH_->faces()) {
    auto mheh = META_MESH_->halfedge_handle(mfh);
    auto mheh3 = META_MESH_->next_halfedge_handle(
                 META_MESH_->next_halfedge_handle(
                 META_MESH_->next_halfedge_handle(mheh)));
    // In a triangular face the halfedge 3 after the current halfedge is the current halfedge.
    VisualAssert(mheh == mheh3,
                 "Test failed: a meta face is not a triangle", {});
    if (stop_) return;
  }
  double delta = swatch->Delta();
  qDebug() << "TestTriMesh passed, time elapsed: " << delta << "ms.";
}

/*!
 * \brief Testing::TestMetaMeshHalfedgeConnectivity
 */
void Testing::TestMetaMeshHalfedgeConnectivity() {
  Stopwatch* swatch = new Stopwatch();
  for (auto mheh : META_MESH_->halfedges()) {
    auto bvh0 = META_MESH_->property(embedding_->mv_connection_,
                               META_MESH_->from_vertex_handle(mheh));
    auto bvh1 = META_MESH_->property(embedding_->mv_connection_,
                               META_MESH_->to_vertex_handle(mheh));
    auto mheh1 = META_MESH_->next_halfedge_handle(mheh);
    VisualAssert(META_MESH_->is_valid_handle(mheh1), "mheh " + std::to_string(mheh.idx())
                 + " has an invalid next_halfedge_handle " + std::to_string(mheh1.idx()), {});
    auto mheh2 = META_MESH_->next_halfedge_handle(mheh1);
    VisualAssert(META_MESH_->is_valid_handle(mheh1), "mheh1 " + std::to_string(mheh1.idx())
                 + " has an invalid next_halfedge_handle " + std::to_string(mheh2.idx()), {});
    auto bvh2 = META_MESH_->property(embedding_->mv_connection_,
                               META_MESH_->to_vertex_handle(mheh1));
    auto bvh3 = META_MESH_->property(embedding_->mv_connection_,
                                     META_MESH_->to_vertex_handle(mheh2));
    if (META_MESH_->is_boundary(mheh)) {
      VisualAssert(META_MESH_->is_boundary(META_MESH_->next_halfedge_handle(mheh)),
        "Test failed: a boundary halfedge does not point towards another"
        " boundary halfedge", {bvh0, bvh1, bvh2});
    } else {
      VisualAssert(bvh0 == bvh3,
        "Test failed: a halfedge is not part of a triangle.", {bvh0, bvh1, bvh2, bvh3});
    }
    if (stop_) return;
  }
  double delta = swatch->Delta();
  qDebug() << "TestMetaMeshHalfedgeConnectivity passed, time elapsed: "
           << delta << "ms.";
}

/*!
 * \brief Testing::TestFlips
 * \param nostop
 */
void Testing::TestFlips(bool nostop) {
  Stopwatch* swatch = new Stopwatch();
  if (nostop) ResetStop();
  for (auto meh : META_MESH_->edges()) {
    if (META_MESH_->is_valid_handle(meh) && !META_MESH_->status(meh).deleted()) {
      if (embedding_->IsRotationOkay(meh)) {
        auto mvh0 = META_MESH_->to_vertex_handle(META_MESH_->halfedge_handle(meh, 0));
        auto mvh1 = META_MESH_->to_vertex_handle(META_MESH_->halfedge_handle(meh, 1));
        embedding_->Rotate(meh);
        embedding_->Rotate(meh);
        auto mvh2 = META_MESH_->to_vertex_handle(META_MESH_->halfedge_handle(meh, 0));
        auto mvh3 = META_MESH_->to_vertex_handle(META_MESH_->halfedge_handle(meh, 1));
        VisualAssert(((mvh0 == mvh3) && (mvh1 == mvh2)),
                     "After rotating meta edge" + std::to_string(meh.idx()) + " twice "
                     "the start/end vertices mismatched.", {});
        if (stop_) return;
        embedding_->CleanUpBaseMesh();
      }
    }
  }
  double delta = swatch->Delta();
  qDebug() << "TestFlips passed, time elapsed: " << delta << "ms.";
}

/*!
 * \brief Testing::TestSplits
 * \param nostop
 */
void Testing::TestSplits(bool nostop) {
  Stopwatch* swatch = new Stopwatch();
  if (nostop) ResetStop();
  // Test Edge splits
  for (auto meh : META_MESH_->edges()) {
    if (meh.idx()%5 == 0) {
      embedding_->Split(embedding_->MiddleBvh(meh));
    }
  }
  // Test Face splits
  for (auto bvh : BASE_MESH_->vertices()) {
    if (bvh.idx()%500 == 0) {
      embedding_->Split(bvh);
    }
  }
  double delta = swatch->Delta();
  qDebug() << "TestSplits passed, time elapsed: " << delta << "ms.";
}

/*!
 * \brief Testing::TestCollapses
 * \param nostop
 */
void Testing::TestCollapses(bool nostop) {
  Stopwatch* swatch = new Stopwatch();
  if (nostop) ResetStop();
  size_t n_e = META_MESH_->n_edges()+1;
  //qDebug() << "Number of edges: " << META_MESH_->n_edges();
  while (n_e > META_MESH_->n_edges()) {
    n_e = META_MESH_->n_edges();
    size_t temp = n_e;
    while (temp > 0) {
      if (embedding_->IsCollapseOkay(
            META_MESH_->halfedge_handle(META_MESH_->edge_handle(temp-1), 0))) {
        embedding_->Collapse(META_MESH_->halfedge_handle(META_MESH_->edge_handle(temp-1), 0),
                             true);
        embedding_->MetaGarbageCollection();
        embedding_->CleanUpBaseMesh();
        //qDebug() << "Number of edges: " << META_MESH_->n_edges();
        if (embedding_->ErrStatus()) {
          return;
        }
        //qDebug() << "Number of edges: " << META_MESH_->n_edges();
      }
      --temp;
      if (temp > META_MESH_->n_edges())
        temp = META_MESH_->n_edges();
    }
  }
  double delta = swatch->Delta();
  qDebug() << "TestCollapses passed, time elapsed: " << delta << "ms.";
}

/*!
 * \brief Testing::TestRelocation
 * \param nostop
 */
void Testing::TestRelocation(bool nostop) {
  Stopwatch* swatch = new Stopwatch();
  if (nostop) ResetStop();
  std::random_device rd;  //Will be used to obtain a seed for the random number engine
  std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
  std::uniform_int_distribution<> dis(0, static_cast<int>(BASE_MESH_->n_vertices())-1);
  for (auto mvh : META_MESH_->vertices()) {
    auto bvh = BASE_MESH_->vertex_handle(dis(gen));
    embedding_->Relocate(mvh, bvh);
  }

  double delta = swatch->Delta();
  qDebug() << "TestRelocation passed, time elapsed: " << delta << "ms.";
}

/*!
 * \brief Testing::VisualAssert like an assert(), but doesn't crash the program. Instead
 * stops ongoing tests and visually marks faulty parts in the mesh for visual debugging
 * \param condition assert(condition), otherwise ->
 * \param message output message if condition fails
 * \param visualize visualization if condition fails
 */
void Testing::VisualAssert(bool condition, std::string message,
                           std::vector<OpenMesh::VertexHandle> visualize) {
  if (!condition) {
    stop_ = true;
    qDebug() << QString::fromStdString(message);
    for (auto v : visualize) {
      VisualizeVertex(v);
    }
  }
}

/*!
 * \brief Testing::VisualizeVertex
 * \param bvh vertex to be visualized
 */
void Testing::VisualizeVertex(OpenMesh::VertexHandle bvh) {
  qDebug() << "Marking vertex: " << bvh.idx() << "in white.";
  auto vertex_colors_pph = BASE_MESH_->vertex_colors_pph();
  BASE_MESH_->property(vertex_colors_pph, bvh) = ACG::Vec4f(0.0f, 0.0f, 0.0f, 1.0f);
  for (auto vvh : BASE_MESH_->vv_range(bvh)) {
    if (BASE_MESH_->property(vertex_colors_pph, vvh) != ACG::Vec4f(0.0f, 0.0f, 0.0f, 1.0f)) {
      BASE_MESH_->property(vertex_colors_pph, vvh) = ACG::Vec4f(1.0f, 1.0f, 1.0f, 1.0f);
    }
  }
}
