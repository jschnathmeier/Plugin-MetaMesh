/**
 * Colors.hh
 */
#pragma once

#include <ACG/Math/VectorT.hh>
#include <QColor>

namespace Utils {

static constexpr ACG::Vec4f RWTH_BLUE(0.0, 0.329, 0.624, 1.0);
static constexpr ACG::Vec4f RWTH_GREEN(0.341, 0.671, 0.153, 1.0);
static constexpr ACG::Vec4f RWTH_ORANGE(0.965, 0.659, 0.0, 1.0);
static constexpr ACG::Vec4f RWTH_VIOLETT(0.380, 0.129, 0.345, 1.0);
static constexpr ACG::Vec4f RWTH_RED(0.800, 0.027, 0.118, 1.0);
static constexpr ACG::Vec4f RWTH_MAGENTA(0.890, 0.000, 0.400, 1.0);
static constexpr ACG::Vec4f RWTH_YELLOW(1.000, 0.929, 0.000, 1.0);

static constexpr ACG::Vec4f RWTH_BLACK(0.0, 0.0, 0.0, 1.0);
static constexpr ACG::Vec4f RWTH_GRAY_75(0.392, 0.396, 0.404, 1.0);
static constexpr ACG::Vec4f RWTH_GRAY_50(0.612, 0.620, 0.624, 1.0);

} // namespace Utils
