#pragma once

#include <OpenMesh/Core/Mesh/PolyConnectivity.hh>
#include <ObjectTypes/PolyMesh/PolyMesh.hh>
#include <ObjectTypes/TriangleMesh/TriangleMesh.hh>
#include <OpenFlipper/common/Types.hh>
#include <ACG/Utils/HaltonColors.hh>
#include <Draw.hh>
#include <queue>
#include <cmath>

class Embedding {
public:
  Embedding();
  ~Embedding() {}

  enum TraceFaceAttr {UNPROCESSEDFACE, PREPROCESSEDFACE};
  enum RandomType {RATIO, TOTAL};

  void CopyInitialization(TriMesh& base_mesh, PolyMesh& meta_mesh);
  void SelectionTriangulation(TriMesh& base_mesh, PolyMesh& meta_mesh);
  void RandomTriangulation(TriMesh& base_mesh, PolyMesh& meta_mesh, double ratio,
                           RandomType rtype);
  void ColorizeMetaMesh();

  void Trace(OpenMesh::HalfedgeHandle mheh, bool cleanup = true,
             bool mark_trace = false,
             TraceFaceAttr facetype = PREPROCESSEDFACE);
  void Retrace(OpenMesh::HalfedgeHandle mheh, bool cleanup = true,
               bool mark_trace = false,
               TraceFaceAttr facetype = PREPROCESSEDFACE);
  void Collapse(OpenMesh::HalfedgeHandle mheh, bool verbose = false);
  bool IsCollapseOkay(OpenMesh::HalfedgeHandle mheh, bool verbose = false);
  void Rotate(OpenMesh::EdgeHandle meh);
  bool IsRotationOkay(OpenMesh::EdgeHandle meh);
  void Relocate(OpenMesh::VertexHandle mvh, OpenMesh::VertexHandle bvh, bool verbose = false,
                bool lowlevel = true);
  void Split(OpenMesh::VertexHandle bvh, OpenMesh::HalfedgeHandle
             mheh = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle,
             bool verbose = false);
  bool ErrStatus() {return debug_hard_stop_;}
  void DumpMetaMeshHalfedgeInfo();

  TriMesh* GetBaseMesh() {return base_mesh_;}
  PolyMesh* GetMetaMesh() {return meta_mesh_;}
  OpenMesh::EdgeHandle GetMetaEdge(OpenMesh::VertexHandle bvh);

  bool IsSectorBorder(OpenMesh::VertexHandle bvh, std::list<OpenMesh::HalfedgeHandle>
                       exceptions = {});

  double MetaHalfedgeWeight(OpenMesh::HalfedgeHandle mheh);
  double MetaVertexWeight(OpenMesh::VertexHandle mvh);

  double CalculateEdgeLength(OpenMesh::HalfedgeHandle mheh);
  double CalculateEdgeLength(OpenMesh::EdgeHandle meh);
  double CalculateFlippedEdgeLength(OpenMesh::HalfedgeHandle mheh);
  double CalculateFlippedEdgeLength(OpenMesh::EdgeHandle meh);

  OpenMesh::VertexHandle MiddleBvh(OpenMesh::EdgeHandle meh);
  OpenMesh::VertexHandle MiddleBvh(OpenMesh::HalfedgeHandle mheh);

  void CleanMetaMesh();
  void CleanUpBaseMesh(bool garbagecollection = true);

  void MetaGarbageCollection(std::vector<OpenMesh::VertexHandle*> vh_update = {},
                         std::vector<OpenMesh::HalfedgeHandle*> heh_update = {},
                         std::vector<OpenMesh::FaceHandle*> fh_update= {});
  void BaseGarbageCollection(std::vector<OpenMesh::VertexHandle*> vh_update = {},
                             std::vector<OpenMesh::HalfedgeHandle*> heh_update = {},
                             std::vector<OpenMesh::FaceHandle*> fh_update= {});
  // Removing an edge
  void RemoveMetaEdgeFromBaseMesh(OpenMesh::HalfedgeHandle mheh, bool cleanup = true);
  void RemoveMetaEdgeFromMetaMesh(OpenMesh::HalfedgeHandle mheh);

  void MarkVertex(OpenMesh::VertexHandle bvh);

  friend class Testing;

  OpenMesh::VPropHandleT<OpenMesh::VertexHandle> bv_connection_;
  OpenMesh::VPropHandleT<OpenMesh::VertexHandle> mv_connection_;
  OpenMesh::VPropHandleT<OpenMesh::HalfedgeHandle> bsplithandle_;
  OpenMesh::HPropHandleT<OpenMesh::HalfedgeHandle> bhe_connection_;
  OpenMesh::HPropHandleT<OpenMesh::HalfedgeHandle> mhe_connection_;
  OpenMesh::HPropHandleT<OpenMesh::HalfedgeHandle> next_heh_;
  OpenMesh::HPropHandleT<double> halfedge_weight_;

private:
  enum SplitTraversal {LEFT, RIGHT};
  bool TriangulationPipeline(
      std::vector<OpenMesh::VertexHandle> meta_mesh_points);
  void InitializeProperties();
  void RemoveProperties();
  bool TriangulateMetaMesh();
  void PreProcessEdges();
  void ProcessHalfedge(OpenMesh::HalfedgeHandle mheh);
  void ProcessEdge(OpenMesh::EdgeHandle meh);
  void ProcessVertex(OpenMesh::VertexHandle bvh);
  void CleanupVertex(OpenMesh::VertexHandle bvh);
  void CleanupFace(OpenMesh::HalfedgeHandle mheh);
  void CleanupHalfedge(OpenMesh::HalfedgeHandle mheh);
  void ProcessNeighbors(OpenMesh::EdgeHandle meh);
  void ProcessFace(OpenMesh::HalfedgeHandle mheh);
  bool ConditionalSplit(OpenMesh::HalfedgeHandle bheh);
  std::vector<OpenMesh::HalfedgeHandle> LeftHalfCircle(OpenMesh::HalfedgeHandle bheh);
  std::vector<OpenMesh::HalfedgeHandle> GetBaseHalfedges(OpenMesh::HalfedgeHandle mheh);
  OpenMesh::VertexHandle ConditionalCollapse(OpenMesh::VertexHandle bvh);
  OpenMesh::HalfedgeHandle SplitBaseHe(OpenMesh::HalfedgeHandle bheh);
  void CollapseBaseHe(OpenMesh::HalfedgeHandle bheh);
  void LowLevelBaseCollapse(OpenMesh::HalfedgeHandle bheh);
  void AdjustPointersForBheCollapse(OpenMesh::HalfedgeHandle bheh);
  void MergeProperties(OpenMesh::HalfedgeHandle bheh0, OpenMesh::HalfedgeHandle bheh1);
  void CreateMetaMeshVertices(std::vector<OpenMesh::VertexHandle> meta_mesh_points);
  bool Delaunay(OpenMesh::VPropHandleT<double> voronoidistance,
                OpenMesh::VPropHandleT<OpenMesh::HalfedgeHandle> to_heh,
                OpenMesh::HPropHandleT<int> multiplicity_heh);
  void TraverseBoundary(OpenMesh::VertexHandle mvh);
  int fact(int n);
  int nCk(int n, int k);
  std::vector<int> VoronoiGenus();
  void SetFaceProperties(OpenMesh::FaceHandle bf,
                         OpenMesh::FaceHandle mf);
  void CopyFaceProperties(OpenMesh::FaceHandle mfh,
                          std::vector<OpenMesh::HalfedgeHandle> boundaryborders);
  OpenMesh::FaceHandle AddFace(std::vector<OpenMesh::VertexHandle> mvh,
                               std::vector<OpenMesh::HalfedgeHandle> mheh);
  void AddBoundaries();
  OpenMesh::HalfedgeHandle FindHalfedge(OpenMesh::HalfedgeHandle bheh);

  void VoronoiBorders();
  void SetBorderProperties(OpenMesh::HalfedgeHandle bheh,
                      OpenMesh::HalfedgeHandle mheh);

  void A_StarTriangulation();

  void NaiveVoronoi(std::queue<OpenMesh::VertexHandle> queue,
                    OpenMesh::VPropHandleT<double> voronoidistance,
                    OpenMesh::VPropHandleT<OpenMesh::HalfedgeHandle> to_heh);

  void ColorizeMetaMeshVertices();
  void ColorizeMetaEdges();
  void ColorizeVoronoiRegions();
  void ColorizeBorders();

  void TestHalfedgeConsistency();

  void BoundaryTrace(OpenMesh::HalfedgeHandle mheh, bool mark_trace = false);
  void SimpleTrace(OpenMesh::HalfedgeHandle mheh, bool cleanup = true,
                   bool mark_trace = false);
  void AdvancedTrace(OpenMesh::HalfedgeHandle mheh);
  std::vector<OpenMesh::HalfedgeHandle> A_StarBidirectional(OpenMesh::HalfedgeHandle bheh0,
                    OpenMesh::HalfedgeHandle bheh1, bool mark_trace = false,
                    OpenMesh::HalfedgeHandle mheh
                    = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle);
  double Distance(OpenMesh::VertexHandle bvhf, OpenMesh::VertexHandle bvhv);
  double A_StarHeuristic(OpenMesh::VertexHandle bvhf, OpenMesh::VertexHandle bvhr,
                           OpenMesh::VertexHandle bvhv);
  std::vector<OpenMesh::VertexHandle> A_StarNeighbors(OpenMesh::VertexHandle bvh,
        OpenMesh::HalfedgeHandle mheh,
        OpenMesh::VPropHandleT<bool> closed_set,
        OpenMesh::VertexHandle towardsbvh,
        OpenMesh::HalfedgeHandle bheh = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle);
  bool SectorEntry(OpenMesh::HalfedgeHandle mheh, OpenMesh::HalfedgeHandle bheh);
  bool ValidA_StarEdge(OpenMesh::HalfedgeHandle bheh,
                       OpenMesh::HalfedgeHandle mheh);
  std::vector<OpenMesh::HalfedgeHandle> A_StarReconstructPath(OpenMesh::VertexHandle middle,
             OpenMesh::VPropHandleT<OpenMesh::VertexHandle> predecessor0,
             OpenMesh::VPropHandleT<OpenMesh::VertexHandle> predecessor1);

  std::pair<OpenMesh::HalfedgeHandle, OpenMesh::HalfedgeHandle> FindA_StarStartingHalfedges(
      OpenMesh::HalfedgeHandle mheh, bool verbose = false);
  void InsertPath(OpenMesh::HalfedgeHandle mheh, std::vector<OpenMesh::HalfedgeHandle> path);

  // Adding an edge
  OpenMesh::HalfedgeHandle AddMetaEdge(OpenMesh::HalfedgeHandle mheh0,
                                       OpenMesh::HalfedgeHandle mheh);

  // Rotation
  void MetaRotation(OpenMesh::EdgeHandle meh);

  // Split
  // Edges
  void EdgeSplit(OpenMesh::EdgeHandle meh, OpenMesh::VertexHandle bvh
             = OpenMesh::PolyConnectivity::InvalidVertexHandle, bool verbose = false);
  void MetaEdgeSplit(OpenMesh::EdgeHandle meh, OpenMesh::VertexHandle bvh);
  void OverwriteHalfedgeConnection(OpenMesh::HalfedgeHandle mheh,
                                   OpenMesh::HalfedgeHandle bheh);
  // Faces
  void FaceSplit(OpenMesh::VertexHandle bvh, OpenMesh::HalfedgeHandle
                 mheh = OpenMesh::PolyConnectivity::InvalidHalfedgeHandle,
                 bool verbose = false);
  OpenMesh::HalfedgeHandle FindFaceHalfedge(OpenMesh::VertexHandle bvh);
  void MetaFaceSplit(OpenMesh::HalfedgeHandle mheh, OpenMesh::VertexHandle bvh);

  // Collapse
  void BendMetaEdgeForwards(OpenMesh::HalfedgeHandle mheh);
  void BendMetaEdgeBackwards(OpenMesh::HalfedgeHandle mheh);
  void RemoveLoop(OpenMesh::HalfedgeHandle mheh);
  void RemoveSelfLoop(OpenMesh::HalfedgeHandle mheh);
  int OneRingEuler(OpenMesh::VertexHandle mvh);

  // Relocate
  void LowLevelRelocate(OpenMesh::VertexHandle mvh, OpenMesh::VertexHandle bvh,
                        bool verbose = false);
  void TraverseGroup(OpenMesh::VertexHandle mvh,
                     OpenMesh::HalfedgeHandle moh,
                     OpenMesh::VPropHandleT<bool> groupselected);
  void CompositeRelocate(OpenMesh::VertexHandle mvh, OpenMesh::VertexHandle bvh,
                         bool verbose = false);
  void MetaSwap(OpenMesh::VertexHandle mvh0, OpenMesh::VertexHandle mvh1);

  bool debug_hard_stop_ = false;
  bool initial_triangulation_ = false;
  bool boundaries_ = false;

  TriMesh* base_mesh_;
  PolyMesh* meta_mesh_;
  Utils::Draw draw_;

  OpenMesh::VPropHandleT<OpenMesh::VertexHandle> voronoiID_;
  OpenMesh::HPropHandleT<OpenMesh::HalfedgeHandle> bhe_border_;
  OpenMesh::HPropHandleT<OpenMesh::HalfedgeHandle> mhe_border_;
};
