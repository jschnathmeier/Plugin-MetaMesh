#include "MetaMeshToolbox.hh"

#include <QGridLayout>
#include <QLabel>

MetaMeshToolbox::MetaMeshToolbox(QWidget* parent) : QWidget(parent)
{
  QGridLayout* layout = new QGridLayout(this);

  combo_box_tests_ = new QComboBox;
  combo_box_tests_->addItem("Display Mesh Info");
  combo_box_tests_->addItem("Mesh Properties");
  combo_box_tests_->addItem("Mesh Operations");
  combo_box_tests_->addItem("Rotations");
  combo_box_tests_->addItem("Splits");
  combo_box_tests_->addItem("Collapses");
  combo_box_tests_->addItem("Relocations");
  combo_box_tests_->setCurrentIndex(1);

  combo_box_remeshing_smtype_ = new QComboBox;
  combo_box_remeshing_smtype_->addItem("Forest Fire");
  combo_box_remeshing_smtype_->addItem("Vertex Weights");
  combo_box_remeshing_smtype_->addItem("Vertex Distances");
  combo_box_remeshing_smtype_->setCurrentIndex(1);

  combo_box_remeshing_strype_ = new QComboBox;
  combo_box_remeshing_strype_->addItem("None");
  combo_box_remeshing_strype_->addItem("Edgewise");
  combo_box_remeshing_strype_->addItem("Patchwise");
  combo_box_remeshing_strype_->setCurrentIndex(0);

  combo_box_remeshing_crorder_ = new QComboBox;
  combo_box_remeshing_crorder_->addItem("Random");
  combo_box_remeshing_crorder_->addItem("Valence");
  combo_box_remeshing_crorder_->addItem("Splitweight");
  combo_box_remeshing_crorder_->setCurrentIndex(2);

  button_triangulate_ = new QPushButton("Triangulate");
  button_randomize_ratio_ = new QPushButton("Randomize a ratio of points");
  button_randomize_total_ = new QPushButton("Randomize a number of points");
  button_run_test_ = new QPushButton("Run test");
  checkbox_copy_markings_ = new QCheckBox("Selection Copying");
  button_retrace_ = new QPushButton("Retrace");
  button_rotate_ = new QPushButton("Rotate");
  button_split_ = new QPushButton("Split");
  button_collapse_ = new QPushButton("Collapse");
  button_relocate_ = new QPushButton("Relocate");
  button_dummy1_ = new QPushButton("Dummy 1");
  button_dummy2_ = new QPushButton("Dummy 2");
  button_next_ = new QPushButton("nxt");
  button_opp_ = new QPushButton("opp");
  button_prev_ = new QPushButton("prv");
  button_remesh_ = new QPushButton("Remesh");
  checkbox_limit_flips_ = new QCheckBox("Limit Flips");
  checkbox_data_output_ = new QCheckBox("Output Data");

  checkbox_copy_markings_->setCheckState(Qt::Checked);
  checkbox_limit_flips_->setCheckState(Qt::Unchecked);
  checkbox_data_output_->setCheckState(Qt::Checked);

  input_ratio_ = new QDoubleSpinBox();
  input_ratio_->setRange(0,1);
  input_ratio_->setDecimals(4);
  input_ratio_->setSingleStep(0.0001);
  input_ratio_->setValue(0.003);
  input_ratio_->setToolTip("Ratio of Base Mesh Vertices to be "
                           "randomly assigned as Meta Mesh vertices");

  input_alpha_ = new QDoubleSpinBox();
  input_alpha_->setRange(1,10);
  input_alpha_->setDecimals(3);
  input_alpha_->setSingleStep(0.001);
  input_alpha_->setValue(2);
  input_alpha_->setToolTip("Alpha parameter; splitting slack var");

  input_beta_ = new QDoubleSpinBox();
  input_beta_->setRange(0.1,1);
  input_beta_->setDecimals(3);
  input_beta_->setSingleStep(0.001);
  input_beta_->setValue(0.5);
  input_beta_->setToolTip("Beta parameter; collapsing slack var");

  input_length_ = new QDoubleSpinBox();
  input_length_->setRange(0,2);
  input_length_->setDecimals(3);
  input_length_->setSingleStep(0.001);
  input_length_->setValue(0.2);
  input_length_->setToolTip("Edge length in % of object diagonal");

  input_iterations_ = new QSpinBox();
  input_iterations_->setRange(1,1000);
  input_iterations_->setValue(10);
  input_iterations_->setSingleStep(1);
  input_iterations_->setToolTip("Number of times the algorithm runs");

  QLabel* label_alpha = new QLabel("Alpha:");
  QLabel* label_beta = new QLabel("Beta:");
  QLabel* label_length = new QLabel("Length:");
  QLabel* label_iterations = new QLabel("Iterations:");
  QLabel* label_smtype = new QLabel("Smoothing:");
  QLabel* label_strtype = new QLabel("Straightening:");
  QLabel* label_corder = new QLabel("Collapse Order:");

  layout->addWidget(button_triangulate_, 0, 0, 1, 6);
  layout->addWidget(button_randomize_ratio_, 1, 0, 1, 6);
  layout->addWidget(button_randomize_total_, 2, 0, 1, 6);
  layout->addWidget(input_ratio_, 3, 0, 1, 6);
  layout->addWidget(button_run_test_, 4, 0, 1, 3);
  layout->addWidget(combo_box_tests_, 4, 3, 1, 3);
  layout->addWidget(checkbox_copy_markings_, 5, 0, 1, 3);
  layout->addWidget(button_retrace_, 5, 3, 1, 3);
  layout->addWidget(button_rotate_, 6, 0, 1, 3);
  layout->addWidget(button_split_, 6, 3, 1, 3);
  layout->addWidget(button_collapse_, 7, 0, 1, 3);
  layout->addWidget(button_relocate_, 7, 3, 1, 3);
  layout->addWidget(button_dummy1_, 8, 0, 1, 3);
  layout->addWidget(button_dummy2_, 8, 3, 1, 3);
  layout->addWidget(button_next_, 9, 0, 1, 2);
  layout->addWidget(button_opp_, 9, 2, 1, 2);
  layout->addWidget(button_prev_, 9, 4, 1, 2);
  layout->addWidget(button_remesh_, 11, 0, 1, 3);
  layout->addWidget(label_smtype, 10, 3, 1, 3);
  layout->addWidget(combo_box_remeshing_smtype_, 11, 3, 1, 3);
  layout->addWidget(label_strtype, 12, 0, 1, 3);
  layout->addWidget(combo_box_remeshing_strype_, 13, 0, 1, 3);
  layout->addWidget(label_corder, 12, 3, 1, 3);
  layout->addWidget(combo_box_remeshing_crorder_, 13, 3, 1, 3);
  layout->addWidget(checkbox_limit_flips_, 15, 0, 1, 2);
  layout->addWidget(label_alpha, 14, 2, 1, 2);
  layout->addWidget(input_alpha_, 15, 2, 1, 2);
  layout->addWidget(label_beta, 14, 4, 1, 2);
  layout->addWidget(input_beta_, 15, 4, 1, 2);
  layout->addWidget(checkbox_data_output_, 17, 0, 1, 2);
  layout->addWidget(label_length, 16, 2, 1, 2);
  layout->addWidget(input_length_, 17, 2, 1, 2);
  layout->addWidget(label_iterations, 16, 4, 1, 2);
  layout->addWidget(input_iterations_, 17, 4, 1, 2);
  this->setLayout(layout);
}
