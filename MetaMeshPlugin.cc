#include "MetaMeshPlugin.hh"

#include <ObjectTypes/PolyMesh/PolyMesh.hh>
#include <ObjectTypes/TriangleMesh/TriangleMesh.hh>

#include <OpenFlipper/BasePlugin/PluginFunctions.hh>
#include <iostream>
#include <QDebug>


/*!
 * \brief MetaMeshPlugin::initializePlugin
 */
void MetaMeshPlugin::initializePlugin()
{
	widget_ = new MetaMeshToolbox();
	
  connect(widget_->button_triangulate_, SIGNAL(clicked()), this, SLOT(triangulate()));
  connect(widget_->button_randomize_ratio_, SIGNAL(clicked()), this,
          SLOT(randomizeratio()));
  connect(widget_->button_randomize_total_, SIGNAL(clicked()), this,
          SLOT(randomizetotal()));
  connect(widget_->button_run_test_, SIGNAL(clicked()), this, SLOT(test_meta_mesh()));
  connect(widget_->button_retrace_, SIGNAL(clicked()), this, SLOT(retrace()));
  connect(widget_->button_rotate_, SIGNAL(clicked()), this, SLOT(rotate()));
  connect(widget_->button_split_, SIGNAL(clicked()), this, SLOT(split()));
  connect(widget_->button_collapse_, SIGNAL(clicked()), this, SLOT(collapse()));
  connect(widget_->button_relocate_, SIGNAL(clicked()), this, SLOT(relocate()));
  connect(widget_->button_remesh_, SIGNAL(clicked()), this, SLOT(remesh()));
  connect(widget_->button_dummy1_, SIGNAL(clicked()), this, SLOT(DummySlotFunction1()));
  connect(widget_->button_dummy2_, SIGNAL(clicked()), this, SLOT(DummySlotFunction2()));
  connect(widget_->button_next_, SIGNAL(clicked()), this, SLOT(nxt()));
  connect(widget_->button_opp_, SIGNAL(clicked()), this, SLOT(opp()));
  connect(widget_->button_prev_, SIGNAL(clicked()), this, SLOT(prv()));
	
	emit addToolbox("MetaMesh", widget_);

  printf("Trying to create MetaMesh object\n");
  embedding_ = new Embedding();
  testing_ = new Testing(embedding_);
  printf("Created MetaMesh object\n");
}

/*!
 * \brief MetaMeshPlugin::pluginsInitialized
 */
void MetaMeshPlugin::pluginsInitialized()
{
  printf("Plugins Initialized\n");
}

/*!
 * \brief MetaMeshPlugin::slotObjectUpdated
 * \param _identifier
 */
void MetaMeshPlugin::slotObjectUpdated(int _identifier)
{
  if (_identifier == meta_mesh_id_ && !selection_mutex_
      && widget_->checkbox_copy_markings_->checkState() == Qt::Checked) {
    selection_mutex_ = true;
    copyselection();
    selection_mutex_ = false;
  }
}

/*!
 * \brief MetaMeshPlugin::triangulate triangulates a meta mesh from marked vertices on the currently
 * loaded mesh
 */
void MetaMeshPlugin::triangulate()
{
  qDebug() << "Triangulate Button pressed";
  using namespace PluginFunctions;
  for (auto* o : objects(TARGET_OBJECTS, DATA_TRIANGLE_MESH)) {
    emit addEmptyObject(DATA_POLY_MESH, meta_mesh_id_);
    base_mesh_id_ = o->id();
    assert(meta_mesh_id_ >= 0);
    meta_mesh_ = polyMesh(meta_mesh_id_);
    meta_mesh_->clear();
    TriMesh& base_mesh = *triMesh(o);

    qDebug() << "Call triangulation on MetaMesh  in object loop";
    embedding_->SelectionTriangulation(base_mesh, *meta_mesh_);
    meta_mesh_->update_normals();
    embedding_->GetBaseMesh()->update_normals();

    //meta_mesh_->clear();
    for (auto bvh : embedding_->GetBaseMesh()->vertices()) {
      embedding_->GetBaseMesh()->status(bvh).set_selected(false);
    }
    emit updatedObject(o->id(), UPDATE_ALL);
    qDebug() << "Exited InitialTriangulation function";
  }
  if (embedding_->ErrStatus()) {
    widget_->checkbox_copy_markings_->setCheckState(Qt::Unchecked);
  }
  calculateoffset();
  addoffset();
  embedding_->ColorizeMetaMesh();
  emit updatedObject(base_mesh_id_, UPDATE_ALL);
  emit updatedObject(meta_mesh_id_, UPDATE_ALL);
}

/*!
 * \brief MetaMeshPlugin::randomizetotal randomize an absolute number of vertices
 */
void MetaMeshPlugin::randomizetotal() {
  randomize(Embedding::RandomType::TOTAL);
}

/*!
 * \brief MetaMeshPlugin::randomizeratio randomize a ratio of vertices
 */
void MetaMeshPlugin::randomizeratio() {
  randomize(Embedding::RandomType::RATIO);
}

/*!
 * \brief MetaMeshPlugin::randomize randomize meta mesh vertices and triangulate
 * \param type
 */
void MetaMeshPlugin::randomize(Embedding::RandomType type) {
  using namespace PluginFunctions;
  for (auto* o : objects(TARGET_OBJECTS, DATA_TRIANGLE_MESH)) {
    emit addEmptyObject(DATA_POLY_MESH, meta_mesh_id_);
    base_mesh_id_ = o->id();
    assert(meta_mesh_id_ >= 0);
    meta_mesh_ = polyMesh(meta_mesh_id_);
    meta_mesh_->clear();
    TriMesh& base_mesh = *triMesh(o);

    qDebug() << "Call triangulation on MetaMesh  in object loop";
    embedding_->RandomTriangulation(base_mesh, *meta_mesh_,
         widget_->input_ratio_->value(), type);
    meta_mesh_->update_normals();
    embedding_->GetBaseMesh()->update_normals();
    emit updatedObject(o->id(), UPDATE_ALL);
    qDebug() << "Exited InitialTriangulation function";
  }
  if (embedding_->ErrStatus()) {
    widget_->checkbox_copy_markings_->setCheckState(Qt::Unchecked);
  }
  calculateoffset();
  addoffset();
  embedding_->ColorizeMetaMesh();
  emit updatedObject(meta_mesh_id_, UPDATE_ALL);
}

/*!
 * \brief MetaMeshPlugin::test_meta_mesh run selected test
 */
void MetaMeshPlugin::test_meta_mesh() {
  using namespace PluginFunctions;
  if (embedding_->GetMetaMesh() == nullptr) {
    qDebug() << "Run some triangulation before calling tests on the meta mesh.";
    return;
  }

  preoperation();

  switch (widget_->combo_box_tests_->currentIndex()) {
  case 0: {
    testing_->DisplayMeshInformation();
    break;
  }
  case 1: {
    testing_->MeshTests();
    break;
  }
  case 2: {
    testing_->OperationTests();
    break;
  }
  case 3: {
    testing_->TestFlips();
    break;
  }
  case 4: {
    testing_->TestSplits();
    break;
  }
  case 5: {
    testing_->TestCollapses();
    break;
  }
  case 6: {
    testing_->TestRelocation();
  }
  }
  if (embedding_->ErrStatus()) {
    widget_->checkbox_copy_markings_->setCheckState(Qt::Unchecked);
    embedding_->GetBaseMesh()->update_normals();
    emit updatedObject(base_mesh_id_, UPDATE_ALL);
    addoffset();
    embedding_->ColorizeMetaMesh();
    return;
  }

  postoperation(UPDATE_ALL, UPDATE_ALL);
}

/*!
 * \brief MetaMeshPlugin::retrace retrace the selected meta edges / halfedges. None selected means
 * everything will be retraced.
 */
void MetaMeshPlugin::retrace() {
  using namespace PluginFunctions;
  preoperation();
  auto retracequeue = GetHalfedgeSelection();
  bool emptyqueue = retracequeue.empty();
  while (!retracequeue.empty()) {
    embedding_->Retrace(retracequeue.front(), true);
    retracequeue.pop();
  }

  // If nothing is selected retrace everything
  if (emptyqueue) {
    for (auto meh : embedding_->GetMetaMesh()->edges()) {
      auto mheh = embedding_->GetMetaMesh()->halfedge_handle(meh, 0);
      embedding_->Retrace(mheh);
    }
  }

  postoperation(UPDATE_ALL, UPDATE_TOPOLOGY);
}

/*!
 * \brief MetaMeshPlugin::rotate rotate all selected meta edges and halfedges
 */
void MetaMeshPlugin::rotate() {
  using namespace PluginFunctions;
  preoperation();
  auto rotationqueue = GetHalfedgeSelection();

  while (!rotationqueue.empty()) {
    embedding_->Rotate(embedding_->GetMetaMesh()->edge_handle(rotationqueue.front()));
    rotationqueue.pop();
  }

  postoperation(UPDATE_ALL, UPDATE_TOPOLOGY);
}

/*!
 * \brief MetaMeshPlugin::split split faces and edges at selected base vertices
 */
void MetaMeshPlugin::split() {
  using namespace PluginFunctions;
  preoperation();

  for (auto bvh : embedding_->GetBaseMesh()->vertices()) {
    if (embedding_->GetBaseMesh()->status(bvh).selected()) {
      embedding_->Split(bvh);
      embedding_->GetBaseMesh()->status(bvh).set_selected(false);
    }
  }

  postoperation(UPDATE_TOPOLOGY, UPDATE_TOPOLOGY);
}

/*!
 * \brief MetaMeshPlugin::collapse collapse all selected meta edges and halfedges
 */
void MetaMeshPlugin::collapse() {
  using namespace PluginFunctions;
  preoperation();

  auto collapsequeue = GetHalfedgeSelection();
  while (!collapsequeue.empty()) {
    embedding_->Collapse(collapsequeue.front(), true);
    collapsequeue.pop();
  }

  postoperation(UPDATE_TOPOLOGY, UPDATE_TOPOLOGY);
}

/*!
 * \brief MetaMeshPlugin::relocate relocate selected meta vertex to selected non-meta vertex
 */
void MetaMeshPlugin::relocate() {
  using namespace PluginFunctions;
  preoperation();

  OpenMesh::VertexHandle mvh = OpenMesh::PolyConnectivity::InvalidVertexHandle;
  OpenMesh::VertexHandle bvh = OpenMesh::PolyConnectivity::InvalidVertexHandle;
  for (auto bvh_iter : embedding_->GetBaseMesh()->vertices()) {
    if (embedding_->GetBaseMesh()->status(bvh_iter).selected()) {
      if (embedding_->GetBaseMesh()->property(embedding_->bv_connection_, bvh_iter).is_valid()) {
        mvh = embedding_->GetBaseMesh()->property(embedding_->bv_connection_, bvh_iter);
      } else {
        bvh = bvh_iter;
      }
      embedding_->GetBaseMesh()->status(bvh_iter).set_selected(false);
    }
  }
  for (auto mvh_iter : embedding_->GetMetaMesh()->vertices()) {
    if (embedding_->GetMetaMesh()->status(mvh_iter).selected()) {
      mvh = mvh_iter;
      embedding_->GetMetaMesh()->status(mvh_iter).set_selected(false);
    }
  }
  if (bvh.is_valid() && mvh.is_valid()) {
    embedding_->Relocate(mvh, bvh, true);
  }

  postoperation(UPDATE_TOPOLOGY, UPDATE_TOPOLOGY);
}

/*!
 * \brief MetaMeshPlugin::remesh calls IsotropicRemesher::Remesh with menu selected parameters
 */
void MetaMeshPlugin::remesh() {
  using namespace PluginFunctions;
  preoperation();

  IsotropicRemesher remesher;
  auto take_screenshot = [&](const QString& name, const QString& dir,
      const int& width = 0, const int& height = 0) {
    screenshot(name, dir, width, height);
  };

  remesher.Remesh(embedding_,  widget_->input_length_->value(),
                  static_cast<uint>(widget_->input_iterations_->value()),
                  widget_->input_alpha_->value(),
                  widget_->input_beta_->value(),
                  IsotropicRemesher::SmoothingType(
                    widget_->combo_box_remeshing_smtype_->currentIndex()),
                  take_screenshot,
                  widget_->checkbox_limit_flips_->isChecked(),
                  IsotropicRemesher::StraighteningType(
                  widget_->combo_box_remeshing_strype_->currentIndex()),
                  IsotropicRemesher::CollapsingOrder(
                  widget_->combo_box_remeshing_crorder_->currentIndex()),
                  widget_->checkbox_data_output_->isChecked());

  postoperation(UPDATE_TOPOLOGY, UPDATE_TOPOLOGY);
}

/*!
 * \brief MetaMeshPlugin::calculateoffset calculate an offset to separate base and meta mesh.
 */
void MetaMeshPlugin::calculateoffset() {
  const double inf = std::numeric_limits<double>::infinity();
  ACG::Vec3d minCorner = {+inf, +inf, +inf};
  ACG::Vec3d maxCorner = {-inf, -inf, -inf};
  for (auto bvh : embedding_->GetBaseMesh()->vertices()) {
    auto p = embedding_->GetBaseMesh()->point(bvh);
    maxCorner.maximize(p);
    minCorner.minimize(p);
  }
  meta_mesh_visualization_offset_[0] = (maxCorner[0]-minCorner[0])*1.3;
}

/*!
 * \brief MetaMeshPlugin::addoffset move the meta mesh away from the base mesh
 */
void MetaMeshPlugin::addoffset() {
  for (auto mvh : embedding_->GetMetaMesh()->vertices()) {
    embedding_->GetMetaMesh()->point(mvh) += meta_mesh_visualization_offset_/2.0;
  }
  for (auto bvh : embedding_->GetBaseMesh()->vertices()) {
    embedding_->GetBaseMesh()->point(bvh) -= meta_mesh_visualization_offset_/2.0;
  }
}

/*!
 * \brief MetaMeshPlugin::removeoffset move the meta mesh back to the base mesh
 */
void MetaMeshPlugin::removeoffset() {
  for (auto mvh : embedding_->GetMetaMesh()->vertices()) {
    embedding_->GetMetaMesh()->point(mvh) -= meta_mesh_visualization_offset_/2.0;
  }
  for (auto bvh : embedding_->GetBaseMesh()->vertices()) {
    embedding_->GetBaseMesh()->point(bvh) += meta_mesh_visualization_offset_/2.0;
  }
}

/*!
 * \brief MetaMeshPlugin::copyselection copy selected elements from the meta mesh
 * into the base mesh (select the corresponding elements)
 */
void MetaMeshPlugin::copyselection() {
  if (!metameshsanitychecks(false))
    return;
  using namespace PluginFunctions;
  removeoffset();
  for (auto bheh : embedding_->GetBaseMesh()->halfedges()) {
    embedding_->GetBaseMesh()->status(bheh).set_selected(false);
  }
  for (auto mheh : embedding_->GetMetaMesh()->halfedges()) {
    auto bheh = embedding_->GetMetaMesh()->property(embedding_->mhe_connection_, mheh);
    embedding_->GetBaseMesh()->status(bheh).set_selected(
          embedding_->GetMetaMesh()->status(mheh).selected());
    while (embedding_->GetBaseMesh()->status(bheh).selected() &&
           embedding_->GetBaseMesh()->property(embedding_->next_heh_, bheh)
           != OpenMesh::PolyConnectivity::InvalidHalfedgeHandle) {
      bheh = embedding_->GetBaseMesh()->property(embedding_->next_heh_, bheh);
      embedding_->GetBaseMesh()->status(bheh).set_selected(true);
    }
  }
  for (auto beh : embedding_->GetBaseMesh()->edges()) {
    embedding_->GetBaseMesh()->status(beh).set_selected(false);
  }
  for (auto meh : embedding_->GetMetaMesh()->edges()) {
    auto bheh = embedding_->GetMetaMesh()->property(embedding_->mhe_connection_,
                embedding_->GetMetaMesh()->halfedge_handle(meh, 0));
    auto beh = embedding_->GetBaseMesh()->edge_handle(bheh);
    embedding_->GetBaseMesh()->status(beh).set_selected(
          embedding_->GetMetaMesh()->status(meh).selected());
    while (embedding_->GetBaseMesh()->status(beh).selected() &&
           embedding_->GetBaseMesh()->property(embedding_->next_heh_, bheh)
           != OpenMesh::PolyConnectivity::InvalidHalfedgeHandle) {
      bheh = embedding_->GetBaseMesh()->property(embedding_->next_heh_, bheh);
      auto beh = embedding_->GetBaseMesh()->edge_handle(bheh);
      embedding_->GetBaseMesh()->status(beh).set_selected(true);
    }
  }

  embedding_->GetBaseMesh()->garbage_collection();

  addoffset();
  emit updatedObject(base_mesh_id_, UPDATE_SELECTION);
  UpdateMetaMesh(UPDATE_SELECTION);
}

/*!
 * \brief MetaMeshPlugin::DummySlotFunction1 function for the first dummy button,
 * used to test various things on the mesh
 */
void MetaMeshPlugin::DummySlotFunction1() {
  using namespace PluginFunctions;
  for (auto bheh : embedding_->GetBaseMesh()->halfedges()) {
    embedding_->GetBaseMesh()->status(bheh).set_selected(false);
  }
  for (auto mheh : meta_mesh_->halfedges()) {
    meta_mesh_->status(mheh).set_selected(false);
  }
  emit updatedObject(base_mesh_id_, UPDATE_SELECTION);
  emit updatedObject(meta_mesh_id_, UPDATE_SELECTION);
}

/*!
 * \brief MetaMeshPlugin::DummySlotFunction2 function for the first dummy button,
 * used to test various things on the mesh
 */
void MetaMeshPlugin::DummySlotFunction2() {
  using namespace PluginFunctions;
  for (auto mheh : meta_mesh_->halfedges()) {
    if (meta_mesh_->is_boundary(mheh)) {
      meta_mesh_->status(mheh).set_selected(true);
    }
  }
  emit updatedObject(meta_mesh_id_, UPDATE_SELECTION);
  emit updatedObject(base_mesh_id_, UPDATE_SELECTION);
}

/*!
 * \brief MetaMeshPlugin::nxt move all selections on meta halfedges forward to their
 * next_halfedge_handle
 */
void MetaMeshPlugin::nxt() {
  if (!metameshsanitychecks())
    return;
  std::vector<OpenMesh::EdgeHandle> unmark_eh;
  std::vector<OpenMesh::HalfedgeHandle> unmark_heh;
  std::vector<OpenMesh::EdgeHandle> mark_eh;
  std::vector<OpenMesh::HalfedgeHandle> mark_heh;
  for (auto meh : embedding_->GetMetaMesh()->edges()) {
    if (embedding_->GetMetaMesh()->status(meh).selected()) {
      auto mheh = embedding_->GetMetaMesh()->halfedge_handle(meh, 0);
      unmark_eh.push_back(meh);
      mark_eh.push_back(embedding_->GetMetaMesh()->edge_handle(
                          embedding_->GetMetaMesh()->next_halfedge_handle(mheh)));
    }
  }
  for (auto mheh : embedding_->GetMetaMesh()->halfedges()) {
    if (embedding_->GetMetaMesh()->status(mheh).selected()) {
      unmark_heh.push_back(mheh);
      mark_heh.push_back(embedding_->GetMetaMesh()->next_halfedge_handle(mheh));
    }
  }
  for (auto unmark : unmark_eh) {
    embedding_->GetMetaMesh()->status(unmark).set_selected(false);
  }
  for (auto unmark : unmark_heh) {
    embedding_->GetMetaMesh()->status(unmark).set_selected(false);
  }
  for (auto mark : mark_eh) {
    embedding_->GetMetaMesh()->status(mark).set_selected(true);
  }
  for (auto mark : mark_heh) {
    embedding_->GetMetaMesh()->status(mark).set_selected(true);
  }
  UpdateMetaMesh(UPDATE_SELECTION_EDGES);
  UpdateMetaMesh(UPDATE_SELECTION_HALFEDGES);
}

/*!
 * \brief MetaMeshPlugin::opp move all selections of meta halfedges to
 * their opposite_halfedge_handle
 */
void MetaMeshPlugin::opp() {
  if (!metameshsanitychecks())
    return;
  std::vector<OpenMesh::EdgeHandle> unmark_eh;
  std::vector<OpenMesh::HalfedgeHandle> unmark_heh;
  std::vector<OpenMesh::EdgeHandle> mark_eh;
  std::vector<OpenMesh::HalfedgeHandle> mark_heh;
  for (auto meh : embedding_->GetMetaMesh()->edges()) {
    if (embedding_->GetMetaMesh()->status(meh).selected()) {
      auto mheh = embedding_->GetMetaMesh()->halfedge_handle(meh, 0);
      unmark_eh.push_back(meh);
      mark_eh.push_back(embedding_->GetMetaMesh()->edge_handle(
                          embedding_->GetMetaMesh()->opposite_halfedge_handle(mheh)));
    }
  }
  for (auto mheh : embedding_->GetMetaMesh()->halfedges()) {
    if (embedding_->GetMetaMesh()->status(mheh).selected()) {
      unmark_heh.push_back(mheh);
      mark_heh.push_back(embedding_->GetMetaMesh()->opposite_halfedge_handle(mheh));
    }
  }
  for (auto unmark : unmark_eh) {
    embedding_->GetMetaMesh()->status(unmark).set_selected(false);
  }
  for (auto unmark : unmark_heh) {
    embedding_->GetMetaMesh()->status(unmark).set_selected(false);
  }
  for (auto mark : mark_eh) {
    embedding_->GetMetaMesh()->status(mark).set_selected(true);
  }
  for (auto mark : mark_heh) {
    embedding_->GetMetaMesh()->status(mark).set_selected(true);
  }
  UpdateMetaMesh(UPDATE_SELECTION_EDGES);
  UpdateMetaMesh(UPDATE_SELECTION_HALFEDGES);
}

/*!
 * \brief MetaMeshPlugin::prv move all halfedge selections on the meta mesh
 * to their prev_halfedge_handle
 */
void MetaMeshPlugin::prv() {
  if (!metameshsanitychecks())
    return;
  std::vector<OpenMesh::EdgeHandle> unmark_eh;
  std::vector<OpenMesh::HalfedgeHandle> unmark_heh;
  std::vector<OpenMesh::EdgeHandle> mark_eh;
  std::vector<OpenMesh::HalfedgeHandle> mark_heh;
  for (auto meh : embedding_->GetMetaMesh()->edges()) {
    if (embedding_->GetMetaMesh()->status(meh).selected()) {
      auto mheh = embedding_->GetMetaMesh()->halfedge_handle(meh, 0);
      unmark_eh.push_back(meh);
      mark_eh.push_back(embedding_->GetMetaMesh()->edge_handle(
                          embedding_->GetMetaMesh()->prev_halfedge_handle(mheh)));
    }
  }
  for (auto mheh : embedding_->GetMetaMesh()->halfedges()) {
    if (embedding_->GetMetaMesh()->status(mheh).selected()) {
      unmark_heh.push_back(mheh);
      mark_heh.push_back(embedding_->GetMetaMesh()->prev_halfedge_handle(mheh));
    }
  }
  for (auto unmark : unmark_eh) {
    embedding_->GetMetaMesh()->status(unmark).set_selected(false);
  }
  for (auto unmark : unmark_heh) {
    embedding_->GetMetaMesh()->status(unmark).set_selected(false);
  }
  for (auto mark : mark_eh) {
    embedding_->GetMetaMesh()->status(mark).set_selected(true);
  }
  for (auto mark : mark_heh) {
    embedding_->GetMetaMesh()->status(mark).set_selected(true);
  }
  UpdateMetaMesh(UPDATE_SELECTION_EDGES);
  UpdateMetaMesh(UPDATE_SELECTION_HALFEDGES);
}

/*!
 * \brief MetaMeshPlugin::metameshsanitychecks checks a few conditions that make
 * visualizing the meta_mesh_ unsafe (crashes the GPUoptimizer or other problems)
 * \param verbose
 * \return false if the meta mesh should not be virualized
 */
bool MetaMeshPlugin::metameshsanitychecks(bool verbose) {
  if (meta_mesh_ == nullptr) {
    if (verbose)
      qDebug() << "The meta mesh is a null pointer.";
    return false;
  }
  if (meta_mesh_->n_vertices() < 3) {
    if (verbose)
      qDebug() << "meta_mesh_ n_vertices is " << meta_mesh_->n_vertices()
               << "but it needs to be at least 3 to be visualized.";
    return false;
  }
  if (meta_mesh_->n_edges() == 0) {
    if (verbose)
      qDebug() << "The meta mesh has no edges.";
    return false;
  }
  return true;
}

/*!
 * \brief MetaMeshPlugin::MetaMeshUpdateNormals
 */
void MetaMeshPlugin::MetaMeshUpdateNormals() {
  if (metameshsanitychecks(false)) {
    meta_mesh_->update_normals();
  }
}

/*!
 * \brief MetaMeshPlugin::EmbeddingGarbageCollection
 */
void MetaMeshPlugin::EmbeddingGarbageCollection() {
  if (metameshsanitychecks(false)) {
    embedding_->MetaGarbageCollection();
  }
}

/*!
 * \brief MetaMeshPlugin::UpdateMetaMesh
 * \param _type
 */
void MetaMeshPlugin::UpdateMetaMesh(const UpdateType &_type) {
  if (metameshsanitychecks(false)) {
    emit updatedObject(meta_mesh_id_, _type);
  }
}

/*!
 * \brief MetaMeshPlugin::GetHalfedgeSelection
 * \return a list of selected meta halfedges (when they or their corresponding base
 * halfedges are selected)
 */
std::queue<OpenMesh::HalfedgeHandle> MetaMeshPlugin::GetHalfedgeSelection() {
  std::queue<OpenMesh::HalfedgeHandle> output;
  if (widget_->checkbox_copy_markings_->checkState() == Qt::Checked) {
    for (auto mheh : embedding_->GetMetaMesh()->halfedges()) {
      if (embedding_->GetMetaMesh()->status(mheh).selected()) {
          embedding_->GetMetaMesh()->status(mheh).set_selected(false);
          output.push(mheh);
      }
    }
    for (auto meh : embedding_->GetMetaMesh()->edges()) {
      if (embedding_->GetMetaMesh()->status(meh).selected()) {
        auto mheh = embedding_->GetMetaMesh()->halfedge_handle(meh, 0);
        embedding_->GetMetaMesh()->status(mheh).set_selected(false);
        output.push(mheh);
      }
    }
  }
  if (widget_->checkbox_copy_markings_->checkState() == Qt::Unchecked) {
    for (auto bheh : embedding_->GetBaseMesh()->halfedges()) {
      if (embedding_->ErrStatus()) {
        break;
      }
      if (embedding_->GetBaseMesh()->status(bheh).selected()) {
        embedding_->GetBaseMesh()->status(bheh).set_selected(false);
        auto mheh = embedding_->GetBaseMesh()->property(embedding_->bhe_connection_, bheh);
        if (mheh.is_valid()) {
          output.push(mheh);
        }
      }
    }
    for (auto beh : embedding_->GetBaseMesh()->edges()) {
      if (embedding_->ErrStatus()) {
        break;
      }
      if (embedding_->GetBaseMesh()->status(beh).selected()) {
        auto bheh = embedding_->GetBaseMesh()->halfedge_handle(beh, 0);
        embedding_->GetBaseMesh()->status(beh).set_selected(false);
        auto mheh = embedding_->GetBaseMesh()->property(embedding_->bhe_connection_, bheh);
        if (mheh.is_valid()) {
          output.push(mheh);
        }
      }
    }
  }
  return output;
}

/*!
 * \brief MetaMeshPlugin::screenshot updates view and takes(saves) a screenshot
 * this function will be handed over to IsotropicRemesher
 * \param name filename
 * \param dir directory
 * \param width dimension X
 * \param height dimension Y
 */
void MetaMeshPlugin::screenshot(const QString& name, const QString& dir,
                                const int& width, const int& height) {
  //qDebug() << "screenshotfunction";
  using namespace PluginFunctions;
  // Take viewer snapshot

  QCoreApplication::processEvents();
  EmbeddingGarbageCollection();
  MetaMeshUpdateNormals();
  embedding_->BaseGarbageCollection();
  addoffset();
  QCoreApplication::processEvents();
  embedding_->ColorizeMetaMesh();
  UpdateMetaMesh(UPDATE_ALL);
  embedding_->GetBaseMesh()->update_normals();
  emit updatedObject(base_mesh_id_, UPDATE_ALL);
  QCoreApplication::processEvents();

  QImage image;
  PluginFunctions::viewerSnapshot(PluginFunctions::ACTIVE_VIEWER, image, width, height);
  image.save(dir + QDir::separator() + name + QString(".png"), "PNG");

  removeoffset();
}

/*!
 * \brief MetaMeshPlugin::preoperation functions to be called before operations on the
 * mesh are performed. Currently only removes the meta mesh offset, but this wrapper
 * is here for consistency with postoperation()
 */
void MetaMeshPlugin::preoperation() {
  removeoffset();
}

/*!
 * \brief MetaMeshPlugin::postoperation functions to be called after operations on the
 * mesh are performed such as garbage collection, updating normals, adding an offset
 * to the meta mesh, colorizing it etc.
 * \param typem meta mesh update type eg. UPDATE_SELECTION
 * \param typeb base mesh update type eg. UPDATE_TOPOLOGY
 * \param verbose
 */
void MetaMeshPlugin::postoperation(const UpdateType &typem, const UpdateType &typeb,
                                   bool verbose) {
  if (embedding_->ErrStatus()
      || !metameshsanitychecks(verbose)) {
    widget_->checkbox_copy_markings_->setCheckState(Qt::Unchecked);
    embedding_->GetBaseMesh()->update_normals();
    emit updatedObject(base_mesh_id_, typeb);
    addoffset();
    embedding_->ColorizeMetaMesh();
    embedding_->CleanMetaMesh();
    return;
  }
  embedding_->GetBaseMesh()->update_normals();

  EmbeddingGarbageCollection();
  MetaMeshUpdateNormals();

  addoffset();
  embedding_->ColorizeMetaMesh();

  UpdateMetaMesh(typem);
  emit updatedObject(base_mesh_id_, typeb);
}
