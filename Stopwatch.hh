#pragma once

#include <chrono>

// A simple stopwatch with ms precision
class Stopwatch {
public:
  Stopwatch();
  ~Stopwatch();
  double Delta();
  void Reset();

private:
  std::chrono::time_point<std::chrono::system_clock> start_;
};
