#pragma once

#include <OpenMesh/Core/Mesh/PolyConnectivity.hh>
#include <ObjectTypes/PolyMesh/PolyMesh.hh>
#include <ObjectTypes/TriangleMesh/TriangleMesh.hh>
#include <OpenFlipper/common/Types.hh>
#include "Embedding.hh"
#include "Stopwatch.hh"

#include <functional>
#include <map>

/*!
 * \brief The IsotropicRemesher class
 */
class IsotropicRemesher {
public:
  IsotropicRemesher() {}
  ~IsotropicRemesher() {}

  enum SmoothingType{FORESTFIRE, VERTEXWEIGHTS, VERTEXDISTANCES};
  enum StraighteningType{NONE, EDGEWISE, PATCHWISE};
  enum CollapsingOrder{RANDOM, VALENCE, SPLITWEIGHT};

  void Remesh(Embedding* embedding,
              double target_length,
              uint iterations = 1,
              double alpha = 2,
              double beta = 1.0/2.0,
              SmoothingType smtype = VERTEXWEIGHTS,
              std::function<void (QString, QString, double, double)> screenshot = {},
              bool limitflips = false,
              StraighteningType strtype = NONE,
              CollapsingOrder corder = SPLITWEIGHT,
              bool dataoutput = true);
  bool DebugStopStatus() {return debug_hard_stop_;}

private:
  void Splits(Embedding* embedding, double high);
  void Collapses(Embedding* embedding, double low, CollapsingOrder corder = SPLITWEIGHT);
  void Flips(Embedding* embedding, bool limitflips = false);
  int FlipEval(Embedding* embedding, OpenMesh::EdgeHandle meh);
  void Smoothing(Embedding* embedding, SmoothingType stype = FORESTFIRE);
  void SmoothingVWD(Embedding* embedding, SmoothingType stype);
  void SmoothVertexVWD(Embedding* embedding,
                    const OpenMesh::VPropHandleT<std::vector<double>>* distances,
                    OpenMesh::VertexHandle mvh,
                    SmoothingType stype);
  double DistanceScoreVW(Embedding* embedding,
                         const OpenMesh::VPropHandleT<std::vector<double>>* distances,
                         OpenMesh::VertexHandle bvh, uint numv);
  double DistanceScoreVD(Embedding* embedding,
                         const OpenMesh::VPropHandleT<std::vector<double>>* distances,
                         OpenMesh::VertexHandle bvh, uint numv);
  void SmoothingFF(Embedding* embedding);
  void SmoothVertexFF(Embedding* embedding, OpenMesh::VPropHandleT<double> distance,
                    OpenMesh::VPropHandleT<OpenMesh::HalfedgeHandle> direction,
                    std::list<OpenMesh::HalfedgeHandle> mih_list);
  std::vector<OpenMesh::VertexHandle> ClosedNeighborhoodFF(Embedding* embedding,
       OpenMesh::VertexHandle bvh, OpenMesh::VPropHandleT<double> distance,
       OpenMesh::VPropHandleT<OpenMesh::HalfedgeHandle> direction,
       std::list<OpenMesh::HalfedgeHandle> mih_list);
  void Straightening(Embedding* embedding, StraighteningType strtype = PATCHWISE);

  void DataOutputParam(Embedding* embedding, QFile* file,
                       double target_length,
                       double scale,
                       uint iterations,
                       double alpha,
                       double beta,
                       SmoothingType smtype,
                       bool limitflips,
                       StraighteningType strtype,
                       CollapsingOrder corder);
  void DataOutputHeader(Embedding* embedding, QFile* file);
  void DataOutput(Embedding* embedding, QFile* file, double time_elapsed,
                  uint iteration);

  bool debug_hard_stop_ = false;
};
