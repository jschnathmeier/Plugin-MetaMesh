#include "IsotropicRemesher.hh"
#include <ACG/Geometry/bsp/TriangleBSPT.hh>
#include <QDebug>
#include <QGraphicsView>
#include <algorithm>

// TODO: if you are going to use the Remesher change the output path here
#define SCREENSHOT_PATH "/home/jschnathmeier/Screenshots"

/*!
 * \brief IsotropicRemesher::Remesh
 * \param embedding: embedding object, needs to be triangulated
 * \param target_length: target edge length as compared to the object diagonal length
 * \param iterations: number of times the algorithm runs
 * \param alpha: slack variable for the  upper length limit for splits
 * \param beta: slack variable for the lower length limit for collapses
 * \param smtype: smoothing options
 * \param screenshot: screenshot function; no screenshots if no function
 * \param limitflips: only do flips that don't increase edge length
 * \param strtype: straightening options
 * \param corder: collapse order options
 */
void IsotropicRemesher::Remesh(Embedding* embedding,
                               double target_length,
                               uint iterations,
                               double alpha,
                               double beta,
                               SmoothingType smtype,
                               std::function<void (QString, QString, double, double)> screenshot,
                               bool limitflips,
                               StraighteningType strtype,
                               CollapsingOrder corder,
                               bool dataoutput) {
  debug_hard_stop_ = false;
  using namespace PluginFunctions;
  qDebug() << "Running the Isotropic Remeshing algorithm";
  auto base_mesh = embedding->GetBaseMesh();
  if (screenshot || dataoutput) {
    QDir dir(QString(SCREENSHOT_PATH) + "/target" + QString::number(target_length)
             + "-iterations" + QString::number(iterations));
    if (!dir.exists()) {
      dir.mkpath(".");
    }
  }
  QFile param_file(QString(SCREENSHOT_PATH) + "/target" + QString::number(target_length)
                   + "-iterations" + QString::number(iterations) +  "/t"
                   + QString::number(target_length) + "i" + QString::number(iterations)
                   + ".param.txt");
  QFile output_file(QString(SCREENSHOT_PATH) + "/target" + QString::number(target_length)
                    + "-iterations" + QString::number(iterations) +  "/t"
                    + QString::number(target_length) + "i" + QString::number(iterations)
                    + ".csv");

  const double inf = std::numeric_limits<double>::infinity();
  ACG::Vec3d minCorner = {+inf, +inf, +inf};
  ACG::Vec3d maxCorner = {-inf, -inf, -inf};
  for (auto bvh : base_mesh->vertices()) {
      const auto& p = base_mesh->point(bvh);
      minCorner.minimize(p);
      maxCorner.maximize(p);
  }

  double scale = (maxCorner-minCorner).length();
  double high = target_length * alpha * scale;
  double low = high * beta / alpha;

  if (dataoutput) {
    output_file.resize(0);
    param_file.resize(0);
    DataOutputParam(embedding, &param_file, target_length, scale, iterations, alpha, beta,
                    smtype, limitflips, strtype, corder);
    DataOutputHeader(embedding, &output_file);
    DataOutput(embedding, &output_file, 0.0, 0);
  }
  if (screenshot) {
    screenshot(QString("IsoRemesh0_Start"),
               QString(SCREENSHOT_PATH) + "/target" + QString::number(target_length)
               + "-iterations" + QString::number(iterations) ,
               0, 0);
  }
  Stopwatch* swatchtotal = new Stopwatch();
  for (uint i = 0; i < iterations; ++i) {
    Stopwatch* swatchit = new Stopwatch();
    Stopwatch* swatchlocal = new Stopwatch();
    qDebug() << "Iteration" << i+1 << "; Splitting...";
    Splits(embedding, high);
    if (debug_hard_stop_) return;
    embedding->MetaGarbageCollection();
    embedding->CleanUpBaseMesh();
    if (screenshot) {
      screenshot(QString("IsoRemeshIt_") + QString::number(i) + QString(".0Splits"),
                 QString(SCREENSHOT_PATH) + "/target" + QString::number(target_length)
                 + "-iterations" + QString::number(iterations) ,
                 0, 0);
    }
    qDebug() << "Time elapsed: " << swatchlocal->Delta() << "ms.";
    swatchlocal->Reset();
    qDebug() << "Iteration" << i+1 << "; Collapsing...";
    Collapses(embedding, low, corder);
    if (debug_hard_stop_) return;
    embedding->MetaGarbageCollection();
    embedding->CleanUpBaseMesh();
    if (screenshot) {
      screenshot(QString("IsoRemeshIt_") + QString::number(i) + QString(".1Collapses"),
                 QString(SCREENSHOT_PATH) + "/target" + QString::number(target_length)
                 + "-iterations" + QString::number(iterations) ,
                 0, 0);
    }
    qDebug() << "Time elapsed: " << swatchlocal->Delta() << "ms.";
    swatchlocal->Reset();
    qDebug() << "Iteration" << i+1 << "; Flipping...";
    Flips(embedding, limitflips);
    if (debug_hard_stop_) return;
    embedding->MetaGarbageCollection();
    embedding->CleanUpBaseMesh();
    if (screenshot) {
      screenshot(QString("IsoRemeshIt_") + QString::number(i) + QString(".2Flips"),
                 QString(SCREENSHOT_PATH) + "/target" + QString::number(target_length)
                 + "-iterations" + QString::number(iterations) ,
                 0, 0);
    }
    qDebug() << "Time elapsed: " << swatchlocal->Delta() << "ms.";
    swatchlocal->Reset();
    qDebug() << "Iteration" << i+1 << "; Smoothing...";
    Smoothing(embedding, smtype);
    if (debug_hard_stop_) return;
    embedding->CleanUpBaseMesh();
    if (screenshot) {
      screenshot(QString("IsoRemeshIt_") + QString::number(i) + QString(".3Smoothing"),
                 QString(SCREENSHOT_PATH) + "/target" + QString::number(target_length)
                 + "-iterations" + QString::number(iterations) ,
                 0, 0);
    }
    qDebug() << "Time elapsed: " << swatchlocal->Delta() << "ms.";
    swatchlocal->Reset();
    qDebug() << "Iteration" << i+1 << "; Straightening...";
    Straightening(embedding, strtype);
    if (debug_hard_stop_) return;
    embedding->CleanUpBaseMesh();
    qDebug() << "Time elapsed: " << swatchlocal->Delta() << "ms.";
    double iteration_time = swatchit->Delta();
    qDebug() << "Iteration" << i+1 << " finished, time elapsed: "
             << iteration_time << "ms.";
    if (screenshot) {
      qDebug() << "Iteration" << i+1 << "Screenshot";
      screenshot(QString("IsoRemeshIt_") + QString::number(i) + QString(".4Straightening"),
                 QString(SCREENSHOT_PATH) + "/target" + QString::number(target_length)
                 + "-iterations" + QString::number(iterations) ,
                 0, 0);
    }
    if (dataoutput) {
      DataOutput(embedding, &output_file, iteration_time, i+1);
    }
  }
  qDebug() << "Finished " << iterations << " iterations of Isotropic Remeshing"
              " with target edge length " << target_length * scale;
  qDebug() << "Time elapsed: " << swatchtotal->Delta() << "ms.";
}

/*!
 * \brief IsotropicRemesher::Splits
 * \param embedding
 * \param high: split edges longer than this
 */
void IsotropicRemesher::Splits(Embedding* embedding, double high) {
  auto meta_mesh = embedding->GetMetaMesh();
  std::vector<OpenMesh::EdgeHandle> medges;
  for (auto meh : meta_mesh->edges()) {
    medges.push_back(meh);
  }
  std::random_shuffle(medges.begin(), medges.end());
  for (auto meh : medges) {
    if (embedding->CalculateEdgeLength(meh) > high) {
      embedding->Split(embedding->MiddleBvh(meh));
      if (embedding->ErrStatus()) {
        qDebug() << "Splitting failed for meta edge " << meh.idx();
        debug_hard_stop_ = true;
      }
    }
    if (meh.idx()%10 == 0) {
      embedding->CleanUpBaseMesh();
    }
  }
}

/*!
 * \brief IsotropicRemesher::Collapses
 * \param embedding
 * \param low: collapse edges shorter than this
 * \param corder: collapse ordering (random / optimizing by valence / optimizing by
 * edge weight heuristic)
 */
void IsotropicRemesher::Collapses(Embedding *embedding, double low,
                                  CollapsingOrder corder) {
  auto meta_mesh = embedding->GetMetaMesh();
  OpenMesh::VPropHandleT<bool> collapseblocked;
  meta_mesh->add_property(collapseblocked, "Blocks collapsing");
  for (auto mvh : meta_mesh->vertices()) {
    meta_mesh->property(collapseblocked, mvh) = false;
  }

  std::vector<OpenMesh::HalfedgeHandle> mhalfedges;
  for (auto mheh : meta_mesh->halfedges()) {
    mhalfedges.push_back(mheh);
  }
  std::random_shuffle(mhalfedges.begin(), mhalfedges.end());
  std::multimap<OpenMesh::HalfedgeHandle, double> mhehs;
  switch(corder) {
  case RANDOM: {
    for (auto mheh : mhalfedges) {
      mhehs.insert(std::make_pair(mheh, 0.0));
    }
    break;
  }
  case VALENCE: {
    for (auto mheh : mhalfedges) {
      double valw = 0.0;

      auto mheh0 = meta_mesh->prev_halfedge_handle(mheh);
      auto mheh1 = meta_mesh->opposite_halfedge_handle(meta_mesh->next_halfedge_handle(mheh));
      auto mheho = meta_mesh->opposite_halfedge_handle(mheh);
      auto mheh2 = meta_mesh->prev_halfedge_handle(mheho);
      auto mheh3 = meta_mesh->opposite_halfedge_handle(meta_mesh->next_halfedge_handle(mheho));

      auto mvh0 = meta_mesh->from_vertex_handle(mheh);
      auto mvh1 = meta_mesh->to_vertex_handle(mheh);
      auto mvh2 = meta_mesh->from_vertex_handle(mheh0);
      auto mvh3 = meta_mesh->from_vertex_handle(mheh2);

      valw = meta_mesh->valence(mvh0) + meta_mesh->valence(mvh1);
      if (meta_mesh->from_vertex_handle(mheh0)
          == meta_mesh->from_vertex_handle(mheh1)) {
        valw -= meta_mesh->valence(mvh2);
      }
      if (meta_mesh->from_vertex_handle(mheh2)
          == meta_mesh->from_vertex_handle(mheh3)) {
        valw -= meta_mesh->valence(mvh3);
      }

      mhehs.insert(std::make_pair(mheh, valw));
    }
    break;
  }
  case SPLITWEIGHT: {
    for (auto mheh : mhalfedges) {
      double splitw = 0.0;

      auto mheh0 = meta_mesh->prev_halfedge_handle(mheh);
      auto mheh1 = meta_mesh->opposite_halfedge_handle(meta_mesh->next_halfedge_handle(mheh));
      auto mheho = meta_mesh->opposite_halfedge_handle(mheh);
      auto mheh2 = meta_mesh->prev_halfedge_handle(mheho);
      auto mheh3 = meta_mesh->opposite_halfedge_handle(meta_mesh->next_halfedge_handle(mheho));

      auto mvh0 = meta_mesh->from_vertex_handle(mheh);

      int newmvh1edges = static_cast<int>(meta_mesh->valence(mvh0)) - 2;
      if (meta_mesh->from_vertex_handle(mheh0)
          == meta_mesh->from_vertex_handle(mheh1)) {
        splitw -= embedding->MetaHalfedgeWeight(mheh0);
        newmvh1edges -= 1;
      }
      auto mhehiter = meta_mesh->prev_halfedge_handle(
            meta_mesh->opposite_halfedge_handle(mheh0));
      while (mhehiter != mheh3) {
        splitw -= embedding->MetaHalfedgeWeight(mhehiter);
        mhehiter = meta_mesh->prev_halfedge_handle(
              meta_mesh->opposite_halfedge_handle(mhehiter));
      }
      if (meta_mesh->from_vertex_handle(mheh2)
          == meta_mesh->from_vertex_handle(mheh3)) {
        splitw -= embedding->MetaHalfedgeWeight(mheh3);
        newmvh1edges -= 1;
      }
      splitw += newmvh1edges * embedding->MetaHalfedgeWeight(mheh);
      mhehs.insert(std::make_pair(mheh, splitw));
    }
    break;
  }
  }

  uint mhehcounter = 0;

  for (auto pair : mhehs) {
    auto mheh = pair.first;
    if (meta_mesh->is_valid_handle(mheh)
        && !meta_mesh->status(mheh).deleted()) {
      auto meh = meta_mesh->edge_handle(mheh);
      if (!meta_mesh->property(collapseblocked,
             meta_mesh->from_vertex_handle(mheh))
          && embedding->IsCollapseOkay(mheh)
          && embedding->CalculateEdgeLength(meh) < low) {
        // If a collapse is about to happen block off the 1-Ring from being
        // collapsed in this iteration.
        for (auto mvh : meta_mesh->vv_range(meta_mesh->from_vertex_handle(mheh))) {
          meta_mesh->property(collapseblocked, mvh) = true;
        }
        embedding->Collapse(mheh);
      }
    }
    if (mheh.idx() % 100 == 0) {
      qDebug() << "Collapsing mheh: " << mhehcounter;
    }
    ++mhehcounter;
  }
  embedding->CleanUpBaseMesh();
  embedding->MetaGarbageCollection();//);
  meta_mesh->remove_property(collapseblocked);
}

/*!
 * \brief IsotropicRemesher::Flips
 * \param embedding
 * \param limitflips: if this is true flips are only done when they don't increase
 * edge length  (expensive check)
 */
void IsotropicRemesher::Flips(Embedding *embedding, bool limitflips) {
  auto meta_mesh = embedding->GetMetaMesh();
  OpenMesh::EPropHandleT<int> flipvalue;
  meta_mesh->add_property(flipvalue, "EP_flipvalue");
  std::vector<OpenMesh::EdgeHandle> medges;
  for (auto meh : meta_mesh->edges()) {
    meta_mesh->property(flipvalue, meh) = FlipEval(embedding, meh);
    medges.push_back(meh);
  }
  std::random_shuffle(medges.begin(), medges.end());
  std::multimap<OpenMesh::EdgeHandle, double> mehs;
  for (auto meh : medges) {
    mehs.insert(std::make_pair(meh, -embedding->MetaHalfedgeWeight(
                               meta_mesh->halfedge_handle(meh, 0))));
  }
  for (auto mpair : mehs) {
    auto meh = mpair.first;
    double newlen, oldlen;
    if (meta_mesh->property(flipvalue, meh) > 0) {
      auto v0 = meta_mesh->to_vertex_handle(meta_mesh->halfedge_handle(meh, 0));
      auto v1 = meta_mesh->to_vertex_handle(meta_mesh->halfedge_handle(meh, 1));
      auto v2 = meta_mesh->to_vertex_handle(meta_mesh->next_halfedge_handle(
                                              meta_mesh->halfedge_handle(meh, 0)));
      auto v3 = meta_mesh->to_vertex_handle(meta_mesh->next_halfedge_handle(
                                              meta_mesh->halfedge_handle(meh, 1)));
      bool flip = true;
      for (auto ee : meta_mesh->ve_range(v0)) {
        if (meta_mesh->property(flipvalue, ee) > meta_mesh->property(flipvalue, meh)) {
          flip = false;
        }
      }
      for (auto ee : meta_mesh->ve_range(v1)) {
        if (meta_mesh->property(flipvalue, ee) > meta_mesh->property(flipvalue, meh)){
          flip = false;
        }
      }
      if (flip) {
        if (limitflips) {
          oldlen = embedding->CalculateEdgeLength(meh);
        }
        embedding->Rotate(meh);
        if (limitflips) {
          newlen = embedding->CalculateEdgeLength(meh);
        }
        if (!limitflips || oldlen >= newlen) {
          for (auto ee : meta_mesh->ve_range(v0)) {
            meta_mesh->property(flipvalue, ee) = FlipEval(embedding, ee);
          }
          for (auto ee : meta_mesh->ve_range(v1)) {
            meta_mesh->property(flipvalue, ee) = FlipEval(embedding, ee);
          }
          for (auto ee : meta_mesh->ve_range(v2)) {
            meta_mesh->property(flipvalue, ee) = FlipEval(embedding, ee);
          }
          for (auto ee : meta_mesh->ve_range(v3)) {
            meta_mesh->property(flipvalue, ee) = FlipEval(embedding, ee);
          }
        } else {
          embedding->Rotate(meh);
        }
      }
    }
    if (meh.idx()%10 == 0) {
      embedding->CleanUpBaseMesh();
    }
  }
  meta_mesh->remove_property(flipvalue);
}

/*!
 * \brief IsotropicRemesher::FlipEval: evaluate whether an edge should be flipped
 * \param embedding
 * \param meh
 * \return the improvement of the meshes' valence deviation if this split
 * was performed
 */
int IsotropicRemesher::FlipEval(Embedding *embedding, OpenMesh::EdgeHandle meh) {
  auto meta_mesh = embedding->GetMetaMesh();
  auto heh0 = meta_mesh->halfedge_handle(meh, 0);
  auto heh1 = meta_mesh->halfedge_handle(meh, 1);
  auto fv = meta_mesh->to_vertex_handle(heh0);
  auto lv = meta_mesh->to_vertex_handle(meta_mesh->next_halfedge_handle(heh0));
  auto bv = meta_mesh->to_vertex_handle(heh1);
  auto rv = meta_mesh->to_vertex_handle(meta_mesh->next_halfedge_handle(heh1));
  uint local_objective_old = (meta_mesh->valence(bv)-6)*(meta_mesh->valence(bv)-6)
          +(meta_mesh->valence(fv)-6)*(meta_mesh->valence(fv)-6)
          +(meta_mesh->valence(rv)-6)*(meta_mesh->valence(rv)-6)
          +(meta_mesh->valence(lv)-6)*(meta_mesh->valence(lv)-6);
  uint local_objective_new = (meta_mesh->valence(bv)-7)*(meta_mesh->valence(bv)-7)
          +(meta_mesh->valence(fv)-7)*(meta_mesh->valence(fv)-7)
          +(meta_mesh->valence(rv)-5)*(meta_mesh->valence(rv)-5)
          +(meta_mesh->valence(lv)-5)*(meta_mesh->valence(lv)-5);
  int local_improvement = static_cast<int>(local_objective_old - local_objective_new);
  // Triangles,
  // Old: (bv, rv, fv), (bv, fv, lv)
  auto fnold1 = (meta_mesh->point(rv)-meta_mesh->point(bv))
          %(meta_mesh->point(fv)-meta_mesh->point(bv));
  auto fnold2 = (meta_mesh->point(fv)-meta_mesh->point(bv))
          %(meta_mesh->point(lv)-meta_mesh->point(bv));
  // New: (bv, rv, lv), (fv, lv, rv)
  auto fnnew1 = (meta_mesh->point(rv)-meta_mesh->point(bv))
          %(meta_mesh->point(lv)-meta_mesh->point(bv));
  auto fnnew2 = (meta_mesh->point(lv)-meta_mesh->point(fv))
          %(meta_mesh->point(rv)-meta_mesh->point(fv));

  bool geometrically_sound = false;
  if(((fnold1|fnnew1) > 0 || (fnold2|fnnew1) > 0)
          && ((fnold1|fnnew2) > 0 || (fnold2|fnnew2) > 0))
      geometrically_sound = true;

  if (local_improvement > 0
          && embedding->IsRotationOkay(meh)
          && geometrically_sound) {
      return local_improvement;
  }
  return 0;
}

/*!
 * \brief IsotropicRemesher::Smoothing
 * \param embedding
 * \param stype: smoothing options; forestfire, vertexweights or vertexdistances
 * \param shakeup: smoothes vertices twice, moving them away from center first and then
 * back to it. Not recommended as it is very expensive and doesn't seem to help much.
 */
void IsotropicRemesher::Smoothing(Embedding *embedding, SmoothingType stype) {
  switch(stype) {
    case FORESTFIRE: {
      qDebug() << "Forest Fire Smoothing";
      SmoothingFF(embedding);
      break;
    }
    case VERTEXWEIGHTS: {
      qDebug() << "Vertex Weights Smoothing";
      SmoothingVWD(embedding, VERTEXWEIGHTS);
      break;
    }
    case VERTEXDISTANCES:{
      qDebug() << "Vertex Distances Smoothing";
      SmoothingVWD(embedding, VERTEXDISTANCES);
      break;
    }
  }
}

/*!
 * \brief IsotropicRemesher::SmoothingVWD
 * \param embedding
 * \param stype
 * \param shakeup
 */
void IsotropicRemesher::SmoothingVWD(Embedding *embedding, SmoothingType stype) {
  auto meta_mesh = embedding->GetMetaMesh();
  auto base_mesh = embedding->GetBaseMesh();
  OpenMesh::VPropHandleT<std::vector<double>> distances;
  OpenMesh::VPropHandleT<OpenMesh::HalfedgeHandle> direction;
  base_mesh->add_property(distances, "Distances from each patch vertex");
  std::vector<OpenMesh::VertexHandle> mvhlist;
  std::vector<OpenMesh::VertexHandle *> mvhlist_pointers;
  for (auto mvh : meta_mesh->vertices()) {
    mvhlist.push_back(mvh);
  }
  std::random_shuffle(mvhlist.begin(), mvhlist.end());
  uint ctr = 0;
  for (auto mvh : mvhlist) {
    if (meta_mesh->is_valid_handle(mvh)
        && !meta_mesh->status(mvh).deleted()) {
      if (ctr%10 == 0) {
        qDebug() << "Smoothing meta vertex " << ctr;
      }
      for (auto bvh : base_mesh->vertices()) {
        base_mesh->property(distances, bvh) = {};
      }
      SmoothVertexVWD(embedding, &distances, mvh, stype);
      if (debug_hard_stop_) return;
    }
    ++ctr;
  }
  embedding->CleanUpBaseMesh();
  embedding->MetaGarbageCollection();//mvhlist_pointers);
  base_mesh->remove_property(distances);
}

/*!
 * \brief IsotropicRemesher::SmoothVertexVWD
 * \param embedding
 * \param distances
 * \param mvh
 * \param stype
 * \param shakeup
 */
void IsotropicRemesher::SmoothVertexVWD(Embedding *embedding,
                        const OpenMesh::VPropHandleT<std::vector<double>>* distances,
                        OpenMesh::VertexHandle mvh,
                        SmoothingType stype) {
  const double inf = std::numeric_limits<double>::infinity();
  auto meta_mesh = embedding->GetMetaMesh();
  auto base_mesh = embedding->GetBaseMesh();
  std::list<OpenMesh::HalfedgeHandle> mih_list;
  for (auto mhehit : meta_mesh->vih_range(mvh)) {
    if (meta_mesh->is_valid_handle(mhehit)
        && !meta_mesh->status(mhehit).deleted()) {
      mih_list.push_back(mhehit);
    }
  }
  //qDebug() << "Smoothing Vertex " << mvh.idx();
  uint it = 0;
  double mindist = inf;
  auto bvhmin = OpenMesh::PolyConnectivity::InvalidVertexHandle;
  struct cmp {
    bool operator()(const std::pair<OpenMesh::VertexHandle, std::vector<double>> &a,
                    const std::pair<OpenMesh::VertexHandle, std::vector<double>> &b) {
      return a.second.back() > b.second.back();
    }
  };
  std::priority_queue<std::pair<OpenMesh::VertexHandle, std::vector<double>>,
      std::vector<std::pair<OpenMesh::VertexHandle, std::vector<double>>>, cmp> minheap;
  for (auto mhehit : mih_list) {
    //qDebug() << "Measuring sector distances for boundary vertex "
    //         << meta_mesh->from_vertex_handle(mhehit).idx();
    if (meta_mesh->from_vertex_handle(mhehit) == meta_mesh->to_vertex_handle(mhehit)) {
      //qDebug() << "Self Edge";
      break;
    }
    auto mvhit = meta_mesh->from_vertex_handle(mhehit);
    auto bvhit = meta_mesh->property(embedding->mv_connection_, mvhit);
    assert(base_mesh->is_valid_handle(bvhit));
    assert(embedding->IsSectorBorder(bvhit, mih_list));
    std::queue<OpenMesh::VertexHandle> patchvertices;
    auto bhehit = meta_mesh->property(embedding->mhe_connection_, mhehit);
    assert(base_mesh->property(*distances,
           base_mesh->to_vertex_handle(bhehit)).size() == it);
    assert(base_mesh->property(embedding->bhe_connection_, bhehit) == mhehit);
    if (embedding->IsSectorBorder(base_mesh->to_vertex_handle(bhehit), mih_list)) {
        qDebug() << "The first bheh " << base_mesh->to_vertex_handle(bhehit).idx()
                 << "of an mheh ends in a sector border outside its patch, this should "
                    "be impossible.";
        qDebug() << "The bheh starts with a meta vertex:" << meta_mesh->is_valid_handle(
                        base_mesh->property(embedding->bv_connection_,
                        base_mesh->from_vertex_handle(bhehit)));
        qDebug() << "The bheh ends in a meta vertex:" << meta_mesh->is_valid_handle(
                        base_mesh->property(embedding->bv_connection_,
                        base_mesh->to_vertex_handle(bhehit)));
        qDebug() << "The bheh ends in:" << base_mesh->property(embedding->bv_connection_,
                     base_mesh->to_vertex_handle(bhehit)).idx() << " the middle vertex "
                     "of the patch is:" << mvh.idx();
    }
    base_mesh->property(*distances, base_mesh->to_vertex_handle(bhehit)).push_back(
        base_mesh->calc_edge_length(bhehit));
    minheap.push(std::make_pair(base_mesh->to_vertex_handle(bhehit),
                 base_mesh->property(*distances, base_mesh->to_vertex_handle(bhehit))));
    // patchvertices is now populated by vertices inside the patch surrounding the seed
    // meta vertex
    //qDebug() << "Distance Scoring Loop";
    while(!minheap.empty()) {
      auto bvhcurr = minheap.top().first;
      minheap.pop();

      bool borderparity = (meta_mesh->is_boundary(mvh) == base_mesh->is_boundary(bvhcurr));

      //qDebug() << "Distance Scoring";
      if (stype == VERTEXWEIGHTS) {
        auto currdist = DistanceScoreVW(embedding, distances, bvhcurr,
                                        static_cast<uint>(mih_list.size()));
        if (borderparity && currdist < mindist) {
          mindist = currdist;
          bvhmin = bvhcurr;
        }
      } else if (stype == VERTEXDISTANCES) {
        auto currdist = DistanceScoreVD(embedding, distances, bvhcurr,
                                        static_cast<uint>(mih_list.size()));
        if (borderparity && currdist < mindist) {
          mindist = currdist;
          bvhmin = bvhcurr;
        }
      } else {
        qFatal("Called smoothing with invalid parameters.");
        return;
      }

      auto bvhcurrdistprop = base_mesh->property(*distances, bvhcurr);
      if (bvhcurrdistprop.size() < it+1) {
        qDebug() << "Iteration " << it << ", Vertex " << bvhcurr.idx()
                 << "'s distance property only has size "
                 << bvhcurrdistprop.size();
        qDebug() << "The vertex is a meta vertex:"
                 << meta_mesh->is_valid_handle(base_mesh->property(
                                                 embedding->bv_connection_, bvhcurr));
        qDebug() << "The vertex is a sector border:"
                 << embedding->IsSectorBorder(bvhcurr);
        qDebug() << "The vertex is a sector border (with exceptions):"
                 << embedding->IsSectorBorder(bvhcurr, mih_list);
        base_mesh->status(bvhcurr).set_selected(true);
        embedding->MarkVertex(bvhcurr);
        qDebug() << "Marking the middle of the patch";
        embedding->MarkVertex(meta_mesh->property(embedding->mv_connection_, mvh));
        debug_hard_stop_ = true;
        return;
      }

      //qDebug() << "Finding Neighbors";
      // Find neighbors, limited by sector border
      for (auto boheh : base_mesh->voh_range(bvhcurr)) {
        if (!embedding->IsSectorBorder(base_mesh->to_vertex_handle(boheh), mih_list)) {
          double newlength = base_mesh->property(*distances, bvhcurr).at(it)
              + base_mesh->calc_edge_length(boheh);
          if (base_mesh->property(*distances, base_mesh->to_vertex_handle(boheh)).size() < it+1) {
            base_mesh->property(*distances, base_mesh->to_vertex_handle(boheh))
                .push_back(newlength);
            assert(base_mesh->property(*distances, base_mesh->to_vertex_handle(boheh)).size()
                   == it+1);
            minheap.push(std::make_pair(base_mesh->to_vertex_handle(boheh),
                         base_mesh->property(*distances, base_mesh->to_vertex_handle(boheh))));
          } else if (base_mesh->property(*distances, base_mesh->to_vertex_handle(boheh)).at(it)
                     > newlength) {
            base_mesh->property(*distances, base_mesh->to_vertex_handle(boheh)).at(it)
                = newlength;
            minheap.push(std::make_pair(base_mesh->to_vertex_handle(boheh),
                         base_mesh->property(*distances, base_mesh->to_vertex_handle(boheh))));
          }
        }
      }
    }
    ++it;
  }
  // The new, faster relocate method sadly cannot yet handle complex patches.
  // Make sure to use the old method if a patch is complex, so check that here.
  bool simple = true;
  for (auto moh : meta_mesh->voh_range(mvh)) {
    auto mvhit = meta_mesh->to_vertex_handle(moh);
    if (mvhit == mvh) {
      simple = false;
    }
  }
  if (simple) {
    embedding->Relocate(mvh, bvhmin);
  } else {
    embedding->Relocate(mvh, bvhmin, false);
  }
  if (embedding->ErrStatus()) {
    qDebug() << "Relocation failed for vertex " << mih_list.front().idx();
    debug_hard_stop_ = true;
  }
}

/*!
 * \brief IsotropicRemesher::DistanceScoreVW
 * \param embedding
 * \param distances
 * \param bvh
 * \param numv
 * \return the distance score for vertex weight smoothing; the sum of the quadratic
 * distances from the patch vertices (minimize this)
 */
double IsotropicRemesher::DistanceScoreVW(Embedding *embedding,
                          const OpenMesh::VPropHandleT<std::vector<double>>* distances,
                          OpenMesh::VertexHandle bvh, uint numv) {
  const double inf = std::numeric_limits<double>::infinity();
  auto base_mesh = embedding->GetBaseMesh();
  if (base_mesh->property(*distances, bvh).size() < numv) {
    return inf;
  }
  double retval = 0;
  for (double num : base_mesh->property(*distances, bvh)) {
    retval += num*num;
  }
  if (base_mesh->is_valid_handle(base_mesh->property(embedding->bsplithandle_, bvh))) {
    retval *= 2;
  }
  return retval;
}

/*!
 * \brief IsotropicRemesher::DistanceScoreVD
 * \param embedding
 * \param distances
 * \param bvh
 * \param numv
 * \return the distance score for vertex distance smoothing; returns the highest distance
 * bvh has from any of the patch vertices; (minimize this)
 */
double IsotropicRemesher::DistanceScoreVD(Embedding *embedding,
                          const OpenMesh::VPropHandleT<std::vector<double>>* distances,
                          OpenMesh::VertexHandle bvh, uint numv) {
  const double inf = std::numeric_limits<double>::infinity();
  auto base_mesh = embedding->GetBaseMesh();
  if (base_mesh->property(*distances, bvh).size() < numv) {
    return inf;
  }
  double retval = 0;
  for (double num : base_mesh->property(*distances, bvh)) {
    if (num > retval) {
      retval = num;
    }
  }
  if (base_mesh->is_valid_handle(base_mesh->property(embedding->bsplithandle_, bvh))) {
    retval *= 2;
  }
  return retval;
}

/*!
 * \brief IsotropicRemesher::SmoothingFF
 * \param embedding
 */
void IsotropicRemesher::SmoothingFF(Embedding *embedding) {
  const double inf = std::numeric_limits<double>::infinity();
  auto meta_mesh = embedding->GetMetaMesh();
  auto base_mesh = embedding->GetBaseMesh();
  OpenMesh::VPropHandleT<double> distance;
  OpenMesh::VPropHandleT<OpenMesh::HalfedgeHandle> direction;
  base_mesh->add_property(distance, "Distance from the patch border");
  base_mesh->add_property(direction, "Direction to aquire first neighbors");
  std::queue<OpenMesh::VertexHandle> mvhlist;
  for (auto mvh : meta_mesh->vertices()) {
    mvhlist.push(mvh);
  }
  while (!mvhlist.empty()) {
    auto mvh = mvhlist.front();
    mvhlist.pop();
    if (meta_mesh->is_valid_handle(mvh)
        && !meta_mesh->status(mvh).deleted()) {
      if (mvh.idx()%10 == 0) {
        qDebug() << "Smoothing meta vertex " << mvh.idx();
      }
      for (auto bvh : base_mesh->vertices()) {
        base_mesh->property(distance, bvh) = inf;
        base_mesh->property(direction, bvh) =
            OpenMesh::PolyConnectivity::InvalidHalfedgeHandle;
      }
      std::list<OpenMesh::HalfedgeHandle> mih_list;
      for (auto mhehit : meta_mesh->vih_range(mvh)) {
        mih_list.push_back(mhehit);
      }
      SmoothVertexFF(embedding, distance, direction, mih_list);
      if (debug_hard_stop_) return;
      embedding->CleanUpBaseMesh();
    }
  }
  embedding->MetaGarbageCollection();
  base_mesh->remove_property(distance);
  base_mesh->remove_property(direction);
}

/*!
 * \brief IsotropicRemesher::SmoothVertexFF
 * \param embedding
 * \param distance: property used for smoothing
 * \param direction: property used for smoothing
 * \param mih_list: list of incoming meta halfedges towards the center of the patch
 */
void IsotropicRemesher::SmoothVertexFF(Embedding *embedding,
                        OpenMesh::VPropHandleT<double> distance,
                        OpenMesh::VPropHandleT<OpenMesh::HalfedgeHandle> direction,
                        std::list<OpenMesh::HalfedgeHandle> mih_list) {
  const double inf = std::numeric_limits<double>::infinity();
  auto meta_mesh = embedding->GetMetaMesh();
  auto base_mesh = embedding->GetBaseMesh();
  auto mvh = meta_mesh->to_vertex_handle(mih_list.front());
  struct cmp {
    bool operator()(const std::pair<OpenMesh::VertexHandle, double> &a,
                    const std::pair<OpenMesh::VertexHandle, double> &b) {
      return a.second > b.second;
    }
  };
  std::priority_queue<std::pair<OpenMesh::VertexHandle, double>,
      std::vector<std::pair<OpenMesh::VertexHandle, double>>, cmp> minheap;

  for (auto mhehit : mih_list) {
    auto mheprev = meta_mesh->prev_halfedge_handle(mhehit);
    while (meta_mesh->from_vertex_handle(mheprev) != mvh) {
      auto bhehit = meta_mesh->property(embedding->mhe_connection_, mheprev);
      assert(bhehit.is_valid());
      assert(base_mesh->is_valid_handle(bhehit));
      auto bvhit = base_mesh->from_vertex_handle(bhehit);
      base_mesh->property(distance, bvhit) = 0.0;
      base_mesh->property(direction, bvhit) = bhehit;
      minheap.push(std::make_pair(bvhit, 0.0));
      while (base_mesh->property(embedding->next_heh_, bhehit).is_valid()) {
        bhehit = base_mesh->property(embedding->next_heh_, bhehit);
        bvhit = base_mesh->from_vertex_handle(bhehit);
        base_mesh->property(distance, bvhit) = 0.0;
        base_mesh->property(direction, bvhit) = bhehit;
        minheap.push(std::make_pair(bvhit, 0.0));
      }
      mheprev = meta_mesh->prev_halfedge_handle(mheprev);
    }
  }

  OpenMesh::VertexHandle middle = OpenMesh::PolyConnectivity::InvalidVertexHandle;
  double maxdist = -inf;

  while (!minheap.empty()) {
    auto bvcurr = minheap.top().first;
    auto neighbors = ClosedNeighborhoodFF(embedding, bvcurr, distance, direction, mih_list);
    if (!neighbors.empty()) {
      for (auto neighbor : neighbors) {
        //base_mesh->status(base_mesh->find_halfedge(bvcurr, neighbor)).set_selected(true);
        minheap.push(std::make_pair(neighbor, base_mesh->property(distance, neighbor)));
      }
    }
    double newdist = minheap.top().second;
    if (base_mesh->is_valid_handle(base_mesh->property(embedding->bsplithandle_,
                                                       minheap.top().first))) {
      newdist *= 2;
    }
    if (maxdist < newdist) {
      maxdist = newdist;
      middle = minheap.top().first;
    }
    minheap.pop();
  }

  assert(middle.is_valid());

  embedding->Relocate(meta_mesh->to_vertex_handle(mih_list.front()), middle);
  if (embedding->ErrStatus()) {
    qDebug() << "Relocation failed for vertex " << mih_list.front().idx();
    debug_hard_stop_ = true;
  }
  //qDebug() << "Finished Relocating";
}

/*!
 * \brief IsotropicRemesher::ClosedNeighborhoodFF
 * \param embedding
 * \param bvh
 * \param distance
 * \param direction
 * \param mih_list
 * \return the vertices in the neighborhood of bvh, considering patch borders
 */
std::vector<OpenMesh::VertexHandle> IsotropicRemesher::ClosedNeighborhoodFF(
    Embedding* embedding, OpenMesh::VertexHandle bvh,
    OpenMesh::VPropHandleT<double> distance,
    OpenMesh::VPropHandleT<OpenMesh::HalfedgeHandle> direction,
    std::list<OpenMesh::HalfedgeHandle> mih_list) {
  auto base_mesh = embedding->GetBaseMesh();
  auto meta_mesh = embedding->GetMetaMesh();
  std::vector<OpenMesh::VertexHandle> retval;
  auto bdistance = base_mesh->property(distance, bvh);
  auto mvh = meta_mesh->to_vertex_handle(mih_list.front());
  if (embedding->IsSectorBorder(bvh, mih_list) ||
      (base_mesh->property(embedding->bv_connection_, bvh).is_valid()
       && base_mesh->property(embedding->bv_connection_, bvh) != mvh)) {
    auto bheh0 = base_mesh->property(direction, bvh);
    if (!base_mesh->is_valid_handle(bheh0)) {
      bheh0 = base_mesh->halfedge_handle(bvh);
    }
    auto bhehit = base_mesh->opposite_halfedge_handle(base_mesh->prev_halfedge_handle(bheh0));
    auto start = bhehit;
    do {
      auto blength = base_mesh->calc_edge_length(bhehit);
      auto bvdis = base_mesh->property(distance, bvh);
      auto itdis = base_mesh->property(distance, base_mesh->to_vertex_handle(bhehit));
      if (bvdis + blength < itdis) {
        base_mesh->property(distance, base_mesh->to_vertex_handle(bhehit)) = bvdis + blength;
        retval.push_back(base_mesh->to_vertex_handle(bhehit));
      }
      bhehit = base_mesh->opposite_halfedge_handle(base_mesh->prev_halfedge_handle(bhehit));
    } while(start != bhehit
            && !embedding->IsSectorBorder(base_mesh->to_vertex_handle(bhehit), mih_list));
  } else {
    for (auto bvhit : base_mesh->vv_range(bvh)) {
      if (!embedding->IsSectorBorder(bvhit, mih_list)) {
        auto blength = base_mesh->calc_edge_length(base_mesh->find_halfedge(bvhit, bvh));
        auto itdistance = base_mesh->property(distance, bvhit);
        if (bdistance + blength < itdistance) {
          itdistance = bdistance + blength;
          base_mesh->property(distance, bvhit) = itdistance;
          retval.push_back(bvhit);
        }
      }
    }
  }
  return retval;
}

/*!
 * \brief IsotropicRemesher::Straightening
 * \param embedding
 * \param strtype
 */
void IsotropicRemesher::Straightening(Embedding *embedding, StraighteningType strtype) {
  auto meta_mesh = embedding->GetMetaMesh();
  auto base_mesh = embedding->GetBaseMesh();
  uint ctr = 0;
  switch(strtype) {
  case NONE: {
    break;
  }
  case EDGEWISE: {
    // Straighten edgewise
    std::vector<OpenMesh::EdgeHandle> medges;
    for (auto meh : meta_mesh->edges()) {
      medges.push_back(meh);
    }
    std::random_shuffle(medges.begin(), medges.end());
    for (auto meh : medges) {
      if (ctr%100 == 0) {
        qDebug() << "Straightening edge " << ctr;
      }
      embedding->Retrace(meta_mesh->halfedge_handle(meh, 0));
      ++ctr;
      if (ctr%20 == 0) {
        embedding->CleanUpBaseMesh();
      }
    }
    break;
  }
  case PATCHWISE: {
  // Straighten patchwise
    std::vector<OpenMesh::VertexHandle> mverts;
    for (auto mvh : meta_mesh->vertices()) {
      mverts.push_back(mvh);
    }
    std::random_shuffle(mverts.begin(), mverts.end());
    for (auto mvh : mverts) {
      if (ctr%10 == 0) {
        qDebug() << "Straightening vertex " << ctr;
      }
      if (!meta_mesh->status(mvh).deleted()) {
        if (embedding->ErrStatus()) {
          debug_hard_stop_  = true;
          return;
        }

        // Non-original vertices may slip away upon cleanup if the patch around them
        // is cleared. In that case cleanup that patch edgewise
        if (base_mesh->is_valid_handle(base_mesh->property(embedding->bsplithandle_,
              meta_mesh->property(embedding->mv_connection_, mvh)))) {
          for (auto moh : meta_mesh->voh_range(mvh)) {
            embedding->Retrace(moh);
          }
        } else {
          /*
          enum facetype{noselfedgesdisc, selfedgesorhighergenus, boundary};
          facetype ft = noselfedgesdisc;
          for (auto moh : meta_mesh->voh_range(mvh)) {
            auto mvhit = meta_mesh->to_vertex_handle(moh);
            if (mvhit == mvh) {
              ft = selfedgesorhighergenus;
            }
            if (meta_mesh->is_boundary(meta_mesh->to_vertex_handle(moh))
                && ft == noselfedgesdisc) {
              ft = boundary;
            }
          }
          switch(ft) {
          case noselfedgesdisc: {
            for (auto moh : meta_mesh->voh_range(mvh)) {
              embedding->RemoveMetaEdgeFromBaseMesh(moh);
            }
            embedding->BaseGarbageCollection();
            for (auto moh : meta_mesh->voh_range(mvh)) {
              embedding->Trace(moh);
            }
            break;
          }
          case boundary: {
            for (auto moh : meta_mesh->voh_range(mvh)) {
              embedding->RemoveMetaEdgeFromBaseMesh(moh);
            }
            embedding->BaseGarbageCollection();
            auto mheh0 = meta_mesh->halfedge_handle(mvh);
            auto mheh1 = meta_mesh->prev_halfedge_handle(mheh0);
            embedding->Trace(mheh0);
            if (mheh0 != mheh1) {
              embedding->Trace(mheh1);
            }
            for (auto moh : meta_mesh->voh_range(mvh)) {
              if (moh != mheh0 && moh != mheh1) {
                embedding->Trace(moh);
              }
            }
            break;
          }
          case selfedgesorhighergenus: {
            embedding->Relocate(mvh, meta_mesh->property(embedding->mv_connection_, mvh));
            break;
          }
          }
          */
          embedding->Relocate(mvh, meta_mesh->property(embedding->mv_connection_, mvh));
        }
      }
      if (ctr%20 == 0) {
        embedding->CleanUpBaseMesh();
      }
      ++ctr;
    }
    break;
  }
  }
}

void IsotropicRemesher::DataOutputParam(Embedding *embedding,
                                        QFile *file,
                                        double target_length,
                                        double scale,
                                        uint iterations,
                                        double alpha,
                                        double beta,
                                        SmoothingType smtype,
                                        bool limitflips,
                                        StraighteningType strtype,
                                        CollapsingOrder corder) {
  if(file->open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text)) {
    QString target_length_str = QString::number(target_length);
    QString target_length_adjusted_str = QString::number(target_length*scale);
    QString iterations_str = QString::number(iterations);
    QString alpha_str = QString::number(alpha);
    QString beta_str = QString::number(beta);
    QString smtype_str;
    switch(smtype) {
    case FORESTFIRE: {
      smtype_str = QString("ForestFire");
      break;
    }
    case VERTEXWEIGHTS: {
      smtype_str = QString("VertexWeights");
      break;
    }
    case VERTEXDISTANCES: {
      smtype_str = QString("VertexDistances");
      break;
    }
    }
    QString limitflips_str;
    if (limitflips) {
      limitflips_str = QString("true");
    } else {
      limitflips_str = QString("false");
    }
    QString strtype_str;
    switch (strtype) {
    case NONE: {
      strtype_str = QString("None");
      break;
    }
    case EDGEWISE: {
      strtype_str = QString("Edgewise");
      break;
    }
    case PATCHWISE: {
      strtype_str = QString("Patchwise");
      break;
    }
    }
    QString corder_str;
    switch (corder) {
    case RANDOM: {
      corder_str = QString("Random");
      break;
    }
    case VALENCE: {
      corder_str = QString("Valence");
      break;
    }
    case SPLITWEIGHT: {
      corder_str = QString("Splitweight");
      break;
    }
    }

    QTextStream tstream(file);
    tstream << "Target edge length: " << target_length_str.toStdString().c_str() << endl
            << "Adjusted length: " << target_length_adjusted_str.toStdString().c_str() << endl
            << "Iterations: " << iterations_str.toStdString().c_str() << endl
            << "Alpha: " << alpha_str.toStdString().c_str() << endl
            << "Beta: " << beta_str.toStdString().c_str() << endl
            << "Smoothing type: " << smtype_str.toStdString().c_str() << endl
            << "Limit Flips: " << limitflips_str.toStdString().c_str() << endl
            << "Straightening Type: " << strtype_str.toStdString().c_str() << endl
            << "Collapsing Order: " << corder_str.toStdString().c_str();
    file->close();
  }
}

void IsotropicRemesher::DataOutputHeader(Embedding *embedding, QFile *file) {
  if(file->open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text)) {
    QTextStream tstream(file);
    tstream << "iteration,"
            << "time,"
            << "basefaces,"
            << "metafaces,"
            << "valence.avg,"
            << "valence.sd,"
            << "edgelength.avg,"
            << "edgelength.sd";
    tstream << endl;
    file->close();
  }
}

/*!
 * \brief IsotropicRemesher::DataOutput
 * write data about the current iteration into the file
 * \param embedding
 * \param file
 * \param time_elapsed
 * \param iteration
 */
void IsotropicRemesher::DataOutput(Embedding *embedding,
                                   QFile* file,
                                   double time_elapsed,
                                   uint iteration) {
  if(file->open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text)) {
    auto meta_mesh = embedding->GetMetaMesh();
    auto base_mesh = embedding->GetBaseMesh();

    unsigned long baseeuler = base_mesh->n_vertices() - base_mesh->n_edges()
        + base_mesh->n_faces();
    unsigned long  metaeuler = meta_mesh->n_vertices() - meta_mesh->n_edges()
        + meta_mesh->n_faces();
    double basegenus = (2-baseeuler)/2.0;
    double metagenus = (2-metaeuler)/2.0;
    double avgmetaedgelength = 0.0;
    for (auto meh : meta_mesh->edges()) {
      avgmetaedgelength += embedding->CalculateEdgeLength(meh);
    }
    avgmetaedgelength /= meta_mesh->n_edges();
    double metaedgelengthdeviation = 0.0;
    for (auto meh : meta_mesh->edges()) {
      metaedgelengthdeviation += (avgmetaedgelength-embedding->CalculateEdgeLength(meh))
          *(avgmetaedgelength-embedding->CalculateEdgeLength(meh));
    }
    metaedgelengthdeviation /= meta_mesh->n_edges();
    metaedgelengthdeviation = std::sqrt(metaedgelengthdeviation);
    double avgvalence = 0.0;
    double valencedeviation = 0.0;
    for (auto mvh : meta_mesh->vertices()) {
      avgvalence += meta_mesh->valence(mvh);
      valencedeviation += (6-meta_mesh->valence(mvh))*(6-meta_mesh->valence(mvh));
    }
    avgvalence /= meta_mesh->n_vertices();
    valencedeviation /= meta_mesh->n_vertices();
    valencedeviation = std::sqrt(valencedeviation);

    QTextStream tstream(file);
    tstream << iteration << ","
            << time_elapsed << ","
            << base_mesh->n_faces() << ","
            << meta_mesh->n_faces() << ","
            << avgvalence << ","
            << valencedeviation << ","
            << avgmetaedgelength << ","
            << metaedgelengthdeviation;
    tstream << endl;
    file->close();
  }
}
