#pragma once

#include <OpenFlipper/BasePlugin/BaseInterface.hh>
#include <OpenFlipper/BasePlugin/ToolboxInterface.hh>
#include <OpenFlipper/BasePlugin/LoggingInterface.hh>
#include <OpenFlipper/BasePlugin/LoadSaveInterface.hh>
#include <OpenFlipper/common/Types.hh>

#include "MetaMeshToolbox.hh"
#include "Embedding.hh"
#include "IsotropicRemesher.hh"
#include "Testing.hh"

class MetaMeshPlugin : public QObject, BaseInterface, ToolboxInterface,
    LoggingInterface, LoadSaveInterface
{
  Q_OBJECT
  Q_INTERFACES(BaseInterface)
	Q_INTERFACES(ToolboxInterface)
  Q_INTERFACES(LoggingInterface)
  Q_INTERFACES(LoadSaveInterface)

	Q_PLUGIN_METADATA(IID "org.OpenFlipper.Plugins.Plugin-MetaMesh")

signals:

	// BaseInterface
  void updateView() override;
  void updatedObject(int _identifier, const UpdateType& _type) override;

	
	// ToolboxInterface
	void addToolbox(QString _name, QWidget* _widget) override;
	
	// LoggingInterface
	void log(Logtype _type, QString _message) override;
	void log(QString _message) override;

  // LoadSaveInterface
  void addEmptyObject(DataType _type, int& _id) override;

public:
	QString name() override { return QString("MetaMesh"); }
  QString description() override { return QString(""); }

public slots:
	QString version() override { return QString("1.0"); }
  void triangulate();
  void randomizetotal();
  void randomizeratio();
  void test_meta_mesh();
  void retrace();
  void rotate();
  void split();
  void collapse();
  void relocate();
  void remesh();
  void togglecopy();
  void copyselection();
  void DummySlotFunction1();
  void DummySlotFunction2();
  void nxt();
  void opp();
  void prv();

private slots:
	// BaseInterface
	void initializePlugin() override;
	void pluginsInitialized() override;

  void slotObjectUpdated(int _identifier) override;

private:
  bool metameshsanitychecks(bool verbose = true);
  void randomize(Embedding::RandomType type);
  void calculateoffset();
  void addoffset();
  void removeoffset();
  void MetaMeshUpdateNormals();
  void EmbeddingGarbageCollection();
  void UpdateMetaMesh(const UpdateType &_type);
  std::queue<OpenMesh::HalfedgeHandle> GetHalfedgeSelection();
  void screenshot(const QString& name, const QString& dir,
                  const int& width = 0, const int& height = 0);
  void preoperation();
  void postoperation(const UpdateType& typem, const UpdateType &typeb,
                     bool verbose = false);

  bool selection_mutex_ = false;

	MetaMeshToolbox* widget_;
  Embedding* embedding_;
  Testing* testing_;
  int base_mesh_id_=-1;
  int meta_mesh_id_=-1;
  PolyMesh* meta_mesh_;
  ACG::Vec3d meta_mesh_visualization_offset_ = {0.0f, 0.0f, 0.0f};
};

